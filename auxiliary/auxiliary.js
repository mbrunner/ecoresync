// EcoreSync Aux Functions
// A collection of auxiliary functions for ecoreSync.
// (C) 2019 Matthias Brunner

function compareEObjects(eObjectA,eObjectB,strict=false)
{
    //Compares two eObjects based on their js object and their EOQ id
    //if strict comparision is enabled the eObjects must be the same js object

   
    if(eObjectA==null || eObjectB==null)
    {
        return false;
    }

    if(eObjectA && eObjectB)
    {
        if(eObjectA.isProxy)
        {
            eObjectA=eObjectA.noproxy;
        }
        if(eObjectB.isProxy)
        {
            eObjectB=eObjectB.noproxy;
        }
        if(strict)
        {
            return eObjectA==eObjectB;
        }
        else
        {
            let res;
            try{
            res=eObjectA==eObjectB || eObjectA.get("_#EOQ") == eObjectB.get("_#EOQ");
            }
            catch(error)
            {
                console.error(error);
                console.trace();
            }
            return res;
        }
    }
    else
    {
        console.error('compareEObjects: At least one EObject is undefined');
        return false;
    }

}

function EObjectisContained(eContainer,eObject)
{
    //Non-recursive check if the eObject is contained in the eContainer
    //The function returns true only if the eObject is directly contained by the eContainer
    if(eContainer)
    {
        let contents=eContainer.eContents();
        for(let i in contents)
        {
            if(compareEObjects(contents[i],eObject))
            {          
                return true;
            }
        }
    }
    return false;
}

function EObjectisReferenced(eReference,eObject)
{
    //Non-recursive check if the eObject is contained in the eContainer
    //The function returns true only if the eObject is directly contained by the eContainer
    
    let contents=eReference.array();
    for(let i in contents)
    {
        if(compareEObjects(contents[i],eObject))
        {          
            return true;
        }
    }
    return false;
}

