function EcoreSyncBuffer(ecoreSync){
    this.ecoreSync=ecoreSync;
    var self=this;
    this.create=function(deferTime){
        var DeferredLoader=function(ecoreSync,deferTime)
        {
           var self=this;
           this.ecoreSync=ecoreSync;
           this.cmd=new eoq2.Cmp();
           this.pending=false;
           this.listeners=[];
           this.deferTime=deferTime
           this._execFun=async function(){
                if(self.pending){                
                    var execCmd=Object.assign({},self.cmd);
                    var execListeners=self.listeners.slice();
                    self.cmd=new eoq2.Cmp();
                    self.listeners=[];
                    self.pending=false;
                    var result=await self.ecoreSync.remoteExec(execCmd);
                    execListeners.forEach(function(e){
                        e.cb(result[e.idx]);           
                    })
                }             
           }
           this._observeResult=async function(idx){                
                var result=new Promise(function(resolve,reject){
                    self.listeners.push({idx:idx,cb:(result) => { resolve(result)}});
                })
                self._triggerExecution();
                return result
           }          
           this._triggerExecution=async function(){
                if(!self.pending)
                {
                    const delay = interval => new Promise(resolve => setTimeout(resolve,interval));
                    self.pending=true;
                    await delay(self.deferTime)
                    self._execFun();
                }
           } 

           this.Get=async function(query){
                self.cmd.Get(query);
                var res=await self._observeResult(self.cmd.a.length-1)
                return res
           }

           this.Set=async function(object,feature,value){               
                self.cmd.Set(object,feature,value);
                var res=await self._observeResult(self.cmd.a.length-1)
                return res
           }

        }
        return new DeferredLoader(self.ecoreSync,deferTime);
    }
}