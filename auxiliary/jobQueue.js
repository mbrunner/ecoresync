// EcoreSync Jobs
// Supplies a generic interface for processing a job queue
// (C) 2019 Matthias Brunner

var EcoreSyncJobQueue=function(ecoreSync)
{
    var self=this;
    this.__ecoreSync=ecoreSync;


    this.queue=function(callback=null)
    {
        var thisJob=this;
        this.callback=callback;
        this.currentTask=null;
        this.tasks=[];
        this.forkedTasks=[];
        this.endReached=false;
        this._isFinalized=false;

        this.setCallback=function(callback)
        {
            thisJob.callback=callback;
        }
        this.addJob=function(task)
        {
            if(!thisJob._isFinalized)
            {
                task.queue=thisJob;
                task.done=function(){ thisJob.__next(task);};
                task.fork=function(){ thisJob.forkedTasks.push(task);                                      
                                      task.done=function(){                                           
                                          thisJob.forkedTasks.splice(thisJob.forkedTasks.indexOf(task),1);
                                          if(thisJob.endReached){ if(thisJob.forkedTasks.length==0){thisJob.__finalize(); }}
                                         };                                                
                                       setTimeout(function(){  thisJob.__next(task);},0);
                                      };

                task.ecoreSync=self.__ecoreSync;
                thisJob.tasks.push(task);
            }
            else
            {
                alert('Job cannot be added to finalized queue');
            }
        };
        this.addJobAsNext=function(task)
        {
            if(!thisJob._isFinalized)
            {
                task.queue=thisJob;
                task.done=function(){ thisJob.__next(task);};
                task.fork=function(){ thisJob.forkedTasks.push(task);                                      
                                      task.done=function(){                                           
                                          thisJob.forkedTasks.splice(thisJob.forkedTasks.indexOf(task),1);
                                          if(thisJob.endReached){ if(thisJob.forkedTasks.length==0){thisJob.__finalize(); }}
                                         };                                                
                                       setTimeout(function(){  thisJob.__next(task);},0);
                                      };
                task.ecoreSync=self.__ecoreSync;
                thisJob.tasks.unshift(task);
            }
            else
            {
                alert('Job cannot be added to finalized queue');
            }
        }
        this.removeTask=function(task)
        {
            if(thisJob.tasks.indexOf(task)!=-1)
            {  
                thisJob.tasks.splice(thisJob.tasks.indexOf(task),1);
            }
        }
        this.run=function()
        {
            thisJob.__next(null);
        };        
        this.__next=function(task)
        {
            if(thisJob.currentTask!=task && task!=null)
            {
                throw 'Task returned, but it is not the running task. Task='+task.taskName
            }

            if(!thisJob.__isFinalized)
            {
                if(thisJob.tasks.length>0)
                {                         
                    thisJob.currentTask=thisJob.tasks.shift();                     
                    thisJob.currentTask.action();                    
                }
                else
                {
                    
                    thisJob.endReached=true;
                    if(thisJob.forkedTasks.length==0)
                    {
                        thisJob.__finalize();
                    }                        
                }
            }
            else
            {
                throw ' Tried to execute task of finalized queue.'
            }
        };
        this.__finalize=function()
        {     
            if(!thisJob._isFinalized)
            {
                //console.warn("Queue finalized");
                if(thisJob.callback!=null)
                {
                    setTimeout(thisJob.callback, 0);
                }
                thisJob._isFinalized=true;
            }
            else
            {
                throw 'Already finalized queue cannot be finalized again.';
            }
            
        };
     };

    this.CustomJob=function(customAction)
    {
        var task=this;
        task.taskName="Custom";
        this.queue=null;
        this.ecoreSync=null;
        this.done=null; 
        this.customAction=customAction;    
        this.action=function()
        {                    
            task.customAction(task);
        };
    };

    this.queueGroup=function(callback)
    {
        //group of queues that run in parallel
        //important: overrides all set queue callbacks
        var self=this;
        this.queueStates=[];
        this.queues=[];
        this.callback=callback;
        var checkComplete=function()
        {           
            var state=true;
            for(let i in self.queueStates)
            {
                state&=self.queueStates[i];
            }
            if(state)
            {
                setTimeout(function(){self.callback.apply();},0);
            }
        }

        this.addQueue=function(queue)
        {
            queue.setCallback(function()
            {               
                self.queueStates[self.queues.indexOf(queue)]=true;
                checkComplete.apply();
            });
            self.queues.push(queue);
            self.queueStates.push(false);   
        };         

        //run queue group
        this.run=function()
        {   
            if(self.queues.length>0)
            {          
                for(let i in self.queues)
                {
                    self.queues[i].run();
                }
            }
            else
            {
                setTimeout(function(){self.callback.apply();},0);
            }

        }

    }

}