// EcoreSync Changes
// Observes changes on the remote server and applies them to the local index if applicable.
// (C) 2019 Matthias Brunner


var EcoreSyncChanges=function(ecoreSync)
{
    var self=this;
    this.ecoreSync=ecoreSync;
    
    this.interval=1000; //milliseconds
    this.periodicUpdates=false;
    this.intervalTimer=null;
    this.changeId=-1;
    this.changeQueue=[];
    this.changePending=false;
    this.recentChange=0;
    this.maxChangesPerPeriod=100;

    // this.setInterval=function(interval)
    // {        
    //     this.interval=interval;
    //     if(self.periodicUpdates)
    //     {
    //         clearInterval(self.intervalTimer); 
    //         self.enablePeriodicUpdates();
    //     }   
    // };

    // this.enablePeriodicUpdates=function()
    // {       
    //     self.periodicUpdates=true;
    //     self.intervalTimer=setInterval(self.periodicUpdate,self.interval);
    // };

    // this.disablePeriodicUpdates=function()
    // {
    //     this.periodicUpdates=false;
    //     clearInterval(self.intervalTimer);
    // };

    // this.isEqual=function(changeA,changeB)
    // {
    //     //checks if two changes are equal
    //     if(changeA.length == changeB.length)
    //     {
    //         var equalEntries=0;
    //         for(let i in changeA)
    //         {
    //             if(i!=2)
    //             {
    //                 if(changeA[i]===changeB[i])
    //                 {
    //                     equalEntries+=1;
    //                 }
    //             }
    //             else
    //             {
    //                 if(changeA[2].hasOwnProperty('v') && changeB[2].hasOwnProperty('v') )
    //                 {
    //                     equalEntries+=1;
    //                 }
    //             }
    //         }

    //         if(equalEntries==changeA.length)
    //         {
    //             return true;
    //         }        
    //     }        
    //     return false;
    // };

    // this.periodicUpdate=function()
    // {
    //     if(self.changeQueue.length==0)
    //     {
    //         //Only check for changes if queue has been processed
    //         //var query="";
    //         let changeId=self.changeId+1;
    //         if(self.changeId==0)
    //         {
    //             changeId = 0;
    //         }

    //         let cmd = new eoq2.Chg(changeId,self.maxChangesPerPeriod);

    //         //let self = this;
    //         self.ecoreSync.eoq2domain.Do(cmd).then(function(res) {
    //             let val = eoq2.ResGetValue(res);
    //             for(let i in val) {
    //                 if(!self.changeQueue.find(function(e){ return e[0] == val[i][0]; }))
    //                 {
    //                     self.changeQueue.push(val[i]);           
    //                 }                
    //             }          
    //             self.processChangeQueue();
    //         });
    //     }
    // };




    // this.processChangeQueue=function() {   
    //     if(!this.processPending) {
    //         // Processes the received changes FIFO 
    //         for(let i in self.changeQueue) {
    //             let change = self.changeQueue[i];
    //             let cid = change.a[0];
    //             if(cid==self.changeId+1) {                     
    //                 self.processChange(change,function(){self.changeQueue.splice(i,1);});             
    //                 break;
    //             } else {                    
    //                 if(cid<=self.changeId || cid==0) {
    //                     //Already up-to-date
    //                     self.changeQueue.splice(i,1);             
    //                 }
    //             }
    //         }
    //     }
       
    // };

    this.processChange=async function(change,onSuccess)
    {       
   
        // this.processPending=true;
        let changeId = change[0];
        let changeType = change[1];
        let target = change[2].v;
        let featureName = change[3];
        //var transactionId=change[1];
        var eObject=self.ecoreSync.proxy.unproxy(self.ecoreSync.find(function(e){ return e.get("_#EOQ")==target}));
        //var updateCommand=change[3];
        var newValue=change[4];
        //var oldValue=change[5];


        if(self.ecoreSync.history.pull(changeId)) {
            //change was caused locally (hence no explicit sync needed)    
            if(onSuccess!=null) { onSuccess.apply(); }
            //self.update(changeId);
            
        } else { //change is not in history
            //Actually process the change
            if(eObject) {    
                switch(changeType) {
                    case eoq2.event.ChgTypes.SET:
                        {
                            let currentValue=eObject.get(featureName);  
                            var res= await ecoreSync.get(eObject,featureName);

                            //look if the change is an object and if the change changes anything locally
                            if(newValue.v) {
                                if(res.get("_#EOQ")!=newValue.v)
                                {
                                    var object=await ecoreSync.getObjectById(newValue.v);
                                    eObject.set(featureName,object);
                                }
                            } else {
                                if(currentValue!=newValue) {                        
                                    eObject.set(featureName,newValue);    
                                }     
                            }           
                        } 
                        break;
                    case eoq2.event.ChgTypes.ADD:
                        {         
                            var res= await ecoreSync.get(eObject,featureName); //this initializes the feature, may want to check whether the feature was initialized beforehand
                            var inReference=false;

                            if(Array.isArray(res))
                            {
                                if(res.find(function(e){    return e.get("_#EOQ")==newValue.v  })){
                                    inReference=true;
                                }                                    
                            }
                            else
                            {
                                throw 'CHG result is not an array.'
                            }
                                                    
                            if(!inReference) {        
                                
                                var object=await self.ecoreSync.getObjectById(newValue.v);
                                eObject.get(featureName).add(self.ecoreSync.proxy.unproxy(object));         
                            }                             
                        }                   
                        break;
                    case eoq2.event.ChgTypes.REM:
                        {
                            var res= await ecoreSync.get(eObject,featureName); //this initializes the feature, may want to check whether the feature was initialized beforehand
                            var inReference=false;

                            if(Array.isArray(res))
                            {
                                if(res.find(function(e){    return e.get("_#EOQ")==newValue.v  })){
                                    inReference=true;
                                }                                    
                            }
                            else
                            {
                                throw 'CHG result is not an array.'
                            }
                                                    
                            if(inReference) 
                            {                                 
                                var object=await self.ecoreSync.getObjectById(newValue.v);
                                eObject.get(featureName).remove(self.ecoreSync.proxy.unproxy(object));         
                            }                               
                        }
                        break;
                    case eoq2.event.ChgTypes.MOV:
                            break;
                    default:
                        throw new Error("Got change of unknown type: "+changeType);
                }
            } else {
                //Object is not on index, therefore the change is ignored. (It will take effect once the object is synced anyway.)
                if(onSuccess!=null) { onSuccess.apply(); }
                //self.update(changeId);                  
            }     
        }
 
    };

    // this.update=function(changeId)
    // {      
    //     this.processPending=false;
    //     self.changeId=changeId;
    //     setTimeout(function(){
    //         self.processChangeQueue();
    //     },0);
    // };

    // this.postpone=function(changeId)
    // {       
    //     this.processPending=false;
    // };

    this.setRevisionToLatest=function(callback=null) {
        //let query="CHANGES 0";
        let cmd = new eoq2.Sts();
        this.ecoreSync.eoq2domain.Do(cmd).then(function(val) {
            var latest= val;
           
            // self.update(latest);            
            if(callback!=null) {
                setTimeout(function(){callback.apply();},0);
            }
        })
    };

    this.onDomainChangeEvent = function(evts,src) {
        for(let i=0;i<evts.length;i++) {
            let evt = evts[i];
            if($DEBUG) console.error(evt);
            // let cid = evt.a[0];
           
            try{
                self.processChange(evt.a);              
            }
            catch(e)
            {
                console.error("Failed to process change"+e);
               // $app.notesManager.PutNote('FAILED: CHG EVT: '+evt.a[1]+' #'+evt.a[2].v+' /'+evt.a[3], 'info');
            }

            // if(!self.changeQueue.find(function(e){ return e[0] == cid; })){
            //     self.changeQueue.push(evt);           
            // }                
        }          
        //self.processChangeQueue();
    };


    this.listen=function() {
        this.setRevisionToLatest(
            function() {
                self.ecoreSync.eoq2domain.Observe(self.onDomainChangeEvent,[eoq2.event.EvtTypes.CHG]);
                //self.enablePeriodicUpdates();
            }
        );
    }

    
};
