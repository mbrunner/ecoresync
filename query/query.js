var QryMetaSegTypes={
    CLS : 'CLASS',  //class
    CLN : 'CLASSNAME',  //class name
    CON : 'CONTAINER',  //parent (container)
    PAR : 'PARENT',  //parent (container)
    ALP : 'ALLPARENTS',  //parent (container)
    ASO : 'ASSOCIATES',  //ASSOCIATES(start:root) all elements refering to this one beginning at start. default is root
    IDX : 'INDEX',  //index within its containment
    CFT : 'CONTAININGFEATURE',  //the feature that contains the element
    FEA : 'FEATURES',  //all features
    FEV : 'FEATUREVALUES',  //all feature values
    FEN : 'FEATURENAMES',  //all feature names
    ATT : 'ATTRIBUTES',  //all attribute features
    ATN : 'ATTRIBUTENAMES',  //all attribute feature names
    ATV : 'ATTRIBUTEVALUES',  //all attribute feature values
    REF : 'REFERENCES',  //all reference features
    REN : 'REFERENCENAMES',  //all reference feature names
    REV : 'REFERENCEVALUES',  //all reference feature values
    CNT : 'CONTAINMENTS',  //all containment features
    CNV : 'CONTAINMENTVALUES',  //all containment feature values
    CNN : 'CONTAINMENTNAMES',  //all containment feature names
    
    //class operators
    PAC : 'PACKAGE',  //class
    STY : 'SUPERTYPES',  //directly inherited classes
    ALS : 'ALLSUPERTYPES',  //all and also indirectly inherited classes
    IMP : 'IMPLEMENTERS',  //all direct implementers of a class
    ALI : 'ALLIMPLEMENTERS',  //all and also indirect implementers of a class  
    MMO : 'METAMODELS',  //retrieve all metamodels
    
     //Control flow operators 
    IFF : 'IF',  //if(condition,then,else); ,  //DEPRICATED
    TRY : 'TRY',  //catch errors and return a default,  //NOT IMPLEMENTED        
    
    //list operators
    LEN : 'SIZE',  //size of a list,  //DEPRICATED
    
    //recursive operators
    REC : 'REPEAT',  //REPEAT(<query>,depth) repeat a given query until no more results are found,  //NOT IMPLEMENTED    
}