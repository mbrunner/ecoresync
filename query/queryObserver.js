/* ecoreSync EOQ2 QueryObserver */
/* This ecoreSync queryObserver enables local EOQ2 query observance */

/* The ecoreSync queryObserver is based on the pyeoq2 queryRunner. The original python code was written by Björn Annighöfer */
/* ecoreSync provides a mdbAccessor and mdbObserver to enable hybrid (local/remote) query evaluation */
/* (C) 2020 Instiute of Aircraft Systems, Matthias Brunner */

class EcoreSyncQueryObserver extends EcoreSyncQueryRunner{
    constructor(ecoreSync){
        super(ecoreSync.mdbAccessor);
        this.ecoreSync=ecoreSync;
        this.mdbAccessor=this.ecoreSync.mdbAccessor;
        this.mdbObserver=this.ecoreSync.mdbObserver;   
    }

    async Eval(qry,callback)
    {
        if(!callback) { console.trace();  throw 'callback undefined'}
        var res=null
        var modelroot = await this.mdbAccessor.GetRoot()
        var context = modelroot
        res=await this.EvalOnContextAndScope(context,qry,context,history,async function(results){ 
            callback(await ApplyToAllElements(results,self.ecoreSync.decode))         
        })
        return res
    }

    async EvalOnContextAndScope(context,seg,scope,eoqHistory,callback)
    {
        if(!callback) { console.trace();  throw 'callback undefined'}
    
        var res = null
        var t=null
        if(seg) t = seg.qry      
        if(!t)  return seg 
 
        try{        
            var evalFunction = this.segmentEvaluators[t]  
            if(!evalFunction)
            {
                throw('eval function not found for segment type:'+t)
            }      

            var v=null;    
            if(seg){      
                 v = seg.v    
            }  

            try{
                res = await evalFunction(context,v,scope,eoqHistory,callback)    
            }
            catch(e)
            {
                console.error('Failed to evaluate segment. An error occured in the evaluation function '+evalFunction.name+': '+e)
                throw e
            }
        }
        catch(e){
             console.error("Segment type error type:"+t+" "+e);
             throw e
         }           
        
        return res  

    
        /*
        var res = null
        var t = seg.qry
        if(!t)  return seg 
      
        try{        
            var evalFunction = this.segmentEvaluators[t]       
            var v = seg.v    
            res = await evalFunction(context,v,scope,history,callback)    
        }
        catch(e){
             console.error("Unknown segment type:"+t+" "+e);
        }                   
        return res   
        */
    }

    async EvalPth(context,name,scope,history,callback)
    {
        if(!callback) { console.trace();  throw 'callback undefined'}
        var self=this;
        var superEvalPth=super.EvalPth.bind(this);

        var res = null
        var pathFunctor = async function(o){     
            var res=null;
            if(!o){ 
                throw 'pathFunctor: object undefined, feature='+name
            }
            else
            {   
                res=await self.mdbAccessor.Get(o,name)        
                await self.mdbObserver.Observe(o,name,async function(results){ var result=await superEvalPth(context,name,scope);  callback(result); })               
            }
            return res
        }        
        try{          
             res= await ApplyToAllElements(context, pathFunctor)         
        }
        catch(e){
            console.error("Error evaluating path segment "+name+": "+e)
            throw e
        }

        return res   
    }

    async EvalAny(context,args,scope,eoqHistory,callback){
        var res=null;
        var self=this;
           var anyFunctor=async function(a,b){
                if(IsList(b)){
                    for(let j in b){
                        for(let i in a){               
                            if(await self.EvalEqu(a[i],b[j],scope,eoqHistory,callback)) return true 
                        } 
                    }
                }else{                    
                        for(let i in a){                           
                            if(await self.EvalEqu(a[i],b,scope,eoqHistory,callback)) return true 
                        } 
                }        
                return false
            }
    
        if(!IsList(context)){
            console.error("ANY(1): Select argument must be a single element or a list of elements but got: "+context);
        }
        var select = await this.EvalOnContextAndScope(context,args,context,eoqHistory,callback)
        if(IsList(select) && !IsListOfObjects(select)){
           console.error("ANY(2): Select argument must be a single element or a list of elements but got: "+select);
        }
        res = ApplyToAllListsOfElementsInA(context,select,anyFunctor)       

        return res
    }

    async EvalCls(context,name,scope,eoqHistory,callback){
        var res = null
        var self=this;
        var clsFunctor = function(o){ 
            var res=null; 
            res=self.mdbAccessor.GetAllChildrenOfType(o,name)
            return res;
        } 
        
        try{
            res = await ApplyToAllElements(context, clsFunctor)
        }
        catch(e){
            console.error("Error evaluating class segment "+name+": "+e)
            throw e
        }
        return res
    }
    

    async EvalTrm(context,args,scope,eoqHistory,callback){
        var res = null
        var superEvalTrm=super.EvalTrm.bind(this);

        //Define select functors
        var TrmOperator=function(a,b,c){
            var res = null;
            if(a instanceof Terminator){
                res = a
            }else if(b){
                res = new Terminator(c)
            }else{
                res = a
            }
            return res
        }
        var TrmElemVsElemFunc=function(a,b,c){
            var res=[];
            b.forEach(function(e,i){
                res.push(TrmOperator(a[i],e,c));
            })
            return res
        }
        var TrmElemVsStructFunc=function(a,b,c){
            throw "Error applying termination: Argument of termination condition must be of lower depth than the context, but got "+a+","+b+","+c
        }
        var TrmStructVsElemFunc=function(a,b,c){
            var res=[];
            b.forEach(function(e,i){
                res.push(TrmOperator(a[i],e,c));
            })
            return res
        }
        //Begin of function
        var condquery = args[0]
        if(!condquery){ //special default case
            condquery = new eoq2.Qry().Equ(null)
        }

        var onChange=async function(results){
            var res=await superEvalTrm(context,args,eoqHistory);
            callback(res);
        }

        var defaultVal = await this.EvalOnContextAndScope(context,args[1],context,eoqHistory,onChange)  
        var condition = await this.EvalOnContextAndScope(context,condquery,context,eoqHistory,onChange)
        


        try{
            res = ApplyToSimilarListsOfObjects([context],[condition],TrmElemVsElemFunc,TrmElemVsStructFunc,TrmStructVsElemFunc,defaultVal)
        }
        catch(e){
            throw "Failed evaluating terminator "+args+". Terminator condition context and argument must be arrays of similar structure. Argument must be either be an array of Bool, but got "+condition+": "+e
        }
        
        return res[0] //return the first element because context and condition were listified above
    }

    async EvalQry(context,args,scope,history,callback)
    {
        if(!callback) { console.trace();  throw 'callback undefined'}
        var self=this;
        var res = null
        var currentContext = scope //each subquery restarts from the current scope
        
       
        var segmentCallback=function(seg){  

            //This is the segment callback of segment #seg indexed in args
            //The segment returns its new results and the successor should be evaluated using this new result
            //if the segment is the end of the query, we directly call the callback

            var evalStartSeg=seg+1; //therefore we start evaluation after his segment

            if(evalStartSeg<args.length)
            {
                //there are successors
                return async function(results){
                    //evaluate all successors (there should be a way to prevent multiple cb registration during this evaluation)
                    //e.g. the first time there should be a registration, but not afterwards

                    var currentContext=results;            
                    var res=null;
                    if(!args[evalStartSeg])
                    {       
                        throw('Segment evaluation error: no such segment')
                    }  

                    for(let i=evalStartSeg; i<args.length; i++)
                    {                
                        if(currentContext instanceof Terminator){ break; }
                        currentContext = await self.EvalOnContextAndScope(currentContext,args[i],scope,history,segmentCallback(i))
                    }

                    res = Determinate(currentContext) //not sure if this is working in this context
                    
                    callback(res); 
                }
            }
            else
            {
                //there are no successors, then then we can return the Query results directly
                return async function(results){
                    callback(results);
                }
            }
        }

        var currentContext = scope //each subquery restarts from the current scope
        //var newScope = context
        for(let i=0; i<args.length; i++){       
            if(currentContext instanceof Terminator){ break; }        
            currentContext = await this.EvalOnContextAndScope(currentContext,args[i],scope,history,segmentCallback(i))           
        }

        res = Determinate(currentContext)
        return res      
    }

    async EvalSel(context,args,scope,eoqHistory,callback)
    {
        if(!callback) { console.trace();  throw 'callback undefined'}
        var res = []
        var SelListVsListFunc=function(a,b){  
            return a.filter(function(e,i){
                return b[i]
            })
        }
        var SelListVsStructFunc=function(a,b){
            console.error("Error applying selector: Argument of selector must be of lower depth than the context, but got:");
            console.error('a=');
            console.error(a);
            console.error('b=');
            console.error(b);
        }
        var SelStructVsListFunc=function(a,b){
            return a.filter(function(e,i){
                return b[i]
            })
        }

        //Start Select evaluation        
        //selector changes the context
        var select=null
        try{            
             select = await this.EvalOnContextAndScope(context,args,context,eoqHistory,function(select){
                var res=[];
                try{
                    res = ApplyToSimilarListsOfObjects(context,select,SelListVsListFunc,SelListVsStructFunc,SelStructVsListFunc)
                }catch(e){
                    console.error("Failed evaluating selector during callback: "+args+". Selectors context and argument must be arrays of similar structure. Argument must be either be an array of Bool, but got "+select+": "+e)
                    throw e
                }     
                callback(res) //callback toward parent           
            })             
        }
        catch(e)
        {
            console.error('Select evaluation failed: '+e)
        }

        try{ 
            res = ApplyToSimilarListsOfObjects(context,select,SelListVsListFunc,SelListVsStructFunc,SelStructVsListFunc)
        }catch(e){
            console.error("Failed evaluating selector "+args+". Selectors context and argument must be arrays of similar structure. Argument must be either be an array of Bool, but got "+select+": "+e)
            throw e
        }        
   
        return res
    }

    async EvalEqu(context,args,scope,eoqHistory,callback){
        var res = await this.EvalElementOperation(context, args, scope, 'EQU', this.equEvaluators,eoqHistory,callback)
        return res
    }
    
    async EvalNeq(context,args,scope,eoqHistory,callback){
        var res = await this.EvalElementOperation(context, args, scope,'NEQ', this.neqEvaluators,eoqHistory,callback)
        return res
    }

    async EvalIdx(context,args,scope,eoqHistory,callback){
        var self=this;
        var res = null
        if(!IsList(context)){
            throw 'IDX: Can only select from lists but got: '+context
        }
        var n = await this.EvalOnContextAndScope(context,args,context,eoqHistory,function(){ console.warn('no callback implemented for EvalIdx'); })
        if(this._type(n)=='int'){
            var idxFunctor = (a,b) => a[b]
            res = ApplyToAllListsOfElementsInA(context,n,idxFunctor)
        }
        else if (this._type(n)=='str'){
            if("SORTASC"==n){
                var ascFunctor = (a,b) => a.sort()
                res = ApplyToAllListsOfElementsInA(context,null,ascFunctor)
            }
            else if("SORTDSC"==n){
                var dscFunctor = (a,b) => a.sort().reverse()
                res = ApplyToAllListsOfElementsInA(context,null,dscFunctor)
            }
            else if("FLATTEN"==n){
                if(IsList(context)){   
                    var _flatten=function(e){
                        if(Array.isArray(e))
                        {
                            return e.flatMap(_flatten)
                        }
                        else
                        {
                            return e
                        }
                    }                 
                    res=context.flatMap(_flatten)
                }
                else{
                    res = context
                }
            }
            else if("SIZE"==n){
                var lenFunctor = (a,b) => a.length
                res = ApplyToAllListsOfElementsInA(context,null,lenFunctor)
            }
            else{
                throw 'unkown index keyword: '+n
            }
        }else if(IsList(n) && n.length==3 && n[0]!=null && self._type(n[0])=="int")
        {
            var rngFunctor = (a,b) => self._range(a,b[0],b[1],b[2])
            res = ApplyToAllListsOfElementsInA(context,n,rngFunctor)
        }
        else{
            throw 'Invalid index argument, got: '+n+'('+self._type(n)+')'
        }
        return res
    }

    /* Private Methods */

    async EvalElementOperation(context,args,scope,operator,opEvaluators,eoqHistory,callback){
        var self=this;
        var res = null
        //varine operators
            var opEqualListsFunc=(a,b) => opEvaluators[self._type(a)](a,b)
            var opOnlyOp1ListFunc=function(a,b){
                    op1Functor = (o1,o2) => opEvaluators[self._type(o1)](o1,o2)
                    return ApplyToAllElementsInB(a,b,op1Functor)
            }
            var opOnlyOp2ListFunc=function(a,b){ // Is this the same as above?
                    var op2Functor = (o1,o2) => opEvaluators[self._type(o1)](o1,o2)
                    return ApplyToAllElementsInA(a,b,op2Functor)
            }
            var op1 = context
            var op2 = await this.EvalOnContextAndScope(context,args,scope,eoqHistory,callback)
            try{
                res = ApplyToSimilarElementStrutures(op1, op2, opEqualListsFunc, opOnlyOp1ListFunc, opOnlyOp2ListFunc)
            }
            catch(e){
                console.error("Failed to evaluate "+operator+". Context and arguments must be single elements or arrays of same type and size, but got "+op1+" "+op2+" "+ operator +" :"+e)
            }
            return res
    }  

}