// EcoreSync Mirror
// This component enables mirroring objects from the remote server, so that a copy of the remote object resides in the local model.
// By default, this component performs a recursive deep copy through which all directly and indirectly contained objects are copied.
// Provides a set of jobs that can be organized in a job queue.
// (C) 2019 Matthias Brunner


var EcoreSyncMirror=function(ecoreSync)
{
    var self=this;
    this.__ecoreSync=ecoreSync;

    this.ResourceSync=function(uri)
    {
        var task=this;
        this.taskName="ResourceSync";
        this.uri=uri;
        this.queue=null;
        this.ecoreSync=null;
        this.done=null;
        this.action=function()
        {
            var resource=self.__ecoreSync.__getResource(uri);;
            if(resource)
            {
                if(resource.get("_#EOQ")==null)
                {
                    self.__ecoreSync.__initResource(resource,function(){ console.log("QUEUE: Loading resource "+uri+" complete");  task.queue.addJobAsNext(new self.ObjectSync(resource.eContents()[0])); task.done();});     
                }    
            }    
        };
    };


    this.ResourceMetaSync=function(uri)
    {
        //Provides the meta-model of the resource identified by its URI locally 
        var task=this;
        this.taskName="ResourceMetaSync";
        this.uri=uri;
        this.queue=null;
        this.ecoreSync=null;
        this.done=null;
        this.action=function()
        {
            var resource=self.__ecoreSync.__getResource(uri);;
            if(resource)
            {
                if(resource.get("_#EOQ")==null)
                {
                    self.__ecoreSync.__initResource(resource,function(){ console.log("QUEUE: Loading resource "+uri+" complete");  task.queue.addJobAsNext(new self.ObjectSync(resource.eContents()[0])); task.done();});     
                }    
            }    
        };
    };

    this.EObjectFromId=function(objectId,callback)
    {
        var task=this;
        this.taskName="EObjectFromId";
        this.eObject=null;
        this.callback=callback;
        let queue=new ecoreSync.jobQueue.queue();  
        queue.addJob(new ecoreSync.mirror.ObjectSyncById(objectId,task));       
        queue.addJob(new ecoreSync.jobQueue.CustomJob(function(thistask){
            task.callback(task.eObject);
            thistask.done();
        }));      
        queue.run();
    }

    //BA: unused?
    // this.ObjectSyncById=function(objectId,parentTask)
    // {
    //     var parentTask=parentTask;
    //     var task=this;
    //     this.taskName="ObjectSyncById";
    //     this.objectId=objectId;
    //     this.eObject=null;
    //     this.queue=null;
    //     this.ecoreSync=null;
    //     this.done=null;
    //     this.action=function()
    //     {           
    //         let query="RETRIEVE #"+objectId+" /eClass";
    //         let onSuccess=function(value){
    //             let eClass=Ecore.EClass.create();
    //             eClass.set("_#EOQ",value[2].v);
    //             if(task.ecoreSync.index.contains(eClass))
    //             {
    //                 eClass=task.ecoreSync.index.getIndexEObject(eClass);
    //             }                
    //             parentTask.eObject=eClass.create();
    //             parentTask.eObject.set("_#EOQ",task.objectId);
    //             task.queue.addJob(new ecoreSync.mirror.ObjectSync(parentTask.eObject));  
    //             task.done();
    //         };
    //         let onFailure=function(msg){
    //             alert('Query Error: '+msg);
    //             task.done();
    //         };
    //         task.ecoreSync.domain.DoFromStr(query,onSuccess,onFailure);            
    //     };
    // };


    //BA: unused?
    // this.CheckScopeForSync=function(parent,eObject)
    // {
    //     var task=this;
    //     this.taskName="CheckScopeForSync";
    //     this.eObject=eObject;
    //     this.queue=null;
    //     this.ecoreSync=null;
    //     this.done=null;
    //     this.action=function()
    //     {
            
    //         let query="RETRIEVE #"+parent.get("_#EOQ")+" @RESOURCE\nRETRIEVE #"+eObject.get("_#EOQ")+" @RESOURCE";

    //         let onSuccess=function(value)
    //         {

    //            if(value[0][2]==null || value[1][2]==null)
    //            {
    //                 task.done();
    //            }       
    //            else
    //            {  
    //                 if(value[0][2].v == value[1][2].v)
    //                 {            
    //                     task.queue.addJobAsNext(new ecoreSync.mirror.ObjectSync(eObject));                  
    //                 }            
    //                 task.done();
    //            }
    //         }
    //         let onFailure=function(msg)
    //         {       
    //            task.done();
    //         };
    //         task.ecoreSync.domain.DoFromStr(query,onSuccess,onFailure);  
            
            
            
    //     };
    // };


    this.ObjectSync=function(eObject)
    {
        var task=this;
        this.taskName="ObjectSync";
        this.eObject=eObject;
        this.queue=null;
        this.ecoreSync=null;
        this.done=null;
        this.action=function()
        {
           //console.warn("Job: Adding jobs for sync of Object of Type "+eObject.eClass.get("name")+"/ #"+eObject.eClass.get("_#EOQ")); 
            task.queue.addJobAsNext(new ecoreSync.mirror.ContainmentsSync(eObject));
            task.queue.addJobAsNext(new ecoreSync.mirror.ReferencesSync(eObject));    
            task.queue.addJobAsNext(new ecoreSync.mirror.AttributesSync(eObject));
            task.queue.addJobAsNext(new ecoreSync.mirror.ClassSync(eObject));
            //console.warn("Job result: OK");   
            task.done();
        };
    };


    this.ClassSync=function(eObject)
    {
        var task=this;
        this.taskName="ClassSync";
        this.eObject=eObject;
        this.queue=null;
        this.ecoreSync=null;
        this.done=null;
        this.action=function()
        {     
            this.taskName+="/"+eObject.eClass.get("_#EOQ");
            ecoreSync.__initClass(eObject.eClass,true,function(){ task.done();});                  
        };
    };


    this.AttributesSync=function(eObject)
    {
        var task=this;
        this.taskName="AttributeSync";
        this.eObject=eObject;
        this.queue=null;
        this.ecoreSync=null;
        this.done=null;
        this.isDone=false;
        this.__returnedJobs=0;
        this.__threshold=-1;
        this.__returnEnabled=false;
        this.__return=function()
        {
            task.__returnedJobs+=1; 
            task.__check();
        }
        this.__check=function()
        {  
            if(task.__returnEnabled)
            {
                if(task.isDone)
                {                   
                    console.warn("WARNING: Called check after task was done. "+task.taskName+" #"+task.eObject.get("_#EOQ"));
                }
                else
                {
                    if(task.__threshold==task.__returnedJobs)
                    {                        
                       //console.warn("Job result: OK"); 
                        task.done();
                        task.isDone=true;
                    }      
                }
            }         
        } 
        this.action=function()
        {      
            task.fork();       
            var attr=task.eObject.eClass.get("eAllAttributes");
            task.__threshold=attr.length;

            for(let i in attr)
            {
                if(attr[i].get("name")!="_#EOQ")
                {
                   ecoreSync.__get(task.eObject,attr[i].get("name"),function(){ task.__return(); });     
                }      
                else
                {
                    task.__threshold-=1;
                }        
            }         
            task.__returnEnabled=true;          
            task.__check();
        };
    };

    this.ReferencesSync=function(eObject)
    {
        var task=this;
        this.taskName="ReferencesSync";
        this.eObject=eObject;
        this.queue=null;
        this.ecoreSync=null;
        this.done=null;
        this.isDone=false;
        this.__returnedJobs=0;
        this.__threshold=-1;
        this.__returnEnabled=false;
        this.__return=function()
        {
            task.__returnedJobs+=1; 
            task.__check();
        }
        this.__check=function()
        {  
            if(task.__returnEnabled)
            {
                if(task.isDone)
                {                   
                     console.warn("WARNING: Called check after task was done. "+task.taskName+" #"+task.eObject.get("_#EOQ"));
                }
                else
                {
                    if(task.__threshold==task.__returnedJobs)
                    {
                        //console.warn("Job result: OK"); 
                        task.done();
                        task.isDone=true;
                    }  
                 
                }
            }         
        }        
        this.action=function()
        {     
            
            task.fork();  
            var refs=task.eObject.eClass.get("eAllReferences");
            task.__threshold=refs.length;
            
            for(let i in refs)
            {          
                var refName=refs[i].get("name");
                ecoreSync.get(task.eObject,refName).then(function(){
                    let contents = [];
                    //BA: fixed upper and lower bound consideration
                    if(refs[i].get('upperBound')==1) {
                        let content = task.eObject.get(refName);
                        if(content) {
                            contents = [content]; 
                        }
                    } else {
                        contents=task.eObject.get(refName).array();
                    }
                    let queueGroup=new task.ecoreSync.jobQueue.queueGroup(function(){ task.__return(); });    
                    for(let j in contents) 
                    {
                        let queue=new task.ecoreSync.jobQueue.queue();  
                        queue.addJobAsNext(new ecoreSync.mirror.CheckScopeForSync(eObject,contents[j])); 
                        queueGroup.addQueue(queue);                   
                    }      
                    queueGroup.run();               
                },
                function(error)
                {
                    //unresolved model?
                    //TODO: error handling!!!
                    console.error("Error resolving reference!"+error);
                    task.__return();
                });                      
            }   
                         
            task.__returnEnabled=true;           
            task.__check();
             
              
        };
    };

    this.ContainmentsSync=function(eObject)
    {
        var task=this;
        this.taskName="ContainmentsSync";
        this.eObject=eObject;
        this.queue=null;
        this.ecoreSync=null;
        this.done=null;
        this.fork=null;
        this.isDone=false;
        this.__returnedJobs=0;
        this.__threshold=-1;
        this.__returnEnabled=false;
        this.__return=function()
        {
            task.__returnedJobs+=1; 
            task.__check();
        }
        this.__check=function()
        {  
            if(task.__returnEnabled)
            {
                if(task.isDone)
                {                   
                     console.warn("WARNING: Called check after task was done. "+task.taskName+" #"+task.eObject.get("_#EOQ"));
                }
                else
                {
                    if(task.__threshold==task.__returnedJobs)
                    {
                        //console.warn("Job result: OK"); 
                        task.done();
                        task.isDone=true;              
                    }  
                 
                }
            }         
        }        

        this.action=function()
        {      
           // console.warn("ContainmentSync #"+eObject.get("_#EOQ"));      
           task.fork();            
            var conts=task.eObject.eClass.get("eAllContainments");
            task.__threshold=conts.length;                       

            for(let i in conts)
            {       
      
                ecoreSync.__updateReference(task.eObject,conts[i].get("name"),function(){                       
                    let contents=[];
                    //BA: changed in order to consider real multiplicities
                    if(conts[i].get("upperBound")==1) {
                        let content = task.eObject.get(conts[i].get("name"))
                        if(content) {
                            contents = [content]; 
                        }
                    } else {
                        contents=task.eObject.get(conts[i].get("name")).array();
                    }   
                    let queueGroup=new ecoreSync.jobQueue.queueGroup(function(){ task.__return(); });                               
                    for(let j in contents) 
                    {       
                        let queue=new task.ecoreSync.jobQueue.queue();           
                        queue.addJobAsNext(new task.ecoreSync.mirror.CheckScopeForSync(eObject,contents[j]));  
                        queueGroup.addQueue(queue);                                                   
                    }                   
                    queueGroup.run();              
                });   
                     
            }             

            task.__returnEnabled=true;
            task.__check();                     
            
                    
        };
    };


    /*
    this.ContainmentsSync=function(eObject)
    {
        var task=this;
        this.taskName="ContainmentsSync";
        this.eObject=eObject;
        this.queue=null;
        this.ecoreSync=null;
        this.done=null;
        this.isDone=false;
        this.__returnedJobs=0;
        this.__threshold=-1;
        this.__returnEnabled=false;
        this.__return=function()
        {            
            task.__returnedJobs+=1; 
            task.__check();
        }
        this.__check=function()
        {  
            if(task.__returnEnabled)
            {
                if(task.isDone)
                {                   
                    console.warn("WARNING: Called check after task was done. "+task.taskName+" #"+task.eObject.get("_#EOQ"));
                }
                else
                {
                    if(task.__threshold==task.__returnedJobs)
                    {
                        //console.warn("Job result: OK."); 
                        task.done();
                        task.isDone=true;
                    }  
                }
            }         
        }        
        this.action=function()
        {                    
            var conts=task.eObject.eClass.get("eAllContainments");
            task.__threshold=conts.length;            
            for(let i in conts)
            {
               
                ecoreSync.__updateReference(task.eObject,conts[i].get("name"),function(){                     
                    let contents=task.eObject.get(conts[i].get("name")).array();                 
                    for(let j in contents) 
                    {                  
                        task.queue.addJobAsNext(new ecoreSync.mirror.CheckScopeForSync(eObject,contents[j]));    
                    } 
                    task.__return();     
                });       
            }  
            task.__returnEnabled=true;
            task.__check();             
        };
    };*/
   
}