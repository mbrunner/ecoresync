// EcoreSync Proxy
// The proxies enable the automatic loading of queried model references, containments and attributes.
// The event system of ecore.js informs listeners on any changes.
// (C) 2019 Matthias Brunner


var EcoreSyncProxy=function(ecoreSync)
{
    var __proxy=this;
    this.__ecoreSync=ecoreSync;

    this.EObject=function()
    {
        var EObjectProxy={get: function(target,key,receiver){   
            
            var self=target;      
            var originalMethod;
            
            if(target)
            {
                originalMethod=target[key];
            }

            if(!originalMethod)
            {
                
                if(key!="noproxy" && key!="isProxy" && key!="proxyType" && key!="_attributeInitialized" && key!="then" && key!="eContainer")
                {
                     console.error("proxy received call to invalid key");
                     console.error(key);
                     console.error(target)
                     console.error(this);
                }
            }           
            if(key=="create")
            {              
                return function(){return originalMethod.apply(self,arguments);};  
            }            
            if(key=="isProxy")
            {
                return true;
            }
            if(key=="then")
            {
                return undefined;
            }
            if(key=="_attributeInitialized")
            {
                return originalMethod;
            }
            if(key=="noproxy")
            {
                return target;
            }
            if(key=="proxyType")
            {
                return "EObject";
            }
            if(key=="eClass")
            {
                if(target.get("name")=="ResourceSet" || target.get("name")=="Resource")
                {                    
                    return target.eClass;
                }
                else
                {
                    return new Proxy(target.eClass,EObjectProxy);
                }
            }
            if(key=="eContainer")
            {
                if(target.eContainer)
                {
                    if(target.get("name")=="ResourceSet" || target.get("name")=="Resource")
                    {                    
                        return target.eContainer;
                    }
                    else
                    {
                        return new Proxy(target.eContainer,EObjectProxy);
                    }
                }              
            }
            if(key=="eResource")
            {      
                return function(){ return new Proxy(target.eResource(),__proxy.EResource()) };                
            }
            if(key=="set")
            {
                return function(){              
                   // __proxy.__ecoreSync.__setValue(self,arguments[0],arguments[1],self.get(arguments[0])); 
                   if(arguments[0])
                   {
                    console.warn('proxy set method is deprecated')
                    //__proxy.__ecoreSync.set(self,arguments[0],arguments[1]);               
                    result=originalMethod.apply(self,arguments); 
                   }    
                   else
                   {
                       console.error("feature is undefined");
                   }              
                    return result;
                };    
            }
            if(key=="unset")
            {
                return function(){
                    __proxy.__ecoreSync.unset(self,arguments[0]);
                    result=originalMethod.apply(self,arguments);                   
                    return result;
                };    
            }
            if(key=="get")
            {
            
            
                return function(){
                    switch(arguments[0])
                    {
                        case "eType":
                            return new Proxy(target.get("eType"),EObjectProxy);
                        case "eAllAttributes":
                            proxyResults=[];
                            if(target._isInitialized)
                            {      
                                results=originalMethod.apply(self,arguments);                                
                                for (let i in results)
                                {
                                    if(results[i].get("name")!="_#EOQ")
                                    {
                                        proxyResults.push(new Proxy(results[i],EObjectProxy));
                                    }
                                }
                            }
                            else
                            {
                                if(target.get("_#EOQ")!=null)
                                {
                                    __proxy.__ecoreSync.__initClass(target);     
                                }                           
                            }
                            return proxyResults;
                        case "eAllContainments":
                            proxyResults=[];
                            if(target._isInitialized)
                            {      
                                results=originalMethod.apply(self,arguments); 
                                for (let i in results)
                                {
                                    proxyResults.push(new Proxy(results[i],EObjectProxy));
                                }
                            }
                            else
                            {
                                if(target.get("_#EOQ")!=null)
                                {
                                __proxy.__ecoreSync.__initClass(target);    
                                }                            
                            }
                            return proxyResults;
                        case "eAllReferences":
                            proxyResults=[];
                            if(target._isInitialized)
                            {                           
                                results=originalMethod.apply(self,arguments); 
                                for (let i in results)
                                {
                                    proxyResults.push(new Proxy(results[i],EObjectProxy));
                                }
                            }
                            else
                            {
                                if(target.get("_#EOQ")!=null)
                                {
                                    __proxy.__ecoreSync.__initClass(target);
                                }
                            }
                            return proxyResults;
                            case "eAllStructuralFeatures":
                                proxyResults=[];
                                if(target._isInitialized)
                                {                           
                                    results=originalMethod.apply(self,arguments); 
                                    for (let i in results)
                                    {
                                        proxyResults.push(new Proxy(results[i],EObjectProxy));
                                    }
                                }
                                else
                                {
                                    if(target.get("_#EOQ")!=null)
                                    {
                                        __proxy.__ecoreSync.__initClass(target);
                                    }
                                }
                                return proxyResults;
                        case "contents":
                            if(target.get("_#EOQ")==null)
                            {
                                    if(target.eClass.get("name")=="ResourceSet")
                                    {
                                        __proxy.__ecoreSync.__refreshResourceSet(target);
                                        return originalMethod.apply(self,arguments); 
                                    }
                                    if(target.eClass.get("name")=="Resource")
                                    {
                                        console.log("Loading "+target.get("uri"))
                                        __proxy.__ecoreSync.__initResource(target);
                                        return originalMethod.apply(self,arguments); 
                                    }
                            }
                            else
                            {
                                return originalMethod.apply(self,arguments); 
                            }     
                            break;
                        default:    
                        
                            if(!target.has(arguments[0]))
                            {
                                
                                console.warn("Object does not have key '"+arguments[0]+"'");
                                console.warn(target);
                                if(target.get("_#EOQ")!=null)
                                {
                                  //__proxy.__ecoreSync.__get(target,arguments[0]);    
                                                   
                                }
                                else
                                {

                                    if(target.get("name")=="Resource" || target.get("name")=="ResourceSet" || target.eClass.get("name")=="Resource" || target.eClass.get("name")=="ResourceSet")
                                    {
                                        return originalMethod.apply(self,arguments);                                        
                                    }
                                    else
                                    {
                                        console.warn("Cannot query object due to missing object id. ("+arguments[0]+"). Local information available only.");
                                        //return originalMethod.apply(self,arguments);    
                                    }
                                
                                }
                            }
                            else
                            {
                                
                                var featureName=arguments[0];                    
                                //feature is a reference/containment
                                let feature = target.eClass.get("eAllContainments").find(function(e){return e.get("name")==featureName;});
                                if(!feature) {
                                    feature = target.eClass.get("eAllReferences").find(function(e){return e.get("name")==featureName;});
                                }

                                //if(target.eClass.get("eAllContainments").find(function(e){return e.get("name")==featureName;})!=undefined || target.eClass.get("eAllReferences").find(function(e){return e.get("name")==featureName;})!=undefined)
                                if(feature)
                                {         
                                    //BA: modified to handle single element references                                                
                                    if(!target.get(arguments[0]) || target.get(arguments[0])["_isInitialized"]!=true)
                                    {
                                        __proxy.__ecoreSync.__updateReference(target,arguments[0]);
                                    }
                                    result=originalMethod.apply(self,arguments); 
                                    if(feature.get('upperBound')==1) {
                                        if(result) {
                                            return new Proxy(result,__proxy.EObject(__proxy.__ecoreSync));
                                        } else {
                                            return null;
                                        }
                                    } else { 
                                        return new Proxy(result,__proxy.EReference(__proxy.__ecoreSync));
                                    }
                                }

                                //feature is an attribute
                                if(target.eClass.get("eAllAttributes").find(function(e){return e.get("name")==featureName;})!=undefined)
                                {                
                                
                                
                                    var directAccess=false;

                                    if(featureName!="_#EOQ")
                                    {
                                        
                                        
                                        if(target.eClass.get("name")=="Resource" || target.eClass.get("name")=="ResourceSet" || target.get("name")=="Resource" || target.get("name")=="ResourceSet" || target.get("name")=="EReference" || target.eClass.get("name")=="EReference"  || target.get("name")=="EAttribute" || target.eClass.get("name")=="EAttribute")
                                        {                                                                            
                                            directAccess=true;
                                        }
                                        else
                                        {                                
                                            
                                            if(typeof target["_attributeInitialized"] === "object" && (target["_attributeInitialized"] != null))
                                            {          
                                                                                                            
                                                //we check if the attribute was not queried before
                                                if(!target["_attributeInitialized"][featureName])
                                                {
                                                    if(target.get("_#EOQ")!=null)
                                                    {
                                                            __proxy.__ecoreSync.__get(target,arguments[0]);
                                                        
                                                    }
                                                    else
                                                    {
                                                        
                                                    }
                                                }
                                                else
                                                {
                                                    directAccess=true;
                                                
                                                }                                                   
                                            }
                                            else
                                            {
                                                //Query Attribute
                                                if(target.get("_#EOQ")!=null)
                                                {                   
                                                                                
                                                    __proxy.__ecoreSync.__get(target,arguments[0]);
                                                }                               
                                            }                   
                                        }
                                    }
                                    else
                                    {
                                        //object Id can be accessed directly
                                        directAccess=true;
                                    }
                                
                                    if(directAccess)
                                    {                           
                                        //provide direct access
                                        return originalMethod.apply(self,arguments);  
                                    }
                                    else
                                    {
                                        return undefined;
                                    }
                                }
                            }     
                    
                    }                    
                };
            }


            if(key=="eContents")
            {
                if(target.get("_#EOQ")==null)
                {

                    switch(target.eClass.get("name"))
                    {
                        case "ResourceSet":
                                __proxy.__ecoreSync.__refreshResourceSet(target);
                                return function(){ return [];}
                        case "Resource":
                                __proxy.__ecoreSync.__initResource(target);
                                return function(){ return [];}
                        default:
                                console.log("eContents of unknown object can not be determined.");
                                return function(){ return [];}
                    }
                
                }
                else
                {

                    switch(target.eClass.get("name"))
                    {
                        case "ResourceSet":
                            return function(){
                                var __proxyEContents=[];
                                var __eContents=target.eContents();
                                for (let i in __eContents )
                                {
                                    __proxyEContents.push(new Proxy(__eContents[i],EObjectProxy));
                                }
                                return __proxyEContents;
                                };
                        case "Resource":
                            return function(){
                                var __proxyEContents=[];
                                var __eContents=target.eContents();
                                for (let i in __eContents )
                                {
                                    __proxyEContents.push(new Proxy(__eContents[i],EObjectProxy));
                                }
                                return __proxyEContents;
                                };
                        default:
                            if(target.eClass._isInitialized)
                            {
                                return function(){
                                    var __proxyEContents=[];
                                    var __eContents=target.eContents();
                                    for (let i in __eContents )
                                    {
                                        __proxyEContents.push(new Proxy(__eContents[i],EObjectProxy));
                                    }
                                    return __proxyEContents;
                                    };
                            }
                            else
                            {            
                                    if(target.get("_#EOQ")!=null)
                                    {                    
                                    __proxy.__ecoreSync.__initClass(target.eClass);
                                    }
                                    return function(){ return [];}
                            }                                
                    }                

                }
            }
            else
            {
                return target[key];
            } 

            }};

            return EObjectProxy;
        };

        
    this.EReference=function()
    {
    var EReferenceProxy={get: function(target,key,receiver){
        var originalMethod=target[key];
        var self=target;
        if(!originalMethod)
        {
            if(key!="noproxy" && key!="isProxy" && key!="proxyType" && key!="_isInitialized")
            {
                 console.log(target);
                 console.error("proxy received call to undefined key key="+key);
                 console.warn("proxy is EReference");
            }
        }  
        else
        {
            //console.warn("call to proxy: key="+key)
        }

        if(key=="_isInitialized")
        {
            return self._isInitialized;
        }

        if(key=="proxyType")
        {
            return "EResource";
        }

        if(key=="add")
        {

            return function(){

                if(target._isContainment)
                {
                    __proxy.__ecoreSync.__createIn(self,arguments[0]);      
                }
                else
                {
                    __proxy.__ecoreSync.__addReferenceTo(self,arguments[0]);   
                }           

                //Carry out the action
                result=originalMethod.apply(self,arguments);                   
                return result;
            };    
        }


        if(key=="remove")
        {
            return function(){   
                
                if(target._isContainment)
                {
                    __proxy.__ecoreSync.__deleteFrom(self._owner,self._feature.get('name'),arguments[0]);     
                }
                else
                {
                    __proxy.__ecoreSync.__removeReferenceTo(self._owner,self._feature.get('name'),arguments[0]);   
                }          

                //Carry out the action
                result=originalMethod.apply(self,arguments);                   
                return result;
            };
        }

        if(key=="array")
        {
           proxyResults=[];
           results=target.array();
           for(let i in results)
           {
                proxyResults.push(new Proxy(results[i],__proxy.EObject(__proxy.__ecoreSync)));
           }   
           return function(){ return proxyResults; };
        }

        if(key=="length")
        {
            return originalMethod;
        }


            //return originalMethod.apply(self,arguments);
        
    }};
    return EReferenceProxy;
    };


    this.EResource=function()
    {

    var EResourceProxy={get: function(target,key,receiver){
        
        var originalMethod=target[key];
        var self=target;
        if(!originalMethod)
        {
            if(key!="noproxy" && key!="isProxy")
            {
                 console.error("proxy received call to undefined key key="+key)
            }
        }       
        if(key=="get")
        {
            return function(){
                 switch(arguments[0])
                 {
                    case "contents":
                        if(target.get("_#EOQ")==null)
                        {
                            __proxy.__ecoreSync.__initResource(target.get("uri"));
                            return originalMethod.apply(self,arguments); 
                        }
                        else
                        {
                            return originalMethod.apply(self,arguments); 
                        } 
                        break;    
                    default:
                        return originalMethod.apply(self,arguments);
                }          
                            };
        }

        if(key=="eContents")
        {
            if(target.get("_#EOQ")==null)
            {
                __proxy.__ecoreSync.__initResource(target.get("uri"));
                return function(){          
                    proxyResults=[];
                    results=target.eContents();
                    for(let i in results)
                    {
                         proxyResults.push(new Proxy(results[i],self.EObject(__proxy.__ecoreSync)));
                    }   
                    return function(){ return proxyResults; };                
                }; 
            }
            else
            {
                proxyResults=[];
                results=target.eContents();
                for(let i in results)
                {
                     proxyResults.push(new Proxy(results[i],self.EObject(__proxy.__ecoreSync)));
                }   
                return function(){ return proxyResults; };  
            } 
        }

        return originalMethod;
    
        }};
    
        return EResourceProxy;
        }

        this.unproxy=function(object)
        {
            if(object)
            {
                if(object.isProxy)
                {
                    return object.noproxy;
                }
            }        
            return object;            
        }
};



