// EcoreSync History
// Saves the recent transactions 
// (C) 2019 Matthias Brunner


var EcoreSyncHistory=function(ecoreSync)
{
    var self=this;
    this.ecoreSync=ecoreSync;
    this.numEntries=500;
    this.history=[];
    this.__nextIndex=0;
    this.push=function(query,transactionId)
    {
        self.history[self.__nextIndex]={query:query,transactionId:transactionId};
        self.__nextIndex+=1;
        if(self.__nextIndex>=self.numEntries)
        {
            self.__nextIndex=0;
        }
    };
    this.pull=function(transactionId)
    {
        return self.history.find(function(e){return e.transactionId==transactionId; });
    }    
};
