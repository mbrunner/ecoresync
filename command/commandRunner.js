/* ecoreSync EOQ2 CommandRunner */
/* This ecoreSync CommandRunner enables local EOQ2 commands on ecoreSync */

/* The ecoreSync CommandRunner is based on the pyeoq2 CommandRunner. The original python code was written by Björn Annighöfer */
/* ecoreSync provides a mdbAccessor to enable hybrid (local/remote) command and query evaluation */

/* (C) 2020 Instiute of Aircraft Systems, Matthias Brunner */

function EcoreSyncCmdRunner(ecoreSync,maxChanges=100)
{
    var self=this;
    this.ecoreSync=ecoreSync;
    this.mdbAccessor=ecoreSync.mdbAccessor;
    this.qryEvaluator=new EcoreSyncQueryRunner(self.mdbAccessor)
    this.maxChanges=maxChanges

       
    //internals
    self.latestTransactionId=0
    self.transactions=[]
    self.tempChangesForTransaction={}
    self.tempHistoryForTransaction={}

    self.earliestChangeId=0
    self.latestChangeId=0
    self.changes=[]
    self.currenteoqHistoryId=0
    self.eoqHistory=[]


    self.Exec=async function(cmd){
        var tid= self.StartTransaction()
        var res= await self.ExecOnTransaction(cmd,tid)
        res=await ApplyToAllElements(res,self.ecoreSync.decode);
        self.EndTransaction(res,tid)
        return res
    }


    self.ExecOnTransaction=function(cmd,tid){
        var res = null        

        try{
            evaluator = self.cmdEvaluators[cmd.cmd]
            res = evaluator(cmd.a,tid)
        }
        catch(e)
        {         
            errorMsg = "Error evaluating command: Unknown command type: "+cmd.cmd+" :"+e
            console.error(errorMsg)     
        }
       
        return res
    }

    /* Command Evaluators */

    self.ExecGet=async function(args,tid){
        var status = 'OK'
        var res = null  
        try{
            var target = args
            var eoqHistory = self.tempHistoryForTransaction[tid]
            res = await self.qryEvaluator.Eval(target,eoqHistory)
            self.AddToHistory(res,tid)
        }
        catch(e){
            errorMsg = e.toString()
            console.error(errorMsg)
            status = 'ERR'
            res = errorMsg
        }
        return res
    }

    self.ExecCmp=async function(args,tid){
        var subresults = []
        var n = 0;
        for(c in args){
            n+=1
            try{
                var subresult = self.ExecOnTransaction(args[c], tid) 
                subresult=await ApplyToAllElements(subresult,self.ecoreSync.decode);
                subresults.push(subresult)
            }
            catch(e){       
            console.error("Sub command "+n+" failed: "+e)
            }
        }
        return subresults
    }


    self.ExecCrn=async function(args,tid){
        var status = 'OK'
        var res = null  
        try{ 
            var eoqHistory = self.tempHistoryForTransaction[tid]
            var packageName = await self.qryEvaluator.Eval(args[0],eoqHistory)
            var className = await self.qryEvaluator.Eval(args[1],eoqHistory)
            var n = await self.qryEvaluator.Eval(args[2],eoqHistory)
            var constructorArgs = args[3].map(function(a){ return self.qryEvaluator.Eval(a,eoqHistory)})
            var constructorArgs= await Promise.all(constructorArgs);
            if(IsNoList(packageName) && IsNoList(className)){
                if(IsNoList(n)){
                    res = await self.mdbAccessor.CreateByName(packageName,className,n,constructorArgs)
                }
                else{
                   throw 'Error in create: n must be a positive integer, but got: '+n
                }
            }
            else{
                throw 'Error in create: packageName and className must be strings, but got: '+packageName +', '+className 
            }
            self.AddToHistory(res,tid)
        }
        catch(e){
            status = 'ERR'           
            throw e
        }
        return res; //Res(CmdTypes.CRN,status,res,tid,self.latestChangeId)
    }


    self.ExecAdd=async function(args,tid)
    {
        var status='OK'
        var res=null;
        var eoqHistory = self.tempHistoryForTransaction[tid] //hack to make history work for the SET command, because transactions are not implemented yet for the commandrunner    
        let tgt =  self.qryEvaluator.Eval(args[0],eoqHistory)
        let ft =  self.qryEvaluator.Eval(args[1],eoqHistory)
        let val =  self.qryEvaluator.Eval(args[2],eoqHistory)     

        //parellelizes the query evaluation
        var target=await tgt
        var feature=await ft
        var value=await val

        if(IsNoList(target)) // e.g. #20
        {      
            if(IsNoList(feature))
            {           
                if(IsNoList(value))
                {                   
                        //default case: one target, one feature, single value
                        await self.mdbAccessor.Add(target,feature,value);
                }
                else
                {    
                    var operations=[];
                    for(let v in value){
                        operations.push(self.mdbAccessor.Add(target,feature,value[v]) );
                    }
                    await Promise.all(operations)
                }
            }
      
        }else if(IsListOfObjects(target)){ //e.g. [#20,#22,#23]
            //all multiple targets
            if(IsNoList(feature))
            {
                //multiple targets, all single feature
                if(IsNoList(value))
                {
                    //case: multiple target, single feature, single value
                    var operations=[];
                    for(let t in target)
                    {
                        operations.push(self.mdbAccessor.Add(target[t],feature,value));
                    }
                    await Promise.all(operations)
                }
                else if(value.length && target.length)
                {
                    //case: multiple target, single feature, multiple value, with equal list lengths
                    var operations=[];
                    for(let t in target)
                    {
                        operations.push(self.mdbAccessor.Add(target[t],feature,value[t]));
                    }
                    await Promise.all(operations)
                }
            }

        }   




        res = [target,feature,value]
        self.AddToHistory(res,tid)
        return res;

    }
    /*

    def ExecAdd(self,args,tid):
        status = ResTypes.OKY
        res = None  
        cid = self.latestChangeId
        try: 
            #eval all arguments
            eoqHistory = self.tempHistoryForTransaction[tid]
            target = self.qryEvaluator.Eval(args[0],eoqHistory)
            feature = self.qryEvaluator.Eval(args[1],eoqHistory)
            value = self.qryEvaluator.Eval(args[2],eoqHistory)
            
            #set the value(s) depending on the multiplicity of the arguments
            if(IsNoList(target)): # e.g. #20
                if(IsNoList(feature)):
                    if(IsNoList(value)):
                        (oldVal,oldOwner,oldFeature,oldIndex) = self.mdbAccessor.Add(target,feature,value)
                        cid = self.AddLocalChange(tid,ChgTypes.ADD,target,feature,value,oldVal,oldOwner,oldFeature,oldIndex)
                    elif(IsList(value)):
                        for v in value:
                            (oldVal,oldOwner,oldFeature,oldIndex) = self.mdbAccessor.Add(target,feature,v)
                            cid = self.AddLocalChange(tid,ChgTypes.ADD,target,feature,v,oldVal,oldOwner,oldFeature,oldIndex)
                    else:
                        raise EoqError(0,'Error in add: value must be single value or list of values, but got: %s:'%(value)) 
                elif(IsListOfObjects(feature)):
                    if(IsNoList(value)):
                        for f in feature:
                            (oldVal,oldOwner,oldFeature,oldIndex) = self.mdbAccessor.Add(target,f,value)
                            cid = self.AddLocalChange(tid,ChgTypes.ADD,target,f,value,oldVal,oldOwner,oldFeature,oldIndex)
                    elif(IsList(value) and len(value) == len(feature) and IsListOfObjects(value[0])):
                        for i in range(len(feature)):
                            for v in value[i]:
                                (oldVal,oldOwner,oldFeature,oldIndex) = self.mdbAccessor.Add(target,feature[i],v)
                                cid = self.AddLocalChange(tid,ChgTypes.ADD,target,feature[i],v,oldVal,oldOwner,oldFeature,oldIndex)
                    else:
                        raise EoqError(0,'Error in add: value must be single value or list of list of values with outer list having a length equal to the number of features, but got: %s:'%(value)) 
                else:
                    raise EoqError(0,'Error in add: feature must be single object or list of objects but got: %s:'%(feature)) 
            elif(IsListOfObjects(target)): # e.g. [#20,#22,#23]
                if(IsNoList(feature)):
                    if(IsNoList(value)):
                        for t in target:
                            (oldVal,oldOwner,oldFeature,oldIndex) = self.mdbAccessor.Add(t,feature,value)
                            cid = self.AddLocalChange(tid,ChgTypes.ADD,t,feature,value,oldVal,oldOwner,oldFeature,oldIndex)
                    elif(IsList(value) and len(value) == len(target) and IsListOfObjects(value[0])):
                        for i in range(len(t)):
                            (oldVal,oldOwner,oldFeature,oldIndex) = self.mdbAccessor.Set(target[i],feature,value[i])
                            cid = self.AddLocalChange(tid,ChgTypes.ADD,target[i],feature,value[i],oldVal,oldOwner,oldFeature,oldIndex)
                    else:
                        raise EoqError(0,'Error in add: value must be single value or list of list of values with the outer list having a length equal to the number of targets, but got: %s:'%(value)) 
                elif(IsListOfObjects(feature)):
                    if(IsNoList(value)):
                        for t in target:
                            for f in feature:
                                (oldVal,oldOwner,oldFeature,oldIndex) = self.mdbAccessor.Add(t,f,value)
                                cid = self.AddLocalChange(tid,ChgTypes.ADD,t,f,value,oldVal,oldOwner,oldFeature,oldIndex)
                    elif(IsList(value) and len(value) == len(target) and IsList(value[0]) and len(value[0]) == len(feature) and IsListOfObjects(value[0][0])):
                        for i in range(len(target)):
                            for j in range(len(feature)):
                                for v in value[i][j]:
                                    (oldVal,oldOwner,oldFeature,oldIndex) = self.mdbAccessor.Set(target[i],feature[j],v)
                                    cid = self.AddLocalChange(tid,ChgTypes.ADD,target[i],feature[j],v,oldVal,oldOwner,oldFeature,oldIndex)
                    else:
                        raise EoqError(0,'Error in add: value must be single value or list of list of list of values with outer list having a lenght equal to the number of targets and the middle list with a length equal to the number of features, but got: %s:'%(value)) 
                else:
                    raise EoqError(0,'Error in add: feature must be single object or list of objects but got: %s:'%(feature)) 
            
            else: 
                raise EoqError(0,'Error in add: target must be single object or list of objects but got: %s:'%(target))
            
            res = [target,feature,value]
            self.AddToHistory(res,tid)
        except Exception as e:
            status = ResTypes.ERR
            res = str(e)
        return Res(CmdTypes.ADD,status,res,tid,cid)

        */

        self.ExecSet=async function(args,tid){
            var status = 'OK'
            var res=null;
            var eoqHistory = self.tempHistoryForTransaction[tid] //hack to make history work for the SET command, because transactions are not implemented yet for the commandrunner           
            let tgt =  self.qryEvaluator.Eval(args[0],eoqHistory)
            let ft =  self.qryEvaluator.Eval(args[1],eoqHistory)
            let val =  self.qryEvaluator.Eval(args[2],eoqHistory)

          
          
            //parellelizes the query evaluation
            var target=await tgt
            var feature=await ft
            var value=await val
            if(IsNoList(target))
            {
                if(IsNoList(feature))
                {
                    if(IsNoList(value))
                    {
                        //default case: one target, one feature
                        await self.mdbAccessor.Set(target,feature,value);
                    }
                    else
                    {                      
                        var operations=[];
                        for(let v in value){
                            operations.push(self.mdbAccessor.Set(target,feature,value[v]));
                        }
                        await Promise.all(operations)
                    }
                    
                    
                }
            }else if(IsListOfObjects(target)){ //e.g. [#20,#22,#23]
                //all multiple targets
                if(IsNoList(feature))
                {
                    //multiple targets, all single feature
                    if(IsNoList(value))
                    {
                        //case: multiple target, single feature, single value
                        var operations=[];
                        for(let t in target)
                        {
                            operations.push(self.mdbAccessor.Set(target[t],feature,value));
                        }
                        await Promise.all(operations)
                    }
                    else if(value.length && target.length)
                    {
                        //case: multiple target, single feature, multiple value, with equal list lengths
                        var operations=[];                       
                        for(let t in target)
                        {
                            operations.push(self.mdbAccessor.Set(target[t],feature,value[t]));
                        }
                        await Promise.all(operations)
                    }
                }
            }




            res = [target,feature,value]
            self.AddToHistory(res,tid)
            return res;
        }
/*
       def ExecSet(self,args,tid):
       status = ResTypes.OKY

       #res = None  #[target,feature,oldVal]
       cid = self.latestChangeId
       #eval all arguments
       eoqHistory = self.tempHistoryForTransaction[tid]
       target = self.qryEvaluator.Eval(args[0],eoqHistory)
       feature = self.qryEvaluator.Eval(args[1],eoqHistory)
       value = self.qryEvaluator.Eval(args[2],eoqHistory)
       
       #set the value(s) depending on the multiplicity of the arguments
       if(IsNoList(target)): # e.g. #20
           if(IsNoList(feature)):
               (oldVal,oldOwner,oldFeature,oldIndex) = self.mdbAccessor.Set(target,feature,value)
               cid = self.AddLocalChange(tid,ChgTypes.SET,target,feature,value,oldVal,oldOwner,oldFeature,oldIndex)
           elif(IsListOfObjects(feature)):
               if(IsNoList(value)):
                   for f in feature:
                       (oldVal,oldOwner,oldFeature,oldIndex) = self.mdbAccessor.Set(target,f,value)
                       cid = self.AddLocalChange(tid,ChgTypes.SET,target,f,value,oldVal,oldOwner,oldFeature,oldIndex)
               elif(IsListOfObjects(value) and len(value) == len(feature)):
                   for i in range(len(feature)):
                       (oldVal,oldOwner,oldFeature,oldIndex) = self.mdbAccessor.Set(target,feature[i],value[i])
                       cid = self.AddLocalChange(tid,ChgTypes.SET,target,feature[i],value[i],oldVal,oldOwner,oldFeature,oldIndex)
               else:
                   raise EoqError(0,'Error in set: value must be single value or list of values with equal length of the number of features, but got: %s:'%(value)) 
           else:
               raise EoqError(0,'Error in set: feature must be single object or list of objects but got: %s:'%(feature)) 
       elif(IsListOfObjects(target)): # e.g. [#20,#22,#23]
           if(IsNoList(feature)):
               if(IsNoList(value)):
                   for t in target:
                       (oldVal,oldOwner,oldFeature,oldIndex) = self.mdbAccessor.Set(t,feature,value)
                       cid = self.AddLocalChange(tid,ChgTypes.SET,t,feature,value,oldVal,oldOwner,oldFeature,oldIndex)
               elif(IsListOfObjects(value) and len(value) == len(target)):
                   for i in range(len(t)):
                       (oldVal,oldOwner,oldFeature,oldIndex) = self.mdbAccessor.Set(target[i],feature,value[i])
                       cid = self.AddLocalChange(tid,ChgTypes.SET,target[i],feature,value[i],oldVal,oldOwner,oldFeature,oldIndex)
               else:
                   raise EoqError(0,'Error in set: value must be single value or list of values with equal length of the number of targets, but got: %s:'%(value)) 
           elif(IsListOfObjects(feature)):
               if(IsNoList(value)):
                   for t in target:
                       for f in feature:
                           (oldVal,oldOwner,oldFeature,oldIndex) = self.mdbAccessor.Set(t,f,value)
                           cid = self.AddLocalChange(tid,ChgTypes.SET,t,f,value,oldVal,oldOwner,oldFeature,oldIndex)
               elif(IsList(value) and len(value) == len(target) and IsListOfObjects(value[0]) and len(value[0]) == len(feature)):
                   for t in target:
                       for i in range(len(feature)):
                           (oldVal,oldOwner,oldFeature,oldIndex) = self.mdbAccessor.Set(t,feature[i],value[i])
                           cid = self.AddLocalChange(tid,ChgTypes.SET,t,feature[i],value[i],oldVal,oldOwner,oldFeature,oldIndex)
               else:
                   raise EoqError(0,'Error in set: value must be single value or list of values with equal length of the number of targets, but got: %s:'%(value)) 
           else:
               raise EoqError(0,'Error in set: feature must be single object or list of objects but got: %s:'%(feature)) 
       
       else: 
           raise EoqError(0,'Error in set: target must be single object or list of objects but got: %s:'%(target))
       
       res = [target,feature,value]
       self.AddToHistory(res,tid)
       return res;
*/

    /* Private Methods */

    self.StartTransaction=function()
    {
        self.mdbAccessor.Lock() 
        if($DEBUG) console.debug("Local transaction id="+tid+" started")   
        self.latestTransactionId+=1
        var tid = self.latestTransactionId
        self.tempHistoryForTransaction[tid]=[]
        return tid
    }

    self.EndTransaction=function(res,tid)
    {       
        self.mdbAccessor.Release()    
        if($DEBUG) console.debug("Local transaction id="+tid+" complete")   
    }

    self.AddToHistory=function(value,tid){
        self.tempHistoryForTransaction[tid].push(value)
    }


    /* Evaluator Functor Registration */

    self.cmdEvaluators = {} 
    self.cmdEvaluators[CmdTypes.CMP] = self.ExecCmp
    self.cmdEvaluators[CmdTypes.GET] = self.ExecGet
    self.cmdEvaluators[CmdTypes.CRN] = self.ExecCrn
    self.cmdEvaluators[CmdTypes.ADD] = self.ExecAdd
    self.cmdEvaluators[CmdTypes.SET] = self.ExecSet

      
    
  /*
    self.cmdEvaluators['REM'] = self.ExecRem
    self.cmdEvaluators['MOV'] = self.ExecMov
    self.cmdEvaluators['CLO'] = self.ExecClo
    self.cmdEvaluators['CRT'] = self.ExecCrt
    self.cmdEvaluators['CRN'] = self.ExecCrn
    self.cmdEvaluators['STS'] = self.ExecSts
    self.cmdEvaluators['GMM'] = self.ExecGmm
    self.cmdEvaluators['RMM'] = self.ExecRmm
    self.cmdEvaluators['UMM'] = self.ExecUmm
    self.cmdEvaluators['GAA'] = self.ExecGaa
    self.cmdEvaluators['CHG'] = self.ExecChg
    self.cmdEvaluators['CAL'] = self.ExecCal
    self.cmdEvaluators['ASC'] = self.ExecAsc
    self.cmdEvaluators['ABC'] = self.ExecAbc
    */

}







