var CmdTypes={
    //data related commmands
    GET : 'GET', //get cmd
    SET : 'SET', //set cmd value
    ADD : 'ADD', //add cmd value
    REM : 'REM', //remove cmd value
    MOV : 'MOV', //move cmd cmd
    DEL : 'DEL', //delete cmd
    CLO : 'CLO', //clone source target mode
    CRT : 'CRT', //create by class
    CRN : 'CRN', //create by name
    
    //meta model related commands
    GMM : 'GMM', //get meta models
    RMM : 'RMM', //register meta model
    UMM : 'UMM', //unregister meta model
    
    //maintenance related commands
    HEL : 'HEL', //hello
    GBY : 'GBY', //goodbye
    SES : 'SES', //session
    STS : 'STS', //status
    CHG : 'CHG', //changes
    
    //Action related commands
    GAA : 'GAA', //get all actions
    CAL : 'CAL', //call
    ASC : 'ASC', //async Call
    ABC : 'ABC', //abort call
    CST : 'CST', //call status
    
    CMP : 'CMP' //compound
}