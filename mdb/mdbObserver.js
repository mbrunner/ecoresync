/* ecoreSync ModelDB observer */
/* The ecoreSync mdbObserver API is used to create model listeners. It enables the queryObserver to observer queries on the ecoreSync mdb. It provides the ecoreSync
   functionalities in a EOQ2 compatibile manner. */

/* (C) 2020 Instiute of Aircraft Systems, Matthias Brunner */

function EcoreSyncMdbObserver(ecoreSync)
{
    var self=this;
    self.ecoreSync=ecoreSync;
    self.registry=[];
    self.eventBroker=$app.plugins.require('eventBroker')

    //TODO: way to unregister the observers


    var CallbackHandler=function(cbFunc,interval){

        const delay = interval => new Promise(resolve => setTimeout(resolve,interval));
        var self=this;
        self.cbFunc=cbFunc;
        self.pending=false

        self.callback=async function(){
            
            if(!self.pending)
            {
                self.pending=true;
                await delay(interval)
                cbFunc();
                self.pending=false;
            }         
        };
    }


      /* Model Observers */
    self.Observe=async function(obj,feature,callback){
        /* Observes a feature of an object */
        /* calls the callback function with the new feature value */

        if(!obj) { console.trace();  throw 'cannot observe null object'}

        var decodedObject=await self.ecoreSync.decode(obj)   
        var eClass=await self.ecoreSync.isClassInitialized(decodedObject.eClass)

        if(decodedObject.eClass.getEStructuralFeature(feature))
        {
            if(decodedObject.eClass.getEStructuralFeature(feature).eClass.get("name")=="EAttribute")
            {
                self.ObserveAttribute(decodedObject,feature,callback);
            }

            if(decodedObject.eClass.getEStructuralFeature(feature).eClass.get("name")=="EReference")
            {
                self.ObserveReference(decodedObject,feature,callback);
            }
        }else
        {
            console.error(eClass); 
            console.error('Object #'+decodedObject.get("_#EOQ")+' has no feature='+feature)
        }        
    }

    self.Unobserve=function(obj,feature){
        /*Remove the correct instance !*/
    }

    self.ObserveAttribute=function(obj,feature,callback){
        /* Observes a reference of an object */
        /* calls the callback function with the new attribute value as argument */
        var callbackHandler=new CallbackHandler(function(){ callback(obj.get(feature)); },10);
        let localCallback=function(){  callbackHandler.callback();  }
        self.eventBroker.subscribe('ecore/'+obj.get("_#EOQ")+'/change/'+feature,localCallback);
    }


    self.ObserveReference=function(obj,feature,callback){
        /* Observes a reference of an object */
        /* calls the callback function with the new reference contents as argument */

        var callbackHandler=new CallbackHandler(function(){ 
            var results=null;
            if(obj.eClass.getEStructuralFeature(feature).get("upperBound")>1)
            {
                results=obj.get(feature).array();
                results=results.map(function(e){ 
                    return self.ecoreSync.encode(e);
                })
            }
            else
            {           
                results=self.ecoreSync.encode(obj.get(feature))
            }

            callback(results); },10);
            
        //TODO: remember to do garbage collection in case of REMOVE
        let localCallback=function(){ callbackHandler.callback(); }
        self.eventBroker.subscribe('ecore/'+obj.get("_#EOQ")+'/*/'+feature,localCallback);
    }


}

