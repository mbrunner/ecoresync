/* ecoreSync ModelDB accessor */
/* The ecoreSync mdbAccessor API is used to enable hybrid (local/remote) query evaluation. It enables the query runner to run queries on the ecoreSync mdb. It provides the ecoreSync
   functionalities in a EOQ2 compatibile manner. */

/* The ecoreSync mdbAccessor is based on the pyeoq2 mdbAccessor. The original python code was written by Björn Annighöfer */
/* (C) 2020 Instiute of Aircraft Systems, Matthias Brunner */

function EcoreSyncMdbAccessor(ecoreSync)
{
    var self=this;
    self.ecoreSync=ecoreSync;
    /* META-MODEL ACCESSORS */

    self.Lock=function(){
        console.debug('mdb lock unsupported')
    }

    self.Release=function(){
        console.debug('mdb release unsupported')
    }

    self.GetAllMetamodels=function(){
        console.debug('get all meta model unsupported')
    }

    self.RegisterMetamodel=function(package){
        console.debug('register meta model unsupported')
    }

    self.UnregisterMetamodel=function(package)
    {
        console.debug('unregister meta model unsupported')
    }

    /* MODEL ACCESSORS */

    self.GetRoot=async function(){
        var root=await ecoreSync.getObjectById(0)
        return self.ecoreSync.encode(root)
    }

    self.Get=async function(obj,feature){
        /* Gets the feature contents for an encoded object */
        /* Returns encoded objects */
        if(!obj) { throw 'mdbAccessor Get: object is undefined, feature='+feature}
        var res=null;
        var decodedObject=await self.ecoreSync.decode(obj)   
        if(decodedObject)
        {     
            var value=await self.ecoreSync.get(decodedObject,feature)  

            if(Array.isArray(value))
            {
                res=value.map(function(e){ return self.ecoreSync.encode(e) })
            }
            else
            {
                res=self.ecoreSync.encode(value)
            }
        }
        else
        {
            if(obj.eClass.getEStructuralFeature(feature).get("upperBound")!=1)
            {
                res=[]
            }
        }
        return res
    }

    self.Set=async function(obj,feature,value){
        var decodedObject=await self.ecoreSync.decode(obj)  
        var decodedValue=value;
        if(value.qry=="OBJ"){  decodedValue=await self.ecoreSync.decode(value); } 
        return self.ecoreSync.set(decodedObject,feature,decodedValue)
    }

    self.GetParent=async function(obj){
        var res=null;
        var decodedObject=await self.ecoreSync.decode(obj)  
        var value= await self.ecoreSync.getParent(decodedObject)
        res=self.ecoreSync.encode(value)
        return res
    }

    self.GetAllParent=function(obj){
        console.debug('get all parents unsupported')
    }

    self.GetIndex=function(obj){
        console.debug('get index unsupported')
    }

    self.GetContainingFeature=function(obj){
        console.debug('get containing feature unsupported')
    }

    self.Add=async function(obj,feature,child){
        var decodedObject=self.ecoreSync.proxy.unproxy(await self.ecoreSync.decode(obj))
        var decodedChild=self.ecoreSync.proxy.unproxy(await self.ecoreSync.decode(child))

        return await self.ecoreSync.add(decodedObject,feature,decodedChild)
    }

    self.Remove=function(obj,feature,child){
        return self.ecoreSync.remove(obj,feature,child)
    }

    self.Move=function(obj,newIndex){
        console.debug('move unsupported')
    }

    self.Clone=function(obj,mode){
        return self.ecoreSync.clone(obj)
    }

    self.Create=async function(clazz,n,constructorArgs=[])
    {
        var res=null;
        var response=await self.ecoreSync.create(clazz,n)        
        if(n>1)
        {
            res=response.map(function(r){
                return self.ecoreSync.encode(self.ecoreSync.proxy.unproxy(r));
            })
        }
        else
        {   
            if(n==1){
                res=self.ecoreSync.encode(self.ecoreSync.proxy.unproxy(response));
            }
            else
            {
                res=response //returns []
            }
        }    
        return res;
    }

    self.CreateByName=async function(packageName,className,n,constructorArgs=[])
    {    
        var eClass=await self.ecoreSync.getEClass(packageName,className);
        return await self.Create(eClass,n,constructorArgs);
    }

    self.Class=async function(obj){
        var decodedObject=self.ecoreSync.proxy.unproxy(await self.ecoreSync.decode(obj))
        return decodedObject.eClass
    }

    self.ClassName=async function(obj){
        var decodedObject=self.ecoreSync.proxy.unproxy(await self.ecoreSync.decode(obj))
        return decodedObject.eClass.get("name")
    }

/* CLASS ACCESSORS */

    self.Package=function(clazz){
        return clazz.eContainer
    }

    self.Supertypes=function(clazz){
        console.debug('supertypes unsupported')
    }

    self.AllSupertypes=function(clazz){
        console.debug('supertypes unsupported')
    }

    self.Implementers=async function(clazz){
        //unfortunaetly, we can never know whether the whole meta model is present locally, therefore we have to resort to an external call
        //ecoreSync is currently not able to track whether this query has already been carried out, so we have to do it remotely everytime
        var res= await ecoreSync.remoteExec(new eoq2.Get(new eoq2.Obj(clazz.v).Met('IMPLEMENTERS')));
        return res.v;
    }
    
    self.AllImplementers=async function(clazz){
        //unfortunaetly, we can never know whether the whole meta model is present locally, therefore we have to resort to an external call
        //ecoreSync is currently not able to track whether this query has already been carried out, so we have to do it remotely everytime
        var res= await ecoreSync.remoteExec(new eoq2.Get(new eoq2.Obj(clazz.v).Met('ALLIMPLEMENTERS')));
        return res.v;
    }

    self.GetAllChildrenOfType=async function(obj,className){
        var decodedObject=await self.ecoreSync.decode(obj)       
        var res=await ecoreSync.getAllContents(decodedObject,function(e){ return e.eClass.get("name")==className})
        res=res.map(function(e){ return self.ecoreSync.encode(e) })
        return res
    }

    self.GetAllChildrenInstanceOfClass=function(obj,className){
        console.debug('implementers unsupported')
    }

    self.GetAllFeatures=function(obj){
        return obj.eClass.get("eStructuralFeatures").array()
    }

    self.GetAllFeatureNames=function(obj){
        return obj.eClass.get("eStructuralFeatures").array().map(function(e){ return e.get("name")})
    }

    self.GetAllFeatureValues=function(obj){
        console.debug('feature values unsupported')
    }

    self.GetAllAttributes=function(obj){
        console.debug('attributes values unsupported')
    }
    
    self.GetAllAttributeNames=function(obj){
        console.debug('attributes unsupported')
    }

    self.GetAllAttributes=function(obj){
        console.debug('attributes unsupported')
    }
    
    self.GetAllAttributeNames=function(obj){
        console.debug('attributes unsupported')
    }
        
    self.GetAllAttributeValues=function(obj){
        console.debug('attributes unsupported')
    }

    self.GetAllReferences=function(obj){
        console.debug('references unsupported')
    }
    
    self.GetAllReferenceNames=function(obj){
        console.debug('references unsupported')
    }
        
    self.GetAllReferenceValues=function(obj){
        console.debug('references unsupported')
    }

    self.GetAllContainments=function(obj){
        console.debug('containments unsupported')
    }
    
    self.GetAllContainmentNames=function(obj){
        console.debug('containments unsupported')
    }
        
    self.GetAllContainmentValues=function(obj){
        console.debug('containments unsupported')
    }

}

