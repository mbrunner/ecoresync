// EcoreSync Index Component
// Indexes all EObjects and prevents identical simultaneous queries to the same remote EObject 
// (C) 2019 Matthias Brunner


var EcoreSyncIndex=function(ecoreSync)
{

    var self=this;
    this.__index=[];
    this.__idIndex={};
    this.ecoreSync=ecoreSync;
    this.eventBroker=$app.plugins.require('eventBroker');
     
    this.stats={success:0,fail:0};

    this.query=function(query)
    {
        return  new Promise(function(resolve,reject){
            
            let onSuccess=function(value)
            {
                resolve(value);
            }

            let onFailure=function(error)
            {                
                console.error(error);
                console.error(query)
                reject("Query failed.");
            }

            self.ecoreSync.domain.DoFromStr(query,onSuccess,onFailure);  
            
        });
    };


    var ItemQuery=function(queryStr,callback=null)
    {
        var self=this;
        this.cmd=queryStr;
        this.__complete=false;
        this.__callbacks=[];

        if(callback!=null)
        {
            this.__callbacks.push(callback);
        }

        this.callback=function()
        {
            for(let i in self.__callbacks)
            {
                if(self.__callbacks[i]!=null)
                {
                    self.__callbacks[i].apply();
                }
            }
        }

        this.appendCallback=function(callback)
        {
            self.__callbacks.push(callback);
        }

        this.isComplete=function()
        {
            return self.__complete;
        }

        this.setComplete=function(fireCallbacks=true)
        {
            self.__complete=true;
            if(fireCallbacks)
            {
                self.callback();
            }
        }
    }

    var IndexItem=function(eObject)
    {
        var self=this;
        this.eObject=eObject;
        this.queries=[];
        this.__callbacks=[];

       
        this.findQuery=function(query)
        {
            return self.queries.find(function(e){return e.cmd==query;});
        }

        this.hasQuery=function(query)
        {
            if(self.findQuery(query))
            {
                return true;
            }
            return false;
        }

        this.hasQueries=function()
        {
            if(self.queries.length>0)
            {
                return true;
            }
            return false;
        }

        this.addQuery=function(query,callback=null)
        {
            if(!self.hasQuery(query))
            {
                self.queries.push(new ItemQuery(query,callback));
            }
        }

        this.removeQuery=function(query,fireCallbacks=true)
        {
            if(self.hasQuery(query))
            {
                let queryItem=self.findQuery(query);
                let index=self.queries.indexOf(queryItem);
                self.queries.splice(index,1);
                queryItem.callback();                
            }
        }

        this.setQueryComplete=function(query,fireCallbacks=true)
        {
            let index=self.queries.findIndex(function(e){return e.cmd==query;});
            if(index!=-1)
            {
                self.queries[index].setComplete(fireCallbacks);
            }
        }   

    }


    this.contains=function(eObject,query=null)
    {

        if(!eObject) { console.error(eObject); console.trace() }

        //var object=self.__index.find(function(e){return compareEObjects(e.eObject,eObject);});
        var object=self.__idIndex[eObject.get("_#EOQ")]
        if(object)
        {
            object=self.__index[object.pos]
            if(query==null)
            {
                return true;
            }
            else
            {
                if(object.hasQuery(query))
                {
                    return true;
                }
            }
        }

        return false;
        
    };

    this.containsId=function(objectId,query=null){
        var object=self.getById(objectId)
        if(object) { return true };
        return false
    }

    this.add=function(eObject,query=null,callback=null)
    {       


        if(eObject.get("_#EOQ")===undefined)
        {
            console.trace();
            throw('ecoreSync: Cannot add eObject to the index, because the object id is undefined')
        }

        if(!self.contains(eObject))
        {
            var registerEventListeners=function(eObject){       
                var ignoredEClasses=["eClass","eResource","ePackage"];
                if(!ignoredEClasses.includes(eObject.eClass.get("name")))
                {
                    eObject.on('change',function(change){
                        self.eventBroker.publish('ecore/'+eObject.get("_#EOQ")+'/change/'+change,eObject.get(change));
                    })

                    /*Performance-wise not great, ecoreSync should sync the whole class at first makes more sense in my opinion*/
                    ecoreSync.isClassInitialized(eObject.eClass).then(function(eClass){
                        var features=[];
                       
                        features=features.concat(eClass.get("eAllReferences"))
                        features=features.concat(eClass.get("eAllContainments"))
                                         
                        features.forEach(function(ft){                                               
                            eObject.on('add:'+ft.get("name"),function(change){                                
                                self.eventBroker.publish('ecore/'+eObject.get("_#EOQ")+'/add/'+ft.get("name"),eObject.get(ft.get("name")));
                            })        
                            eObject.on('remove:'+ft.get("name"),function(change){
                                self.eventBroker.publish('ecore/'+eObject.get("_#EOQ")+'/remove/'+ft.get("name"),eObject.get(ft.get("name")));
                            })
                        })    
                                     
                    }).catch(function(e){
                        console.error('failed to install event listeners: '+e)
                    });
                    
                
                }   
            } 
            registerEventListeners(eObject);
            self.__index.push(new IndexItem(eObject));     
            self.__idIndex[eObject.get("_#EOQ")]={eObject:eObject,pos:self.__index.length-1};     
        }

        if(query)
        {
            var indexItem=self.find(function(e){return compareEObjects(e.eObject,eObject);});

            if(!indexItem.hasQuery(query))
            {
                indexItem.addQuery(query,callback); 
            }
            else
            {
                var itemQuery=indexItem.findQuery(query);
                itemQuery.appendCallback(callback);
            }
        }

    };

    this.queryComplete=function(eObject,query,fireCallbacks=true)
    {       
        if(self.contains(eObject))
        {
            var indexItem=self.find(function(e){return compareEObjects(e.eObject,eObject);});

            if(indexItem.hasQuery(query))
            {
                indexItem.setQueryComplete(query,fireCallbacks);
            }    
            self.stats.success+=1;    
        }
    };

    this.queryFail=function(eObject,query,fireCallbacks=true)
    {     
        console.warn("Query failed gracefully on eObject #"+eObject.get("_#EOQ")+" query:"+query+".");  
        if(self.contains(eObject))
        {
            var indexItem=self.find(function(e){return compareEObjects(e.eObject,eObject);});

            if(indexItem.hasQuery(query))
            {
                indexItem.removeQuery(query,fireCallbacks);
            }  
            self.stats.fail+=1;         
        }
    };


    this.listen=function(eObject,query,callback=null)
    {       
        if(callback!=null)
        {
            var ix=self.__idIndex[eObject.get("_#EOQ")]
            var indexItem=null
            if(ix) indexItem=self.__index[ix.pos]
            //var indexItem=self.find(function(e){return compareEObjects(e.eObject,eObject);});

            if(indexItem)
            {
                if(indexItem.hasQuery(query))
                {            
                    var itemQuery=indexItem.findQuery(query);     
                    if(itemQuery.isComplete())
                    {
                        callback.apply();
                    }
                    else
                    {                       
                        itemQuery.appendCallback(callback);
                    }
                }     
            }
        }
    };


    
    this.find=function(func)
    {
        return self.__index.find(func);
    }

    this.filter=function(func)
    {
        return self.__index.filter(func);
    }

    this.get=function(func)
    {
        let item=self.__index.find(func);        

        if(item)
        {
            return item.eObject;
        }     
    }

    this.getById=function(objectId)
    {
        try{
            //var item=self.__index.find(function(e){ return e.eObject.get("_#EOQ")==objectId;});    
            var item=self.__idIndex[objectId]            
        }
        catch(e)
        {
            console.error(e);
            console.error(objectId);
            console.error(self.__index);
        }

        if(item)
        {
            return item.eObject;
        }     
    }

    this.getIndexEObject=function(eObject)
    {
        var indexItem=self.find(function(e){return compareEObjects(e.eObject,eObject);});
        return indexItem.eObject;
    };

    this.getStats=function()
    {
        return {objectsOnIndex: self.__index.length, successfulQueries: self.stats.success, failedQueries: self.stats.fail};
    }
   


    
};


