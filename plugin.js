/*ecoresync plugin for jsapplication */
/*(C) 2020 Institute of Aircraft Systems, Matthias Brunner */

export function init(pluginAPI,config) {  
    
    pluginAPI.expose({"error":"API unavailable"});
    
    let scriptIncludes=[
        "history/history.js",
        "observer/observer.js",
        "auxiliary/jobQueue.js",
        "mirror/mirror.js",
        "proxy/proxy.js",
        "auxiliary/auxiliary.js",
        "mdb/mdb.js",
        "mdb/mdbAccessor.js",
        "mdb/mdbObserver.js",
        "query/query.js",
        "query/queryRunner.js",
        "query/queryObserver.js",
        "query/util.js",
        "command/command.js",
        "command/commandRunner.js",
        "changes/changes.js",
        "auxiliary/buffer.js",
        "debug/debugger.js",
        "ecoreSync.js"
    ];

    let stylesheetIncludes=[
    ];

    return pluginAPI.loadScripts(scriptIncludes).then(function(){ 
        return pluginAPI.loadStylesheets(stylesheetIncludes).then(function(){ 

            let instances = {};
            for(let i=0;i<config.instances.length;i++) {
                let params = config.instances[i];
                let eoq1Domain = pluginAPI.require('eoq2').instances[params.eoq1DomainId];
                let eoq2Domain = pluginAPI.require('eoq2').instances[params.eoq2DomainId];
                let instance = new EcoreSync(eoq1Domain); 
                //ecoreSync.eoq2domain = app.domain;//HACK for smoothly upgrading to EOQ2
                instance.eoq2domain = eoq2Domain;
                //enable all change events
                let cmd = new eoq2.command.Obs('CHG','*');
                eoq2Domain.Do(cmd); //async
                //store the ecoreSyncInstance for later
                instances[params.ecoreSyncId] = instance;
            }

            //expose ecoreSyncInstances
            pluginAPI.expose({
                instances : instances
            })
		});
	});
};

export var meta={
        id:"plugin.ecoreSync",
        description:"Ecore object synchronization via EOQ",
        author:"Matthias Brunner",
        date:"2020-02-10",
        version:"1.0.0",
        config: {
            instances : [
                // {
                //     ecoreSyncId: 'ecoreSync',
                //     eoq2DomainId: 'eoq2domain',
                //     eoq1DomainId : 'eoq1domain'
                // }
            ]
        },
        requires:[
            'eoq2',
            'ecore',
            'eventBroker'
        ]
    };


