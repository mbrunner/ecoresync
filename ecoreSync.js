// EcoreSync
// Synchronizes the local ECore Resources with the remote ECore Resource using EOQ.
// (C) 2019 Matthias Brunner


//Add EOQ object id as attribute to all EObjects
Ecore.EObject.get("eStructuralFeatures").add(Ecore.EAttribute.create({ name: '_#EOQ', eType: Ecore.EInt }));


function rateLimitedQuery(func,interval)
{
	this.action=func;
	this.interval=interval;
	this.timeout=null;
	this.t0=performance.now();
	var self=this;
	this.runRateLimited=function(){ 
					clearTimeout(self.timeout);
					self.timeout=setTimeout(self.runNow,interval);

					};83
	this.runNow=function(){ if(self.timeout!=null){clearTimeout(self.timeout);} self.action.apply();};	
}


function EcoreSync(domain)
{

    var instance=this;
    var self=this;
    this.domain=domain;

    //this.workspace=new WorkspaceProvider(this);

    //EcoreSync modules
    this.index=new EcoreSyncIndex(this);
    this.proxy=new EcoreSyncProxy(this);
    this.mirror=new EcoreSyncMirror(this);
    this.jobQueue=new EcoreSyncJobQueue(this);
    this.changes=new EcoreSyncChanges(this);
    this.history=new EcoreSyncHistory(this);
    this.mdbAccessor= new EcoreSyncMdbAccessor(this);
    this.mdbObserver= new EcoreSyncMdbObserver(this);
    this.cmdRunner= new EcoreSyncCmdRunner(this); 
    this.queryObserver=new EcoreSyncQueryObserver(this);
    this.buffer= new EcoreSyncBuffer(this);
    this.textSerializer=new eoq2.serialization.TextSerializer();
    this.debugger = new EcoreSyncDebugger(this,$app.plugins.require('eventBroker'));
     
    //Instance Resourceset
    this.__resourceSet=Ecore.ResourceSet.create();
    this.resourceSet=new Proxy(this.__resourceSet,instance.proxy.EObject());
    this.__resourceSet.on('change:resources',function(changed) {
        self.__resourceSet.__updateContents=true; // force update for eContents        
    });

    this.ProxyResource=function(resource){ if(resource!=undefined){ return new Proxy(resource,instance.proxy.EResource()); }else{ return resource; }};
    this.getProxiedObject=function(EObject){        
        
        if(EObject!=undefined){ 
            if(EObject.isProxy)
            {
                return EObject;
            }
            else
            {
                return new Proxy(EObject,instance.proxy.EObject());
            }
         }      
          
         return EObject;
    };

    this.findRoot=function(eObject)
    {
        if(eObject.isProxy){ eObject=eObject.noproxy; };

        if(eObject.eContainer)
        {
            if(eObject.eContainer.eClass.get("name")!="ModelResource")
            {
                return self.findRoot(eObject.eContainer);
            }
            else
            {
                return instance.getProxiedObject(eObject);
            }
        }
        else
        {            
            return instance.getProxiedObject(eObject);
        }
    };

    this.findResource=function(objectId)
    {
        return instance.__resourceSet.get('resources').find(function(e){return e.get('_#EOQ')===objectId});
    };
    
    this.getResource=function(eObject)
    {
        let root=self.proxy.unproxy(self.findRoot(eObject));
        return root.eContainer        
    };


    this.getObjectById=function(id)
    {
        //The object is returned from the index if it is found there, otherwise it is synchronized
        //from the remote server. No attributes/references are synchronized. The synchronized
        //object is put in the index for future tie-in.
              
            return new Promise(function(resolve,reject){               
                
                let indexObject=self.index.getById(id);
                if(indexObject)
                {                     
                    resolve(self.getProxiedObject(indexObject));
                }
                else
                {                      
                    //let query="RETRIEVE #"+id+" /eClass";   
                    let cmd = CMD.Get(QRY.Obj(id).Met('CLASS')); 
                    self.eoq2domain.Do(cmd).then(function(val){
                        let __eClass=self.find(function(e){ return e.get("_#EOQ")==val.v});  
                        if(!__eClass)
                        {                                                          
                            __eClass=Ecore.EClass.create();
                            __eClass.get("eStructuralFeatures").add(Ecore.EAttribute.create({ name: '_#EOQ', eType: Ecore.EInt }));
                            __eClass.set("_#EOQ",val.v);                                                              
                        }
                        self.__initClass(__eClass);   
                        var eObject=__eClass.create();
                        eObject.set("_#EOQ",id);
                       
                        self.index.add(eObject);                        
                        resolve(self.getProxiedObject(eObject));
                    }).catch(function(e) {
                        reject('ecoreSync: Could not synchronize object #'+id+'. Bad Id?');
                    })              
                }
            });       
    };

    //BA: added
    this.getObjectStringPath=function(eObject)
    {
        let self = this;
        return new Promise(function(resolve,reject) {
            var id = eObject.get("_#EOQ");
            if(0==id) {
                resolve("");
            } else {
                //return the path to the object
                let cmd = CMD.Cmp()
                    .Get(QRY.Obj(id).Met("INDEX"))
                    .Get(QRY.Obj(id).Met("CONTAININGFEATURE"))
                    .Get(QRY.His(-1).Pth("name"))
                    .Get(QRY.His(-2).Pth("upperBound"))
                    .Get(QRY.Obj(id).Met("ALLPARENTS").Idx([1,-1,1]))
                    .Get(QRY.His(-1).Met("INDEX"))
                    .Get(QRY.His(-2).Met("CONTAININGFEATURE"))
                    .Get(QRY.His(-1).Pth("name"))
                    .Get(QRY.His(-2).Pth("upperBound"))

                self.eoq2domain.Do(cmd).then(function(val) {
                    var pathSegments = [];
                    //do the container segments first
                    var n = val[4].length;
                    for(var i=0;i<n;i++){ 
                        var index = val[5][i];
                        var featureName = val[7][i];
                        var upperBound = val[8][i];
                        var segmentStr = featureName;
                        if(upperBound!=1) {
                            segmentStr += '.'+index;
                        }
                        pathSegments.push(segmentStr);
                    }
                    //the last segment need special care 
                    {
                        var index = val[0];
                        var featureName = val[2];
                        var upperBound = val[3];
                        var segmentStr = featureName;
                        if(upperBound!=1) {
                            segmentStr += '.'+index;
                        }
                        pathSegments.push(segmentStr);
                    }
                    var pathStr = pathSegments.join('/');
                    resolve(pathStr);
                }).catch(function(e) {
                    reject(e);
                });
            }
        });
    };


    //BA: This function was repaired for EOQ2. However, resources have no meaning any more, therefore it should be removed in future
    this.getObjectResource=function(eObject) {
        let self = this;
        return new Promise(function(resolve,reject) {
            let id = eObject.get("_#EOQ");
            //return the path to the object
            //let cmd = jseoq.CommandParser.StringToCommand('RETRIEVE #'+id+' @RESOURCE');
            let cmd = new eoq2.Get(new eoq2.Qry().Obj(id).Met("ALLPARENTS").Sel(new eoq2.Qry().Met("CLASSNAME").Equ("ModelResource")).Idx(0));
            self.eoq2domain.Do(cmd).then(function(val){
                let resourceId = val.v;
                self.getObjectById(resourceId).then(function(resourceEObject) {
                    resolve(resourceEObject);
                });
            }).catch(function(e) {
                reject(new e);;
            });
        });
    };

    this.getObjectShortPath=function(eObject) {
        let self = this;
        return new Promise(function(resolve,reject) {
            let id = eObject.get('_#EOQ');
            // let cmd = jseoq.CommandParser.StringToCommand("RETRIEVE #"+id+" /eContainer<-1>\n"+
            //                                             "RETRIEVE $-1 /name");

            let cmd = new eoq2.Cmp().Get(new eoq2.Qry().Obj(id).Met("ALLPARENTS"))
                                      .Get(new eoq2.Qry().His(-1).Pth("name"));
            //let self = this;
            self.eoq2domain.Do(cmd).then(function(val) {
                let pathSegments = [];
                for(let j=0;j<val[0].length;j++) {
                    let segmentName = val[1][j];
                    let segmentId = val[0][j].v;
                    pathSegments.push(segmentName?segmentName:'#'+segmentId);
                }
                let pathStr = pathSegments.join('/')
                resolve(pathStr);
            }).catch(function(e) {
                reject(e);
            });
        });
    };

    this.getAllObjectsFromRootToById=function(id) { //the root is the first model element within the resource
        let self = this;
        return new Promise(function(resolve,reject) {
            // let cmd = jseoq.CommandParser.StringToCommand("RETRIEVE #"+id+" @RESOURCE\n"+
            //                                               "RETRIEVE #"+id+" @CONTAININGFEATURENAME\n"+
            //                                               "RETRIEVE #"+id+" /eContainer<-1>\n"+
            //                                               "RETRIEVE $-1 @CONTAININGFEATURENAME");

            let cmd = new jseoq.Cmp().Get(new eoq2.Qry().Obj(id).Met("CONTAININGFEATURE").Pth("name"))
                                     .Get(new eoq2.Qry().Obj(id).Met("ALLPARENTS"))
                                     .Get(new eoq2.Qry().His(-1).Met("CONTAININGFEATURE").Pth("name"));

            self.eoq2domain.Do(cmd).then(function(val) {
                //update the content of the autocomplete box
                //if(jseoq.ResultParser.IsResultOk(result)) {
                    // let resourceId = result.results[0].value.v;
                    // let idChain = [id];
                    // for(let i=0;i<result.results[2].value.v.length;i++) {
                    //     idChain.push(result.results[2].value.v[i].v);
                    // }
                    // let featureChain = [result.results[1].value.v];
                    // for(let i=0;i<result.results[3].value.v.length;i++) {
                    //     featureChain.push(result.results[3].value.v[i].v);
                    // }

                    let idsToResolve = [];
                    let nSegmentsToResource = val[1].length;
                    for(let i=1;i<nSegmentsToResource-1;i++) {//do not resolve the first one since the root has no containing feature
                        let pid = val[1][0].v; //parentId
                        idsToResolve.push(pid);
                    }
                    idsToResolve.push(id);

                    let featuresToResolve = [];
                    for(let i=1;i<nSegmentsToResource-1;i++) { //do not resolve the first one since the root has no containing feature
                        let f = val[2][0]; //parentId
                        featuresToResolve.push(f);
                    }
                    featuresToResolve.push(val[0].v);

                    let objectPomises = [];
                    for(let i=0;i<idsToResolve.length;i++) {
                        let idToResolve = idsToResolve[i];
                        objectPomises.push(self.getObjectById(idToResolve));
                    }
                    Promise.all(objectPomises).then(function(eObjects){
                        let featurePromises = [];
                        let orderedEObjects = Array.from(eObjects);
                        for(let i=0;i<eObjects.length;i++) {
                            let eObject = eObjects[i];
                            let idToResolve = eObject.get('_#EOQ');
                            let featureIndex = idsToResolve.findIndex(function(e) {
                                return e==idToResolve;
                            });
                            if(0==featureIndex) {
                                orderedEObjects[featureIndex] = eObject;
                            } else { 
                                let featureToResolve = featuresToResolve[featureIndex-1];
                                //promise returns can be in any order so sort them back to the initial order. 
                                //-1 because feature array has one element less than object array, 
                                //since the first one has no feature to load
                                orderedEObjects[featureIndex] = eObject; 
                                featurePromises.push(self.isFeatureInitialized(eObject,featureToResolve));
                            }
                        }
                        Promise.all(featurePromises).then(function(unused){
                            resolve(orderedEObjects);
                        },function(error){
                            reject(error);
                        });
                    },function(error){
                        reject(error);
                    });
            }).catch(function(e) {
                reject(e);
            });
        })
    };
    //BA: end added

    this.__getResource=function(uri)
    {
        return instance.__resourceSet.get('resources').find(function(e){return e.get('uri')===uri});
    }


    this.batchGet=new rateLimitedQuery(function(){self.__batchGet.apply();},1);


    this.__indexObjectInstance=function(eObject,eClass)
    {
        if(!self.index.contains(eObject))
        {
            self.index.add(eObject);
        }
        else
        {            
            eObject=self.index.getIndexEObject(eObject);            
        }

        return eObject;
    }

    this.__updateReference=function(EObject,featureName,callback=null)
    {
        // initializes the contents of a reference
            
        //    let query="RETRIEVE #"+EObject.get("_#EOQ")+" /"+featureName+"\n"+
        //              "RETRIEVE $0 /eClass\n"+
        //              "RETRIEVE $1 /name\n"+
        //              "RETRIEVE #"+EObject.get("_#EOQ")+" @CLASS@PACKAGE\n"+
        //              "RETRIEVE #"+EObject.get("_#EOQ")+" @CLASS@PACKAGE/nsURI";

        //    let oid = EObject.get("_#EOQ");
        //    let cmd = new eoq2.Cmp().Get(new eoq2.Qry().Obj(oid).Met("CLASS").Met("PACKAGE"))
        //                              .Get(new eoq2.Qry().Obj(oid).Met("REFERENCES").Sel(new eoq2.Qry().Pth("name").Equ(featureName)).Idx(0))
        //                              .Get(new eoq2.Qry().His(-1).Pth("eType"))
        //                              .Get(new eoq2.Qry().His(-1).Met("ALLIMPLEMENTERS"))
        //                              .Get(new eoq2.Qry().His(-1).Pth("name"))


            let oid = EObject.get("_#EOQ");
            if(oid==undefined) {
                console.log("I should not be here");
            }
            let cmd = new eoq2.Cmp().Get(new eoq2.Qry().Obj(oid).Arr([new eoq2.Qry().Pth(featureName)]).Sel(new eoq2.Qry().Neq(null)))
                                    .Get(new eoq2.Qry().His(-1).Met("CLASS"))
                                    .Get(new eoq2.Qry().His(-1).Pth("name"))
                                    .Get(new eoq2.Qry().Obj(oid).Met("CLASS").Met("PACKAGE"))

            //let query = this.textSerializer.Ser(cmd);
            let query = featureName;
           
           //Update of reference after query succeeded 

        


           let onSuccess=function(val){  
           

                //BA: changed from ecoreSync global to this in findIn(...
                var __ePackage=self.__findIn(self.__resourceSet,val[3].v);
                if(!__ePackage)
                {
                    __ePackage=Ecore.EPackage.create();
                    __ePackage.set("_#EOQ",val[3].v);
                    self.__initPackage(__ePackage);
                }
                

                //Check if feature is actually present                
                if(EObject.eClass.get("eAllContainments").find(function(e){return e.get("name")==featureName})!=undefined || EObject.eClass.get("eAllReferences").find(function(e){return e.get("name")==featureName})!=undefined)
                {                    
                    //if(value[0][2]) //check if feature is empty //BA: single valued features can be empty!!!
                    //{
                        if(Array.isArray(val[0][0]))  //double array                 
                        {
                                //multiple objects are contained     
                                var __eClass=null;                 
                                for (let i in val[0][0])
                                {                               
                                    //BA: why is here no try block around the findIn, but below it is??
                                    __eClass=self.__findIn(instance.__resourceSet,val[1][0][i].v);  
                                    if(!__eClass)
                                    {                                                          
                                        __eClass=Ecore.EClass.create();
                                        __eClass.get("eStructuralFeatures").add(Ecore.EAttribute.create({ name: '_#EOQ', eType: Ecore.EInt }));
                                        __eClass.set("_#EOQ",val[1][0][i].v);                                                              
                                    }

                                    self.__initClass(__eClass);                               
                                                    

                                //Create object instance
                                var object=__eClass.create();
                                object.set("_#EOQ",val[0][0][i].v);
                                object=self.__indexObjectInstance(object,__eClass);
                            
                                
                                if(EObject.get(featureName).find(function(e){return e.get('_#EOQ')===val[0][0][i].v;})==undefined)
                                {                           
                                    EObject.get(featureName).add(object);                                   
                                }      
                              

                                
                                }

                            EObject.get(featureName)["_isInitialized"]=true;
                        }
                        else
                        {
                            //single object is contained  
                            if(val[0][0]) {   
                                var __eClass=null;       
                                try{                         
                                __eClass=self.__findIn(instance.__resourceSet,val[1][0].v); 
                                }
                                catch(error)
                                {
                                    if($DEBUG) console.debug(error);
                                    if($DEBUG) console.debug(value);                                
                                }              
                                if(!__eClass)
                                {
                                    __eClass=Ecore.EClass.create();
                                    __eClass.get("eStructuralFeatures").add(Ecore.EAttribute.create({ name: '_#EOQ', eType: Ecore.EInt }));
                                    __eClass.set("_#EOQ",val[1][0].v);               
                                }    
                        
                                    self.__initClass(__eClass);
                                
            
                                //Create object instance
                                var object=__eClass.create();
                                object.set("_#EOQ",val[0][0].v);
                                object=self.__indexObjectInstance(object,__eClass);
                            

                                //if(!EObject.get(featureName) || EObject.get(featureName).get('_#EOQ')!=value[0][2].v) {
                                EObject.set(featureName,object);
                                //}


                            } else {
                                EObject.unset(featureName);
                            }
                            /*
                            if(EObject.get(featureName).array().find(function(e){return e.get('_#EOQ')===value[0][2].v;})==undefined)
                            {
                            EObject.get(featureName).add(object);     
                            }   
                            */                      

                            //BA: the following is not necessary for single valued references
                            //EObject.get(featureName)["_isInitialized"]=true;
                        }                
                    //}
                    //else if (value[0][2]) //check if there is any object
                    //{
                        //In this case the local model and remote model are out of sync
                        //A Function for verifying the current local model against the remote model may be added                    
                    //}       
                }
                else
                {
                    //feature was empty
                }             
         

                self.index.queryComplete(EObject,query);                 
            };

            let onFailure=function(msg){         
                //If the query fails, the reference is assumed to be empty (e.g. if the first part of the query returns % the query will result in a failure)              
                if($DEBUG) console.error("Query failed. #"+oid+" Original query:"+query+" Error: "+msg);
                if($DEBUG) console.error(EObject)
                if($DEBUG) console.error(cmd);
                EObject.get(featureName)["_isInitialized"]=true;
                self.index.queryFail(EObject,query);                                    
            };


            //Initialization Conditions
            if(self.index.contains(EObject))
            {
               
                if(!self.index.contains(EObject,query))
                {
                    self.index.add(EObject,query,callback);
                    this.eoq2domain.Do(cmd).then(function(val) {
                        onSuccess(val);
                    }).catch(onFailure);
                }
                else
                {
                    self.index.listen(EObject,query,callback);
                }
            }     
        
    
    };

    this.pendingGetRequests=[];

    this.resolveContainment=function(eObject)
    {
        if(eObject.eContainer)
        {
             return self.resolveContainment(eObject.eContainer);
        }
        
        if(eObject.get("_#EOQ")==0)
        {
            return Promise.resolve(eObject);
        }

        return new Promise(function(resolve,reject){

            let oid = eObject.get("_#EOQ");

            let cmd = CMD.Cmp()
                .Get(QRY.Obj(oid).Met("PARENT"))
                .Get(QRY.Obj(oid).Met("CONTAININGFEATURE").Pth("name"));

            self.eoq2domain.Do(cmd).then(function(val) {
                self.getObjectById(val[0].v).then(function(object){
                    self.isClassInitialized(object.eClass).then(function(){     
                        self.isFeatureInitialized(object,val[1]).then(function() {                    
                            resolve(object);
                        });
                    });
                });
            }).catch(function(e) {
                reject(e);
            });

        }).then(function(object){
            return self.resolveContainment(object);
        });      
    }

    this.__getObjectId=function(EObject)
    {
        return "#"+EObject.get("_#EOQ").toString();
    }

    this.__get=function(eObject,name,callback=null)
    {      
        //This function gets an attribute from the remote server eObject.
        //If not already queryable, the query will delay until the object id is updated.
        //This function will only work with attribute features. See function __updateReference
        //to query reference and containment features.

        self.isQueryable(eObject).then(function(){
            //let query="RETRIEVE #"+eObject.get("_#EOQ")+" /"+name;      
            
            let oid = eObject.get("_#EOQ");
            let cmd = CMD.Get(QRY.Obj(oid).Pth(name));
            let query = name;

            if(!self.index.contains(eObject,query)) {
                self.index.add(eObject,query,callback);
                self.eoq2domain.Do(cmd).then(function(val) {
                    self.__setAttributeInitialized(eObject,name); 
                    eObject.set(name,val); 
                    self.index.queryComplete(eObject,query);      
                }).catch(function(e) {
                    throw e;
                });
            } else {
                self.index.listen(eObject,query,callback);
            }
        });
    };

    this.__batchGet=function()
    {
        //var queryStr="";
        let cmd = CMD.Cmp();
        var queryIndex=[];
        alert('Batch Get!');
        for(let i in self.pendingGetRequests)
        {
            if(self.pendingGetRequests[i].state==0) {
                self.pendingGetRequests[i].state=1;
            //    if(queryIndex.length!=0)
            //    {
            //        queryStr+="\n";                
            //    }
            cmd.Get(QRY.Obj(self.pendingGetRequests[i].target).Pth(self.pendingGetRequests[i].name));
            //queryStr+="RETRIEVE "+self.__getObjectId(self.pendingGetRequests[i].target)+" /"+self.pendingGetRequests[i].name;
            queryIndex.push(self.pendingGetRequests[i]);
            } 
        }

        if(queryIndex.length>0) {
            //let modelQuery=new Query(queryStr);
            //modelQuery.onSuccess=function(value){  
            self.eoq2domain.Do(function(val){
                for (let i in queryIndex) {                      
                    if(queryIndex.length>1) {                                            
                       queryIndex[i].target.set(queryIndex[i].name,val[i]);
                    } else { 
                       queryIndex[i].target.set(queryIndex[i].name,val[2]);
                    }
                    self.__setAttributeInitialized(queryIndex[i].target,queryIndex[i].name);       
                    if(queryIndex[i].target.eResource()) {   
                        queryIndex[i].target.eResource().trigger("change",queryIndex[i].target);
                    }
                    //console.log(queryStr);
                    self.pendingGetRequests.splice(self.pendingGetRequests.findIndex(function(o){ 
                        return self.__getObjectId(o.target)==self.__getObjectId(queryIndex[i].target) && o.name==queryIndex[i].name;
                    }), 1);	                    
                }
            });
        }
    }
    
    this.__refreshResourceSet=function(resourceSet,callback=null)
    {
        let cmd = CMD.Get(QRY.Obj(0).Met("FEATURENAMES"));

        self.eoq2domain.Do(cmd).then(function(val) {
            for (let i in val) {	 
                resourceSet.create({ uri: val[i]});
            }            
            resourceSet.set("_#EOQ","0");
            resourceSet.trigger('change:resources');  	

            if(callback!=null) {
                setTimeout(callback, 0);
            }						
        }); 
    };

    this.find=function(func)
    {
        var result=self.index.find(function(e){
            return func(e.eObject);
        });
        if(result) {
            return result.eObject;
        }
    };

    this.filter=function(func)
    {
        var filteredObjects=[];
        var result=self.index.filter(function(e){
            return func(e.eObject);
        });   
        for(let i in result)
        {
            filteredObjects.push(result[i].eObject);
        }
        return filteredObjects;
    };

    this.__findAll=function(EObject,cond)
    {
        //Find Objects contained in EObject satisfying condition function cond
        var found=[];  
        if(EObject)
        {      
            found=EObject.eContents().filter(cond);
    
            // Search in Object contents
            var contents=EObject.eContents();
            var subFound;
            for(let i in contents)
            {
                subFound=self.__findAll(contents[i],cond);
                found=_.union(found,subFound);
            }
        }
        return found;
        
    };

    this.__findIn=function(EObject,id,includeCache=true)
    {
        var found;
        if(EObject!=undefined)
        {
             if(includeCache)
             {
                found=self.index.get(function(c){ return c.eObject.get("_#EOQ")==id; });
             }

             if(!found)
             {
                found=EObject.eContents().find(function(e){return e.get('_#EOQ')===id});
                if(!found)
                {
                    for(let i in EObject.eContents())
                    {
                        found=instance.__findIn(EObject.eContents()[i],id);
                        if(found)
                        {
                            break;
                        }
                    }
                }
            }
        }
        return found;
    }


    this.__findConcreteClasses=function(EClass,includeCache=true)
    {    
        let concreteClasses=instance.filter(function(e){ if(e.has("eSuperTypes")) { return e.get("eSuperTypes").contains(EClass);}else{ return false; }});               
        return concreteClasses;
    };

    this.getFeatureDefinition=function(eObject,featureName)
    {
        let featureDefinition = eObject.eClass.get("eAllContainments").find(function(e){return e.get("name")==featureName;});
        if(!featureDefinition) {
            featureDefinition = eObject.eClass.get("eAllReferences").find(function(e){return e.get("name")==featureName;});
        }
        return featureDefinition;
    };

    this.featureToArray=function(eObject,featureName)
    {
        let featureContents=[];
        let feature=eObject.get(featureName);
        let featureDefinition=eObject.eClass.getEStructuralFeature(featureName);//self.getFeatureDefinition(eObject,featureName);
        if(feature && featureDefinition)
        {
            if(featureDefinition.get('upperBound')==1) {                
                featureContents = [feature];                
            } else {
                featureContents = feature.array();
            }               
        }
        return featureContents;
    };

    this.__setAttributeInitialized=function(EObject,attributeName)
    {

        if(typeof EObject["_attributeInitialized"] === "object" && (EObject["_attributeInitialized"] != null))
        {
            EObject["_attributeInitialized"][attributeName]=true;    
        }
        else
        {
            //Initialize Attribute Status Object
            EObject["_attributeInitialized"]={};
            var attr=EObject.eClass.get("eAllAttributes");
            for( let i in attr)
            {
                if(attr[i].get("name")!="_#EOQ")
                {
                    if(EObject["_attributeInitialized"][attr[i].get("name")]==undefined)
                    {
                        EObject["_attributeInitialized"][attr[i].get("name")]=false;
                    }
                }
            }

            EObject["_attributeInitialized"][attributeName]=true;   
        }
    };

    this.isQueryable=function(eObject)
    {
        //Resolves whether an eObject has an object Id (_#EOQ)
        //For locally created eObjects an _#EOQ is initially missing. 
        //Once the remote server has created the eObject the subsequent execution proceeds.
        return new Promise(function(resolve,reject) {
            if(eObject) {
                if(eObject.isProxy) {
                    eObject=eObject.noproxy;
                }                          
                if(eObject.get("_#EOQ")!=undefined && eObject.get("_#EOQ")!=null) {
                    resolve(eObject);
                } else {         
                    var objectIdUpdated=function(changed){                
                        if(changed=="_#EOQ"){
                            eObject.off('change',objectIdUpdated); 
                            resolve(eObject);                                
                        }                        
                    };   
                    eObject.on('change',objectIdUpdated); 
                }
            } else {
                reject("ecoreSync: Invalid eObject supplied.");                    
            }
        });    
    };

    this.isClassInitialized=function(eClass,fullInit=true)
    {
        //resolves once the eObject's eClass has been initialized
        //note: this function call triggers the initialization of the eClass
        return new Promise(function(resolve,reject) {          
            if(eClass) {              
                //Call of legacy function to be removed in future releases
                self.__initClass(eClass,fullInit,function(){
                    resolve(eClass);
                });               
            } else {
                reject("ecoreSync: Invalid eClass supplied.");                    
            }
        }); 
    };


    this.isReferenceInitialized=function(eObject,feature) {
        //resolves once the eObject's reference has been initialized
        //note: this function call triggers the initialization of the reference
        return new Promise(function(resolve,reject) {
            
            self.isClassInitialized(eObject.eClass).then(function(){
                if(eObject) {
                    if(eObject.isProxy) {
                        eObject=eObject.noproxy;
                    }
                    //Call of legacy function to be removed in future releases
                    self.__updateReference(eObject,feature,function(){ 
                        resolve(feature); 
                    });               
                } else {                  
                    reject("ecoreSync: Invalid eObject supplied.");                    
                }
            });

        });
    };

    this.isAttributeInitialized=function(eObject,attribute) {
        //resolves once the eObject's attribute has been initialized
        //note: this function call triggers the initialization of the attribute
        return new Promise(function(resolve,reject){
            self.isClassInitialized(eObject.eClass).then(function() {
                if(eObject) {
                    if(eObject.isProxy) {
                        eObject=eObject.noproxy;
                    }
                    //Call of legacy function to be removed in future releases                                    
                    self.__get(eObject,attribute,function(){
                        resolve(attribute);
                    });                 
                } else {
                    reject("ecoreSync: Invalid eObject supplied.");                    
                }
            });

        });
    };


    this.isFeatureInitialized=function(eObject,feature) {
        //resolves once the eObject's feature has been initialized
        //note: this function call triggers the initialization of the feature
        //ecoreSync discriminates between attributes and references automatically

        return new Promise(function(resolve,reject){   
            self.isQueryable(eObject).then(function(){        
                self.isClassInitialized(eObject.eClass).then(function(){
                    if(eObject) {
                        if(eObject.isProxy) {
                            eObject=eObject.noproxy;
                        }
                        //Call of legacy function to be removed in future releases               
                        if(eObject.eClass.getEStructuralFeature(feature).eClass.get("name")=="EAttribute") {                        
                            self.__get(eObject,feature,function(){
                                resolve(feature); 
                            }); 
                        }

                        if(eObject.eClass.getEStructuralFeature(feature).eClass.get("name")=="EReference") {                             
                            self.__updateReference(eObject,feature,function(){
                                resolve(feature); 
                            });                         
                        }             
                    } else {
                        reject("ecoreSync: Invalid eObject supplied.");                    
                    }
                });
            });
        });
    };

    this.allFeaturesInitialized=function(eObject) {
        return new Promise(function(resolve,reject) {

           

            ecoreSync.isClassInitialized(eObject.eClass).then(function() {

            
                var featuresInitialized=[];
                var eStructuralFeatures=eObject.eClass.get("eStructuralFeatures").array();
                for(let i in eStructuralFeatures) {
                    if(eStructuralFeatures[i].get("name")!="_#EOQ") {                      
                        featuresInitialized.push(self.isFeatureInitialized(eObject,eStructuralFeatures[i].get("name")))
                    }
                }
                Promise.all(featuresInitialized).then(function(values){                    
                    resolve(values);
                }).catch(function(e) {
                    reject(e);
                });
            });
        });
    }

    this.syncOptions=function(attributes=true,references=true,recurseContainments=true,recurseReferences=false,limitToResource=true)
    {
        this.attributes=attributes;
        this.references=references;
        this.recurseReferences=recurseReferences;
        this.recurseContainments=recurseContainments;      
        this.limitToResource=limitToResource;
    };

    this.isPackageInitialized=function(ePackage)
    {

    }

    this.__getAttr=function(eObject,name)
    {      
        //This function gets an attribute from the remote server eObject.
        //If not already queryable, the query will delay until the object id is updated.
        //This function will only work with attribute features. See function __updateReference
        //to query reference and containment features.

        
        return self.isQueryable(eObject).then(function(){
            return new Promise(function(resolve,reject){

                let oid = eObject.get("_#EOQ");
                let cmd = CMD.Get(QRY.Obj(oid).Pth(name));
                //let query="RETRIEVE #"+eObject.get("_#EOQ")+" /"+name;    
                let query = name;
                
                if(!self.index.contains(eObject,query)) {
                    self.index.add(eObject,query,null);
                    self.eoq2domain.Do(cmd).then(function(val){
                        self.__setAttributeInitialized(eObject,name); 
                        eObject.set(name,val); 
                        resolve(eObject.get(name));
                        self.index.queryComplete(eObject,query);   
                    }).catch(function(e){
                        reject(e); 
                    });
                } else {
                    self.index.listen(eObject,query,function(){ 
                        resolve(eObject.get(name)); 
                    });
                }
            });
        });
            
    };
    
    //TODO: clean up the mess in this function
    this.__getRef=async function(EObject,featureName)
    {
        if(!featureName || featureName=='') throw 'a valid feature name must be supplied'
        var query = featureName;
        //Initialization Conditions
        if(self.index.contains(EObject))
        {
         
            if(!self.index.contains(EObject,query))
            {
              
                self.index.add(EObject,query,null);          
                try{                 
                    let oid = EObject.get("_#EOQ");
                    let cmd = CMD.Cmp()
                        .Get(QRY.Obj(oid).Pth(featureName))
                        .Get(QRY.His(0).Trm().Met("CLASS"))
                        .Get(QRY.His(1).Trm().Pth("name"))
                        .Get(QRY.Obj(oid).Met("CLASS").Met("PACKAGE"))
                        .Get(QRY.His(-1).Pth("nsURI"));

                    var val=await self.eoq2domain.Do(cmd)
                    
                                
                    var __ePackage=self.__findIn(self.__resourceSet,val[3].v);
                    if(!__ePackage)
                    {
                        __ePackage=Ecore.EPackage.create();
                        __ePackage.set("_#EOQ",val[3].v);
                        self.__initPackage(__ePackage);
                    }
                    

                    //Check if feature is actually present                
                    if(EObject.eClass.get("eAllContainments").find(function(e){
                        return e.get("name")==featureName}
                        )!=undefined || EObject.eClass.get("eAllReferences").find(function(e){
                            return e.get("name")==featureName
                    })!=undefined)
                    {                    
                        //if(value[0][2]) //check if feature is empty //BA: single valued features can be empty!!!
                        //{
               
                            var ft=EObject.eClass.getEStructuralFeature(featureName);                    
                            var upperBound=ft.get("upperBound")


                            if(upperBound!=1)
                            {
                                //many         
                                                                    
                                var __eClass=null;                 
                                for (let i in val[0])
                                {           
                                                
                                        //BA: why is here no try block around the findIn, but below it is??
                                        __eClass=self.__findIn(instance.__resourceSet,val[1][i].v);  
                                        if(!__eClass)
                                        {                                                          
                                            __eClass=Ecore.EClass.create();
                                            __eClass.get("eStructuralFeatures").add(Ecore.EAttribute.create({ name: '_#EOQ', eType: Ecore.EInt }));
                                            __eClass.set("_#EOQ",val[1][i].v);                                                              
                                        }

                                        self.__initClass(__eClass);                                                              

                                        //Create object instance
                                        var object=self.index.getById(val[0][i].v)
                                        if(!object)
                                        {
                                            object=__eClass.create();
                                            object.set("_#EOQ",val[0][i].v);
                                            object=self.__indexObjectInstance(object,__eClass);
                                        }
                                    
                                 
                                        try{
                                            
                                            if(EObject.get(ft).find(function(e){return e.get('_#EOQ')===val[0][i].v;})==undefined)
                                            {                           
                                                EObject.get(ft).add(object);                                   
                                            }      
                                            
                                        }
                                        catch(e)
                                        {
                                            console.error('Failed to add object ('+i+') to feature:'+featureName+',error='+e)
                                            console.error(object);
                                        }
                                        

                                        
                                }

                                try{
                                    EObject.get(featureName)["_isInitialized"]=true;
                                }
                                catch(e)
                                {
                                    console.error('failed to add initialization state to feature:'+e);
                                }                               
                                
                                
                            }
                            else
                            {
                                //one
                             
                                if(val[0]) {   
                                    var __eClass=null;       
                                    try{                         
                                    __eClass=self.__findIn(instance.__resourceSet,val[1].v); 
                                    }
                                    catch(error)
                                    {
                                        if($DEBUG) console.debug(error);
                                        if($DEBUG) console.debug(value);                                
                                    }              
                                    if(!__eClass)
                                    {
                                        __eClass=Ecore.EClass.create();
                                        __eClass.get("eStructuralFeatures").add(Ecore.EAttribute.create({ name: '_#EOQ', eType: Ecore.EInt }));
                                        __eClass.set("_#EOQ",val[1].v);               
                                    }    
                            
                                        self.__initClass(__eClass);

                                        //Create object instance
                                        var object=__eClass.create();
                                        object.set("_#EOQ",val[0].v);
                                        object=self.__indexObjectInstance(object,__eClass);
                                        EObject.set(featureName,object);
                                }
                                else
                                {
                                        EObject.unset(featureName);
                                }
                            }

                          
                        //}
                        //else if (value[0][2]) //check if there is any object
                        //{
                            //In this case the local model and remote model are out of sync
                            //A Function for verifying the current local model against the remote model may be added                    
                        //}       
                    }
                    else
                    {
                        //feature was empty
                    }             

                    self.index.queryComplete(EObject,query);    
                    if(EObject.eClass.getEStructuralFeature(featureName).get("upperBound")==1)
                    {                        
                        return EObject.get(featureName);   
                    }
                    else
                    {
                        return EObject.get(featureName).array(); 
                    }                 
                }
                catch(e)
                {
                    console.error('Failure during query:'+e)
                    if($DEBUG) console.error("Query failed. Original query:"+query+" Error: "+e);       
                    self.index.queryFail(EObject,query);  
                    throw "Query failed: "+msg;                        
                }
                /*
                self.eoq2domain.Do(cmd).then(function(val){
                    console.error('SUCCESS:'+featureName)
                    onSuccess(val);
                }).catch(function(e){
                    console.error('FAILURE:'+featureName)
                    onFailure(e);
                });
                */
            }
            else
            {             
                //hack so that not everything has to be changed at the moment
                var pendingQuery=new Promise(function(resolve,reject){                
                    self.index.listen(EObject,query,function(){
                        if(EObject.eClass.getEStructuralFeature(featureName).get("upperBound")==1)
                        {                        
                            resolve(EObject.get(featureName));   
                        }
                        else
                        {
                            resolve(EObject.get(featureName).array());
                        }      
                    });
                })

                return await pendingQuery

            }
        } 
        else
        {
            throw 'This eObject is unknown to this ecoreSync instance.'
        }    
        
    };

    // BEGIN OF ECORE CORE FUNCTIONS 

    this.get=function(eObject,feature)
    {
        eObject=instance.proxy.unproxy(eObject);
        return new Promise(function(resolve,reject) {  
            ecoreSync.isQueryable(eObject).then(function(){               
                ecoreSync.isClassInitialized(eObject.eClass).then(function(){        
                    let featureObj = eObject.eClass.getEStructuralFeature(feature);
                    if(featureObj)
                    {
                        switch(featureObj.eClass.get("name"))
                        {
                            case "EAttribute":
                                return self.__getAttr(eObject,feature).then(function(val){
                                    resolve(val);
                                })
                            break;
                            case "EReference":
                                return self.__getRef(eObject,feature).then(function(val){
                                    resolve(val);
                                });   
                            break;             
                        }
                    }
                    else
                    {
                        reject("no such feature:"+feature);
                    }
                });
            });
        });
    };

    this.set=function(eObject,feature,value)
    {
        return new Promise(function(resolve,reject) {
            //let self = this;
            //let _eObject=ecoreSync.proxy.unproxy(eObject);
            ecoreSync.isQueryable(eObject).then(function(){
                ecoreSync.isClassInitialized(eObject.eClass).then(function(){        
                    let featureObj = eObject.eClass.getEStructuralFeature(feature);
                    if(featureObj) {
                        let upperBound = featureObj.get("upperBound");
                        if(upperBound==1) {
                            let oid = eObject.get("_#EOQ");
                            let v = null;
                            switch(featureObj.eClass.get("name")) {
                                case "EAttribute":
                                    v = self.__getAttributeValue(featureObj,value);
                                    break;
                                case "EReference":
                                    if(null==value) {
                                        v = null;
                                    } else {
                                        let vid = value.get("_#EOQ");
                                        v = new eoq2.Qry().Obj(vid);
                                    }
                                    break;
                            }
                            let cmd = new eoq2.Set(new eoq2.Qry().Obj(oid),feature,v);
                            //update the domain
                            ecoreSync.eoq2domain.RawDo(cmd).then(function(res){
                                let val = eoq2.command.ResGetValue(res);
                                let query = self.__createHistoryEntry("SET",oid,feature,v);
                                let changeId = res.c;
                                instance.history.push(query,changeId);
                                resolve(val);
                            }).catch(function(e){
                                reject(e);
                            })
                        } else {
                            reject("Cannot set value for reference "+feature+" with upper bound : "+upperBound);                              
                        }                                    
                    } else {
                        reject("no such feature");
                    }
                });
            });
        });
    };

    this.unset=function(eObject,feature,fallbackValue=null)
    {  
        return this.set(eObject,feature,null); 
    };

    this.add=function(eContainer,feature,eObject)
    {
        return new Promise(function(resolve,reject) {
            let self = this;
            ecoreSync.isQueryable(eContainer).then(function(){
                ecoreSync.isClassInitialized(eContainer.eClass).then(function(){
                    let featureObj = eContainer.eClass.getEStructuralFeature(feature);
                    if(featureObj) {          
                        if(featureObj.get("upperBound")==1) {      
                                reject("Add failed, can not add features with upper bound = 1.");                           
                        } else {
                            let oid = eContainer.get("_#EOQ");
                            let v = null;
                            switch(featureObj.eClass.get("name")) {
                                case "EAttribute":
                                    v = self.__getAttributeValue(featureObj,eObject);
                                    break;
                                case "EReference":
                                    let vid = eObject.get("_#EOQ");
                                    v = new eoq2.Qry().Obj(vid);
                                    break;
                            } 
                            let cmd = new eoq2.Add(new eoq2.Qry().Obj(oid),feature,v);
                            //update the domain
                            ecoreSync.eoq2domain.RawDo(cmd).then(function(res){
                                let val = eoq2.command.ResGetValue(res);
                                let query = ecoreSync.__createHistoryEntry("ADD",oid,feature,v);
                                let changeId = res.c;
                                instance.history.push(query,changeId);
                                resolve(val);
                            }).catch(function(e){
                                reject(e);
                            })   
                        }  
                    } else {
                        reject("no such feature "+feature);
                    }  
                });  
            });
        });
    };

    this.remove=function(eContainer,feature,eObject)
    {
        return new Promise(function(resolve,reject) {
            ecoreSync.isQueryable(eContainer).then(function(){
                ecoreSync.isClassInitialized(eContainer.eClass).then(function(){
                        let featureObj = eContainer.eClass.getEStructuralFeature(feature);
                        if(featureObj) {                  
                            if(featureObj.get("upperBound")==1) {      
                                reject("Remove failed, can not add features with upper bound = 1.");                           
                        } else {
                            let oid = eContainer.get("_#EOQ");
                            let v = null;
                            switch(featureObj.eClass.get("name")) {
                                case "EAttribute":
                                    v = self.__getAttributeValue(featureObj,eObject);
                                    break;
                                case "EReference":
                                    let vid = eObject.get("_#EOQ");
                                    v = new eoq2.Qry().Obj(vid);
                                    break;
                            }   
                            let cmd = new eoq2.Rem(new eoq2.Qry().Obj(oid),feature,v);
                            //update the domain
                            self.eoq2domain.RawDo(cmd).then(function(res){
                                let val = eoq2.command.ResGetValue(res);
                                let query = self.__createHistoryEntry("REM",oid,feature,v);
                                let changeId = res.c;
                                instance.history.push(query,changeId);
                                resolve(val);
                            }).catch(function(e){
                                reject(e);
                            })  
                        }  
                        
                    } else {
                        reject("no such feature");
                    }  
                });  
            });
        })
    };

    this.clear=function(eContainer,feature)
    {  
        let self = this;
        return new Promise( function(resolve,reject) {
            var promises = [];
            children = eContainer.get(feature).array();
            for(let i=0;i<children.length;i++) {
                child = children[i];
                promises.push(self.remove(eContainer,child));
            }
            Promise.all(promises).then(function(values) {
                resolve(values);
            }).catch(function(e) {
                reject(e);
            });
        });
    };

    this.create=function(eClass,count=1) {
        //Creates multiple object instances of eClass according to the specified count.  
        //Resolves a single value for count=1 or an array for count>1   
        if($DEBUG) console.debug('ecoreSync object creation '+eClass.get("name"))   
        return new Promise(function(resolve,reject) {
            if(Number.isInteger(count)) {
                if(count>0) {
                    ecoreSync.isClassInitialized(eClass).then(function(){      
                        
                        if($DEBUG) console.debug('eClass OK during obj creation eClass='+eClass.get("name"))

                        if(eClass.eContainer)
                        {
                            if($DEBUG) console.debug('eClass container OK')
                            let nsURI=eClass.eContainer.get("nsURI");
                            if(nsURI)
                            {
                                if($DEBUG) console.debug('container nsURI OK, nsURI='+nsURI)                           
                           
                                let cmd= new eoq2.Crn(nsURI,eClass.get("name"),count)                          
                                if($DEBUG) console.debug('creating object (nsURI='+nsURI+', eClass='+eClass.get("name")+')')
                                ecoreSync.eoq2domain.Do(cmd).then(function(val){  
                                    
                                    if($DEBUG) console.debug('successfully created '+count+' object(s) (nsURI='+nsURI+', eClass='+eClass.get("name")+')')
                                    
                                        if(count>1) {
                                            var createdObjects=[];
                                            for(let i in val) {
                                                let createdObject=eClass.create();
                                                createdObject.set("_#EOQ",val[i].v);
                                                ecoreSync.index.add(createdObject); 
                                                createdObjects.push(createdObject);
                                            }
                                            resolve(createdObjects);
                                        } else {
                                            let createdObject=eClass.create();
                                            createdObject.set("_#EOQ",val.v);
                                            ecoreSync.index.add(createdObject); 
                                            resolve(createdObject);
                                        }
                                        
                                });
                            } else {
                                reject("invalid namespace URI");
                            }
                        } else {
                            if($DEBUG) console.debug('eClass container missing')
                            reject("eContainer not available");
                        }                    
                    },function(){
                        reject("failed to initialize class "+eClass.get("name"))
                    });
                } else {
                    if(count==0){ console.warn('created zero objects'); resolve([]);}
                    reject("count must be positive");
                }
            } else {
                reject("count must be a positive integer");
            }
        });
    };

    this.__getAttributeValue = function(featureObj,value) {
        let v = null;
        featureObj=ecoreSync.proxy.unproxy(featureObj)
        switch(featureObj.eClass.get("name")) {
            case "EAttribute":
                let featureTypeName = featureObj.get("eType").get("name");            
                switch(featureTypeName) {
                    case "EInt": //fall throught
                        v = Number.parseInt(value); //force integer
                        break;
                    case "EString": //fall throught
                        v = value;
                        break;
                    case "EBoolean": //fall throught
                        v = (value==true); //force a bool
                        break;
                    case "EDouble":   //fall throught      
                    case "EFloat": //fall throught
                        v = value*(1.0+Number.EPSILON); //hack to force a float value, because JS makes no difference 
                        break;
                    case  "EDate":
                        //TODO: corectly set dates
                        v = "DATEFORMAT"
                        break;
                    default:
                        switch(featureObj.get("eType").eClass.get("name")) {
                            case "EEnum": 
                                v = value
                                break;
                            default:                             
                                if($DEBUG) console.debug(featureObj);
                                if($DEBUG) console.debug(featureObj.get("eType").get("name"))
                                throw new Error("Unsupported attribute type: "+featureTypeName);    
                                break;
                        }                
                        break;
                }
                break;
        }
        return v;
    };


    // END OF ECORE CORE FUNCTIONS

    // ECORESYNC/EOQ UTILITY FUNCTIONS

    this.clone=function(eObject,mode='full'){

        var eoqCloneModes={'class':eoq2.CloModes.CLS,'attributes':eoq2.CloModes.ATT,'deep':eoq2.CloModes.DEP,'full':eoq2.CloModes.FUL};
        var cloneMode=eoqCloneModes[mode]
        if(!cloneMode){ cloneMode=eoq2.CloModes.FUL; console.warn('clone mode='+mode+' unknown, defaulted to mode=full')}

        return new Promise(function(resolve,reject) {
            ecoreSync.isQueryable(eObject).then(function() {
                //let query="CLONE #"+eObject.get("_#EOQ")+" FULL";
                let oid = eObject.get("_#EOQ");
                let cmd = new eoq2.Clo(new eoq2.Qry().Obj(oid),cloneMode);
                ecoreSync.eoq2domain.Do(cmd).then(function(val){
                    var clonedObject=eObject.eClass.create();
                    clonedObject.set("_#EOQ",val.v);
                    instance.index.add(clonedObject);     
                    let eClone = ecoreSync.getProxiedObject(clonedObject);
                    resolve(eClone);
                }).catch(function(e) {
                    reject(error);
                });
            });
        });
    };

    this.getEClass=function(nsURI,name)
    {
        //Asynchronously gets an eClass by its namespace URI and name, full synchronization

        let localEClass=ecoreSync.find(function(e){ 
            if(e.eContainer)
            {
                return e.eContainer.get("nsURI")==nsURI && e.get("name")==name && e.eClass.get("name")=="EClass";
            } 
        });
        if(localEClass)
        {          
            return ecoreSync.isClassInitialized(localEClass).then(function(eClass){
                return Promise.resolve(ecoreSync.getProxiedObject(eClass));
            });
        }
        else
        {
           let cmd=new eoq2.Cmp().Gmm()
                                   .Get(new eoq2.Qry().His(-1).Sel(new eoq2.Qry().Pth('nsURI').Equ(nsURI)).Idx(0).Cls('EClass').Sel(new eoq2.Qry().Pth('name').Equ(name)).Idx(0))
           return self.eoq2domain.Do(cmd).then(function(val)
            {      
                if($DEBUG) console.debug(val)
                if(val[1])
                {
                    if(!ecoreSync.index.containsId(val[1].v))
                    {
                        var eClass=Ecore.EClass.create();
                        eClass.set("_#EOQ",val[1].v);                    
                        ecoreSync.index.add(eClass);
                        return ecoreSync.isClassInitialized(eClass).then(function()
                        {
                            return Promise.resolve(ecoreSync.getProxiedObject(eClass)); 
                        });                            
                    }
                    else
                    {
                        return ecoreSync.isClassInitialized(ecoreSync.index.getById(val[1].v))
                    }  
                }                 
                    
         
                return Promise.reject("eClass not found");
            },
            function(error)
            {
                return Promise.reject("could not get eClass:  "+nsURI+" / "+name+" error: "+error);
            }); 
        }           
    };

    // BEGIN LEGACY FUNCTIONS

    // this.__setValue=function(EObject,feature,newValue,currentValue)
    // {
    //     //This function sets an attribute value of the eObject to a new value.
    //     //If not already queryable, the query will delay until the object id is updated.
    //     //If the query fails, the current value is restored.
    //     //You can use this function to set attribute values available on the remote server,
    //     //therefore setting local attributes e.g. the object id _#EOQ will fail or not perform any action.

    //     if(feature != "_#EOQ" )
    //     {
    //             self.isQueryable(EObject).then(function(){
    //                 self.isClassInitialized(EObject.eClass).then(function(){
    //                 //Sets the value of the EObject EAttribute feature
    //                 let value="";
    //                 let prefix="";     
    //                 let featureObj = EObject.eClass.getEStructuralFeature(feature);
    //                 if(!featureObj) {
    //                     if($DEBUG) console.debug("Could not set value "+ value.toString() + " for feature "+ feature + ". Feature does not exist for element of type "+EObject.eClass.get('name')+".");
    //                 } else { 
    //                     let featureTypeName = featureObj.get("eType").get("name");            

    //                     if(featureObj.eClass.get("name")=="EAttribute")
    //                     {
    //                             switch(featureTypeName)
    //                             {
    //                                 case  "EInt":
    //                                     value=newValue.toString();
    //                                     if(value=="")
    //                                     {
    //                                         value="%";
    //                                     }
    //                                     break;
    //                                 case  "EString":
    //                                     value="'"+newValue+"'";
    //                                     if(value=="")
    //                                     {
    //                                         value="%"; //BA: can never occure
    //                                     }
    //                                     break;
    //                                 case "EBoolean":
    //                                     if(newValue)
    //                                     {
    //                                         value="true";
    //                                     }
    //                                     else
    //                                     {
    //                                         value="false";
    //                                     }
    //                                     break;
    //                                 case "EDouble":          
    //                                     if(Number.isInteger(newValue))
    //                                     {
    //                                         value=newValue+".00";
    //                                     }
    //                                     else
    //                                     {
    //                                         value=newValue;
    //                                     }
                        
    //                                     break;
    //                                 case "EFloat":
    //                                     if(Number.isInteger(newValue))
    //                                     {
    //                                         value=newValue+".00";
    //                                     }
    //                                     else
    //                                     {
    //                                         value=newValue;
    //                                     }
    //                                     break;
    //                                 case  "EDate":
    //                                     value="'"+newValue+"'";
    //                                     if(value=="")
    //                                     {
    //                                         value="%"; //BA: can never occure
    //                                     }
    //                                     break;
    //                                 default:
    //                                     switch(featureObj.get("eType").eClass.get("name"))
    //                                     {
    //                                         case "EEnum": 
    //                                             value="'"+newValue+"'";
    //                                             break;
    //                                         default:
    //                                             if(newValue) {
    //                                                 value='#'+newValue.get("_#EOQ");
    //                                             } else {
    //                                                 value='%'; //unset value
    //                                             }
    //                                             break;
    //                                     }                
    //                                     break;
    //                             }
                                
    //                             let query="UPDATE #"+EObject.get("_#EOQ")+" /"+feature+" "+value;
    //                             let onSuccess=function(value){                      
    //                                 /* No error occured, the operation is valid */    
    //                                 ecoreSync.proxy.unproxy(EObject).set(feature,newValue);  
    //                                 instance.history.push(query,value[1]);     
    //                             };
    //                             let onFailure=function(value){  
    //                                 if($DEBUG) console.debug("Failed to set value");
    //                                 EObject.set(feature,currentValue); 
    //                             };
    //                             self.domain.DoFromStr(query,onSuccess,onFailure);
    //                     }

    //                     if(featureObj.eClass.get("name")=="EReference")
    //                     {
    //                         if(newValue.get("_#EOQ"))
    //                         {
    //                             value="#"+newValue.get("_#EOQ");                                   
    //                             let query="UPDATE #"+EObject.get("_#EOQ")+" /"+feature+" "+value;
    //                             let onSuccess=function(value){                      
    //                                 /* No error occured, the operation is valid */    
    //                                 EObject.set(feature,newValue);  
    //                                 instance.history.push(query,value[1]);     
    //                             };
    //                             let onFailure=function(value){                                      
    //                                 EObject.set(feature,currentValue); 
    //                             };
    //                             self.domain.DoFromStr(query,onSuccess,onFailure);
    //                         }
    //                         else
    //                         {
    //                             //The object must be created first
    //                             self.__createAndSet(EObject,feature,newValue);
    //                         }
    //                     }


              
    //                 }
    //             });
    //         });
    //         };       
        
    //  }


    //BA: Unused function?
     this.__addReferenceTo=function(Reference,EObject)
     {
        //This function adds the eObject to the reference once the owner is queryable.
        //If not already queryable, the query will delay until the object id is updated.
        //The reference is removed locally if the query fails.
        
        self.isQueryable(Reference._owner).then(function(){      
             //Adds a reference to the EObject
             let cmd = null
             let isMulti = Reference._feature.get("upperBound")!=1;
             let oid = Reference._owner.get("_#EOQ");
             let featureName = Reference._feature.get("name");
             let vid = EObject.get("_#EOQ");
             if(isMulti) {
                cmd = new CMD.Add(new eoq2.Qry().Obj(oid),featureName,new QRY.Obj(vid));
             } else {
                cmd = new CMD.Set(new eoq2.Qry().Obj(oid),featureName,new QRY.Obj(vid));
             }
             self.eoq2domain.RawDo(cmd).then(function(res) {
                 let val = eoq2.ResGetValue(res); //quits with exception if it failes
                 self.history.push("ADD",res.c);
             }).catch(function(e) {
                Reference.remove(EObject);
             });
        });            
      };

     //BA: unused
    // this.__createAndSet=function(eObject,feature,newObject)
    // {
    //     self.isQueryable(eObject).then(function(){ 
    //         let nsURI=eClass.eContainer.get("nsURI");
    //         if(nsURI!=null)
    //         {
    //             let cmd = CMD.Crn()
    //                 .Get()
    //             let query="CREATE '"+nsURI+"' '"+newObject.eClass.get("name")+"' 1\n"+
    //                 "UPDATE #"+eObject.get("_#EOQ")+" /"+feature+" $0";
    //             let onSuccess=function(value){  
    //                 newObject.set("_#EOQ",value[0][2].v);     
    //                 instance.__initClass(newObject.eClass); 
    //                 instance.index.add(newObject);     
    //                 self.history.push(query,value[0][1]);   
    //                 eObject.set(feature,newObject);   
    //             };
    //             let onFailure=function(value){  
    //                 eObject.unset(feature);
    //             };
    //             self.domain.DoFromStr(query,onSuccess,onFailure);
    //         }
    //     });
    // };

    this.__createIn=function(EContainer,EObject)
    {
        //This function creates and adds the eObject to the containment once the owner is queryable.
        //If not already queryable, the query will delay until the object id is updated.
        //The eObject is removed locallyfrom the containment if the query fails.
        //The eObject will only be added to the index if the remote creation succeeds.
        //This will only work for a feature with upper bound > 1

        let self = this;
        self.isQueryable(EContainer._owner).then(function(){ 
            let nsURI=EObject.eClass.eContainer.get("nsURI");
            if(nsURI!=null)
            {
                // let query="CREATE '"+nsURI+"' '"+EObject.eClass.get("name")+"' 1\n"+
                //         "UPDATE #"+EContainer._owner.get("_#EOQ")+" /"+EContainer._feature.get("name")+"[+] $0";
                let tid = EContainer._owner.get("_#EOQ");
                let f = EContainer._feature.get("name");
                let cmd = new eoq2.Cmp().Crn(nsURI,EObject.eClass.get("name"),1)
                                          .Add(new eoq2.Qry().Obj(tid),f,new eoq2.Qry().His(-1));

                self.eoq2domain.Do(cmd).then(function(val) {
                    let nid = val[0].v;//new objects id
                    EObject.set("_#EOQ",nid);     
                    instance.__initClass(EObject.eClass); 
                    instance.index.add(EObject); 
                    let query = self.__createHistoryEntry("ADD",tid,f,nid);    
                    self.history.push(query,nid);  
                }).then(function(e){
                    EContainer.remove(EObject);
                });
            }
        }); 
     };

     this.__createHistoryEntry = function(type,target,feature,value) {
         return type+" "+target+" "+feature+" "+value;
     };


     this.__removeReferenceTo=function(EContainer,Reference,EObject)
     {
        return this.remove(EContainer,Reference,EObject);
     };

     this.__deleteFrom=function(EContainer,EFeature,EObject)
     {
        return this.remove(EContainer,EFeature,EObject);
        // if(EObject.get("_#EOQ"))
        // {
        //     //Deletes the EObject from the specified EContainer
        //     let query="UPDATE #"+EContainer._owner.get("_#EOQ")+" /"+EContainer._feature.get("name")+"[-] #"+EObject.get("_#EOQ");
        //     let onSuccess=function(value){  
        //         console.log("EObject successfully removed.");
        //         self.history.push(query,value[0][1]);
        //     };
        //     let onFailure=function(value){  
        //         console.log("Error: EObject could not be removed from containment");
        //         EContainer.add(EObject); 
        //     };
        //     this.domain.DoFromStr(query,onSuccess,onFailure);
        // }
    }
     


    this.__initEnumeration=function(eType,callback=null)
    {
        let oid = eType.get("_#EOQ");
        let cmd = CMD.Get(QRY.Obj(oid).Pth("eLiterals"));
        let query = "eLiterals";
        //let query="RETRIEVE #"+eType.get("_#EOQ")+" /eLiterals";
        let onSuccess=function(val){  
            var __eLiteral=null;
            for(let i in val)
            {
                __eLiteral=Ecore.EEnumLiteral.create();
                __eLiteral.set("name",val[i]);
                __eLiteral.set("literal",val[i]);
                __eLiteral.set("value",i);
                eType.get("eLiterals").add(__eLiteral);
            }    
        };

        //Initialization Conditions
        if(!self.index.contains(eType,query))
        {
            self.index.add(eType,query,callback); 
            this.eoq2domain.Do(cmd).then(function(val) {
                onSuccess(val);
            }).catch(function(e) {
                throw e;
            });
        }
        else
        {
            self.index.listen(eType,query,callback);
        }

    };

    this.initENumById=function(id)
    {
        var eNum=self.index.getById(id);
        if(eNum)
        {
            return Promise.resolve(eNum);
        }
        else
        {
            let cmd = CMD.Cmp()
                .Get(QRY.Obj(id).Pth("name"))
                .Get(QRY.Obj(id).Pth("eLiterals"));
            //let query="RETRIEVE #"+id+" /name\nRETRIEVE #"+id+" /eLiterals";
            return self.eoq2domain.Do(cmd).then(function(val){
            //return self.index.query(query).then(function(value){
                eNum=Ecore.EEnum.create();
                eNum.set("_#EOQ",id);
                eNum.set("name",val[0]);
                var eLiteral=null;
                //if($DEBUG) console.debug(value);
                for(let i in val[1])
                {
                    eLiteral=Ecore.EEnumLiteral.create();
                    eLiteral.set("name",val[1][i]);
                    eLiteral.set("literal",val[1][i]);
                    eLiteral.set("value",i);
                    eNum.get("eLiterals").add(eLiteral);
                }  
                self.index.add(eNum);
                return Promise.resolve(eNum);
            },
            function(error)
            {
                return Promise.reject(error);
            })
        }
    };


    this.initENum=function(eNum)
    {
            //let query="RETRIEVE #"+eNum.get("_#EOQ")+" /name\nRETRIEVE #"+eNum.get("_#EOQ")+" /eLiterals";

            let id = eNum.get("_#EOQ");
            let cmd = CMD.Cmp()
                .Get(QRY.Obj(id).Pth("name"))
                .Get(QRY.Obj(id).Pth("eLiterals"));

            return self.eoq2domain.Do(cmd).then(function(val) {
            //return self.index.query(query).then(function(value){
                eNum=Ecore.EEnum.create();      
                eNum.set("name",val[0]);
                var eLiteral=null;
                for(let i in val[1])
                {
                    eLiteral=Ecore.EEnumLiteral.create();
                    eLiteral.set("name",val[1][i]);
                    eLiteral.set("literal",val[1][i]);
                    eLiteral.set("value",i);
                    eNum.get("eLiterals").add(eLiteral);
                }  
                self.index.add(eNum);
                return Promise.resolve(eNum);
            },
            function(error)
            {
                return Promise.reject(error);
            });        
    };

    this.__initClass=function(EClass,completeQuery=true,callback=null)
    {
        ecoreSync.isQueryable(EClass).then(function(){

            let oid = EClass.get("_#EOQ");

            let cmd=new eoq2.Cmp();
            cmd.Get(new eoq2.Qry().Obj(oid).Pth("name"))
            cmd.Get(new eoq2.Qry().Obj(oid).Pth("abstract"))
            cmd.Get(new eoq2.Qry().Obj(oid).Met("PACKAGE"))

            // //let query="RETRIEVE #"+EClass.get("_#EOQ")+" @RESOURCENAME\n"+
            // let query="RETRIEVE #"+oid+" /name\n"+
            //           "RETRIEVE #"+oid+" /abstract\n"+
            //           "RETRIEVE #"+oid+" /ePackage";

            if(completeQuery)
            {

                cmd.Get(new eoq2.Qry().Obj(oid).Pth("eAllReferences"))
                cmd.Get(new eoq2.Qry().His(3).Pth("name"))
                cmd.Get(new eoq2.Qry().His(3).Pth("eType"))
                cmd.Get(new eoq2.Qry().His(3).Pth("containment"))
                cmd.Get(new eoq2.Qry().His(5).Pth("name"))
                cmd.Get(new eoq2.Qry().His(5).Met("PACKAGE"))
                cmd.Get(new eoq2.Qry().Obj(oid).Pth("eAllAttributes"))
                cmd.Get(new eoq2.Qry().His(9).Pth("name"))
                cmd.Get(new eoq2.Qry().His(9).Pth("eType"))
                cmd.Get(new eoq2.Qry().His(11).Pth("name"))
                cmd.Get(new eoq2.Qry().His(11).Met("CLASS").Pth("name"))
                cmd.Get(new eoq2.Qry().His(3).Pth("lowerBound"))
                cmd.Get(new eoq2.Qry().His(3).Pth("upperBound"))

                // query+="\nRETRIEVE #"+oid+" /eAllReferences\n"+
                //         "RETRIEVE $3 /name\n"+
                //         "RETRIEVE $3 /eType\n"+
                //         "RETRIEVE $3 /containment\n"+
                //         "RETRIEVE $5 /name\n"+
                //         "RETRIEVE $5 /ePackage\n"+
                //         //"RETRIEVE $5 @RESOURCENAME\n"+
                //         "RETRIEVE #"+oid+" /eAllAttributes\n"+
                //         "RETRIEVE $9 /name\n"+
                //         "RETRIEVE $9 /eType\n"+
                //         "RETRIEVE $11 /name\n"+
                //         "RETRIEVE $11 /eClass/name\n"+
                //         "RETRIEVE $3 /lowerBound\n"+ //BA: added upper an lower bound information
                //         "RETRIEVE $3 /upperBound";
            }            

            let query = "__initClass_"+oid+"_"+completeQuery;

            
            let onSuccess=function(val){    
                //$.notify("CLASS INIT SUCCESS #"+EClass.get("_#EOQ"),"success");
                //Initialization after succesful query         

                //Meta Resource
                //var metaResource=self.__getResource(value[0][2]);            
                //if($DEBUG) console.debug(val);
                //Initialize Containment
                if(!EClass.eContainer)
                {
               
                    //Initialize EPackage
                    var __ePackage=self.__findIn(instance.__resourceSet,val[2].v);
                
                    if(!__ePackage)
                    {
                        __ePackage=Ecore.EPackage.create();
                        __ePackage.set("_#EOQ",val[2].v);                     
                        instance.__initPackage(__ePackage);               
                    }            
                    
                    
                    if(__ePackage)
                    {              
                        if(!EObjectisReferenced(__ePackage.get("eClassifiers"),EClass))
                        {
                            __ePackage.get("eClassifiers").add(EClass);
                        }         
                        else
                        {
                            console.log(__ePackage.get("eClassifiers").array());
                            console.log(EClass);

                            if(instance.index.contains(EClass))
                            {
                                console.log("NOT IN CACHE");
                            }


                            console.log(instance.__findIn(instance.__resourceSet,EClass.get("_#EOQ")));

                            throw "Fatal Error: Probable EClass duplicate. EClass container already contains the EClass."+EClass.get("_#EOQ");
                        }
        
                    }
                    else
                    {
                        throw "Fatal Error: EClass container must not be an undefined EPackage.";
                    }
                }

                //Assert containment       
                if(!EClass.eContainer)
                {
                    throw "ASSERTION FAILED: EClass is uncontained. EClass #"+EClass.get("_#EOQ")+" Container #"+val[3].v;
                }


                /*
                if(self.__findIn(instance.__resourceSet,EClass.get("_#EOQ"),false)==undefined)
                {
                    console.log(metaResource);
                    __ePackage.get("eClassifiers").add(EClass);
                    //$.notify("Class "+value[1][2]+" added to EPackage #"+value[3][2].v,"info");
                }
                else
                {
                    console.log(metaResource);
                    //$.notify("Class "+value[1][2]+" already contained #"+EClass.get("_#EOQ")+" in metaResource","info");
                    //throw "HALT";
                }
*/

                //Minimal EClass initialization
                EClass.set("name",val[0]);
                EClass.set("abstract",val[1]);
                self.__setAttributeInitialized(EClass,"name");
                self.__setAttributeInitialized(EClass,"abstract");
                EClass["_isInitialized"]=true; 
                ecoreSync.proxy.unproxy(EClass).get("eStructuralFeatures").add(Ecore.EAttribute.create({ name: '_#EOQ', eType: Ecore.EInt }));
              

                if(EClass.get("abstract"))
                {
                    //Initialize classes that use this class as a supertype
                    instance.__initConcreteClasses(EClass,completeQuery);
                }

                var classesInitialized=[];
                if(completeQuery)
                {
                    
                    for (let i in val[3])
                    {             
                        
                        //Initialize EPackage for Reference Type
                        //var metaResource=self.__getResource(value[9][2]);
                        var __ePackage=self.__findIn(instance.__resourceSet,val[2].v);
                        if(!__ePackage)
                        {
                            __ePackage=Ecore.EPackage.create();
                            __ePackage.set("_#EOQ",val[8][i].v);                     
                            instance.__initPackage(__ePackage);     
                        }               

                        if(!val[5][i])
                        {
                            if($DEBUG) console.debug("Class initialization of #"+EClass.get("_#EOQ"));
                            if($DEBUG) console.debug(query);
                            if($DEBUG) console.debug(EClass);
                            if($DEBUG) console.debug(val[7][i]);
                            if($DEBUG) console.debug(val[3][i]);
                        }

                        __eClass=self.__findIn(instance.__resourceSet,val[5][i].v);
                        if(!__eClass)
                        {
                            __eClass=Ecore.EClass.create();
                            __eClass.get("eStructuralFeatures").add(Ecore.EAttribute.create({ name: '_#EOQ', eType: Ecore.EInt }));
                            __eClass.set("_#EOQ",val[5][i].v);
                            __eClass.set("name",val[7][i]);
                            self.__setAttributeInitialized(__eClass,"name");
                            //self.__initClass(__eClass,false);    
                            classesInitialized.push(ecoreSync.isClassInitialized(__eClass,false));                                              
                        }                     
                        
                        if(EClass.get("eStructuralFeatures").array().find(function(e){return e.get("name")==val[4][i];})==undefined)
                        {                         
                            //let ref=Ecore.EReference.create({ name: value[5][2][i], eType: __eClass, containment: value[7][2][i], lowerBound: 0, upperBound: -1 });
                            let ref=Ecore.EReference.create({ name: val[4][i], eType: __eClass, containment: val[6][i], lowerBound: 0, upperBound: val[15][i] });
                            ref.set("_#EOQ",val[3][i].v);                   
                            ecoreSync.proxy.unproxy(EClass).get("eStructuralFeatures").add(ref);   
                        }      
        
                    }   
                    
                  
                    
                    for(let i in val[10])
                    {
                        if(EClass.get("eStructuralFeatures").array().find(function(e){return e.get("name")==val[10][i];})==undefined)
                        {    
                            //user-defined attributes  
                            var __eType;

                            //Check for Standard Ecore Datatype
                            var keys=Object.keys(Ecore);
                            if( keys.includes(val[12][i]) )
                            {
                                __eType=Ecore[val[12][i]];
                            }
                            else
                            {

                                if(val[12][i]=="EInteger")
                                {
                                    __eType==Ecore["EInt"];
                                }
                                else
                                {

                                        __eType=self.__findIn(instance.__resourceSet,val[11][i].v);
                                        if(!__eType)
                                        {
                                            if(val[13][i]=="EEnum")
                                            {
                                                __eType=Ecore.EEnum.create();
                                                __eType.set("_#EOQ",val[11][i].v);
                                                __eType.set("name",val[12][i]);
                                                self.__initEnumeration(__eType,true);    
                                            }

                                            if(val[14][i]=="EDataType")
                                            {
                                                __eType=Ecore.EDataType.create();
                                                __eType.set("_#EOQ",val[11][i].v);
                                                __eType.set("name",val[12][i]);
                                                $.notify("Uninitialized data type "+val[13][i]+" "+val[12][i]);
                                                //self.__initDataType(__eType,true); 
                                            }                                                         
                                        }
                                }
                            }
          
                            let attr=Ecore.EAttribute.create({ name: val[10][i], eType: __eType });
                            attr.set("_#EOQ",val[9][i].v);
                            ecoreSync.proxy.unproxy(EClass).get("eStructuralFeatures").add(attr);   
                        }
                        else
                        {
                            //for Ecore defined or already present attributes
                            let attr=EClass.get("eStructuralFeatures").array().find(function(e){return e.get("name")==val[10][i];});
                            attr.set("_#EOQ",val[9][i].v);
                        }
                    }

                    
                    delete EClass["_partial"];                    
                }
                else
                {                   
                    EClass["_partial"]=true; 
                }
          
              
                //waiting for the nested object classes to become ready, although the query is technically complete its result is only useful if all 
                //referenced classes are known, therefore we will report back once all classes are initialized                     
                Promise.all(classesInitialized).then(function(){
                    self.__notifyAll(instance.__resourceSet,EClass);                                            
                    self.index.queryComplete(EClass,query); 
                    EClass.trigger("init");
                });
              
            }; 
            

            //Initialization Conditions
            if(!self.index.contains(EClass,query))
            {
               // $.notify("CLASS INIT #"+EClass.get("_#EOQ"));
                self.index.add(EClass,query,callback);
                self.eoq2domain.Do(cmd).then(function(val){ 
                    onSuccess(val);
                }).catch(function(e){
                    if($DEBUG) console.debug("ecoreSync: Failed to initialize class #"+EClass.get("_#EOQ"));
                });
            }
            else
            {
               // $.notify("CLASS WAIT #"+EClass.get("_#EOQ"));
                self.index.listen(EClass,query,callback);
            }   
        });
    };

    this.__initClassReferences=function(EClass,references)
    {
        
    }

    this.__initConcreteClasses=function(EClass,resolveReferences=true)
    {
        //Intializes all EClasses that use EClass as a supertype, resolves references if not instructed not to do so
        //Initializing the concrete classes enables the creation of objects in containments with abstract types

        //let query="RETRIEVE #"+EClass.get("_#EOQ")+" /ePackage\n"+
        //        "RETRIEVE $0 $EClass{eSuperTypes=#"+EClass.get("_#EOQ")+"}";

        let cmd = new eoq2.Get(new eoq2.Qry().Obj(EClass.get("_#EOQ")).Met("IMPLEMENTERS"));
        self.eoq2domain.Do(cmd).then(function(val) {
            for(let i=0;i<val.length;i++)
            {
                var __eClass=self.__findIn(self.__resourceSet,val[i].v);
                if(!__eClass)
                {
                    __eClass=Ecore.EClass.create();
                    __eClass.get("eStructuralFeatures").add(Ecore.EAttribute.create({ name: '_#EOQ', eType: Ecore.EInt }));
                    __eClass.set("_#EOQ",val[i].v);
                    __eClass.get("eSuperTypes").add(EClass);
                    self.__initClass(__eClass,resolveReferences);  
                }
            }
        });
       /*  //let query="RETRIEVE #"+EClass.get("_#EOQ")+" @IMPLEMENTERS";
        //"RETRIEVE $0 $EClass{eSuperTypes=#"+EClass.get("_#EOQ")+"}";
        let onSuccess=function(value){
            for(let i in value[1][2])
            {
                var __eClass=self.__findIn(self.__resourceSet,value[2][i].v);
                if(!__eClass)
                {
                    __eClass=Ecore.EClass.create();
                    __eClass.get("eStructuralFeatures").add(Ecore.EAttribute.create({ name: '_#EOQ', eType: Ecore.EInt }));
                    __eClass.set("_#EOQ",value[2][i].v);
                    __eClass.get("eSuperTypes").add(EClass);
                    self.__initClass(__eClass,resolveReferences);  
                }
            }
        };
        self.domain.DoFromStr(query,onSuccess);    */
    };


    this.__initObject=function(objectId,callback=null)
    {
        //let query="RETRIEVE #"+objectId+" /eClass";
        let cmd = CMD.Get(QRY.Obj(objectId).Met("CLASS"));
        let onSuccess=function(val)
        {
            //console.log(val);
            //Initialize EClass
           var __eClass=self.__findIn(self.__resourceSet,val.v);
           if(!__eClass)
           {
                __eClass=Ecore.EClass.create();
                __eClass.get("eStructuralFeatures").add(Ecore.EAttribute.create({ name: '_#EOQ', eType: Ecore.EInt }));
                __eClass.set("_#EOQ",val.v);
                instance.__initClass(__eClass);
           }

           var __eObject=__eClass.create();
           __eObject.set("_#EOQ",objectId);
           self.index.add(__eObject);
           callback(__eObject);
        }

        this.eoq2domain.Do(cmd).then(function(val){
            onSuccess(val);
        });
    }

    this.__initPackage=function(EPackage,callback=null)
    {
        //Initializes and sorts-in the EPackage
        // let query="RETRIEVE #"+EPackage.get("_#EOQ")+" /eContainer\n"+
        //         "RETRIEVE $-1 @CLASS/name\n"+
        //         "RETRIEVE #"+EPackage.get("_#EOQ")+" /nsURI";


        let pid = EPackage.get("_#EOQ"); //package id
        let cmd = new eoq2.Cmp().Get(new eoq2.Qry().Obj(pid).Met("PARENT"))
                                  .Get(new eoq2.Qry().Met("IF",[new eoq2.Qry().His(-1),new eoq2.Qry().His(-1).Met("CLASSNAME"),null]))
                                  .Get(new eoq2.Qry().Obj(pid).Pth("nsURI"));


        if($DEBUG) console.info("Init package: id="+EPackage.get("_#EOQ")+", type="+EPackage.eClass.get("name"));

        //Initialization Conditions
        let query = "initPackage "+pid
        if(!this.index.contains(EPackage,query))
        {
            this.index.add(EPackage,query,callback);
            let self = this;
            this.eoq2domain.Do(cmd).then(function(val) {              
                //Initialization of EPackage after a successful query
                //Package Properties
                EPackage.set("nsURI",val[2]);
                self.__setAttributeInitialized(EPackage,"nsURI");
    
                //Package containment
                //The EPackage is either contained in another EPackage or located on root level of its resource.
                //The distinction is made based on the query return value (not null=parent package,null=resource)
    
                if(val[1].v=="EPackage")
                {        
                    //Container is a Package

                    var __ePackage=self.__findIn(instance.__resourceSet,val[0].v);
                    if(!__ePackage)
                    {
                        //$.notify("Parent EPackage #"+value[0][2].v+" not found for EPackage #" +EPackage.get("_#EOQ"),"error");
                        //Parent Package not found
                        __ePackage=Ecore.EPackage.create();
                        __ePackage.set("_#EOQ",val[0].v);  
                        __ePackage.get('eClassifiers').add(EPackage);                   
                        instance.__initPackage(__ePackage);                   
                    }
                    else
                    {
                    //$.notify("Parent EPackage #"+value[0][2].v+"  found for EPackage #" +EPackage.get("_#EOQ"),"success");
                        //Parent Package was found
                        var __thisPackage=self.__findIn(__ePackage,EPackage.get("_#EOQ"));
                        if(!EObjectisContained(__ePackage,EPackage))
                        {
                            //Package is not already contained                   
                            __ePackage.get("eSubPackages").add(EPackage);
                            //$.notify("Parent EPackage #"+value[0][2].v+"  : Added EPackage #" +EPackage.get("_#EOQ"),"success");
                        }
                        else
                        {
                            //$.notify("Parent EPackage #"+value[0][2].v+"  : Already contains EPackage #" +EPackage.get("_#EOQ"),"error");
                        }
                    }

                    //$.notify("EPackage "+value[2][2]+" initialized #" +EPackage.get("_#EOQ"),"success");
                }
                else
                {
                    //$.notify("EPackage "+value[2][2]+" contained on ROOT level #" +EPackage.get("_#EOQ"),"info");  
                    //Package is the resource itself                
                    //var metaResource=instance.__getResource(value[1][2]);
                    //if(!EObjectisContained(metaResource,EPackage))
                    //{                      
                    //   metaResource.add(EPackage);        
                    // $.notify("EPackage "+value[2][2]+" initialized on ROOT level #" +EPackage.get("_#EOQ"),"success");            
                    //}     
                    //else
                    //{
                    // $.notify("EPackage "+value[2][2]+" already contained on ROOT level #" +EPackage.get("_#EOQ"),"error");   
                    //}               
                }


        

                //The job is done, therefore remove the EObject/query from the cache
                self.index.queryComplete(EPackage,query);
                //$.notify("Package initialized  nsURI="+EPackage.get("nsURI")+" #"+EPackage.get("_#EOQ"),"success"); 
                

            }); 
        } else {
            this.index.listen(EPackage,query,callback);
        }

        /*
        var __ePackage=self.__findIn(self.__resourceSet,EPackage.get("_#EOQ"));
        if(!__ePackage)
        {
            if(EPackage.get("_#EOQ") != null)
            {        
                //$.notify("Package initializing EOQ="+EPackage.get("_#EOQ"));                
                self.index.add(EPackage,query);
                this.domain.DoFromStr(query,onSuccess);
            
            }
        }     
        */   

    };

    // BA: unused?
    // this.loadResource=function(objectId,callback=null)
    // {
    //     //loads the resource
    //     //BA: load is not necessary any more, since resources are loaded automatically
    //     //let query="CALL 'load-resource' [#"+objectId+"]\n"+
    //     //          "RETRIEVE #"+objectId+" /contents[0]\n"+
    //     //          "RETRIEVE $0 /eClass";

    //     let query="RETRIEVE #"+objectId+" /contents[0]\n"+
    //               "RETRIEVE $0 @CLASS";

    //     //Initialization after query succeeded
    //     let onSuccess=function(value){  

    //         let rootObject=value[0][2].v;
    //         let rootObjectEClass=value[1][2].v;     

    //        //Initialize Root Class
    //        var __eClass=Ecore.EClass.create();
    //        __eClass.get("eStructuralFeatures").add(Ecore.EAttribute.create({ name: '_#EOQ', eType: Ecore.EInt }));
    //        __eClass.set("_#EOQ",rootObjectEClass);
           
    //        if(!self.index.contains(__eClass))
    //        {  
    //             instance.__initClass(__eClass);
    //        } 
    //        else     
    //        {
    //            __eClass=self.index.getById(rootObjectEClass);
    //        } 

    //        //Create Resource Root
    //        resourceRoot=__eClass.create();    
    //        resourceRoot.set("_#EOQ",rootObject);
    //        if(!self.index.contains(resourceRoot))
    //        {  
    //            self.index.add(resourceRoot);
    //        }   

    //        if(callback!=null)
    //        {
    //             callback(rootObject);
    //        }
          			
    //     };      

    //     var onFailure=function(msg)
    //     {
    //         if($DEBUG) console.debug("ecoreSync: Failed to load resource: "+msg+" Original Query: "+query);
    //     };
        
    //     this.domain.DoFromStr(query,onSuccess,onFailure);          
    // };

    //BA unused?
    // this.saveResource=function(eResource)
    // {
    //     let self = this;
    //     return new Promise(function(resolve,reject) {
    //         let id = eResource.get("_#EOQ");
    //         //return the path to the object
    //         let cmd = jseoq.CommandParser.StringToCommand('CALL \'save-resource\' [#' +id+']');
    //         self.domain.DoSync(cmd).then(function(result){
    //             if(jseoq.ResultParser.IsResultOk(result)) {
    //                 resolve(eResource);
    //             } else {
    //                 reject(new Error(result.message));
    //             }
    //         });
    //     }); 
    // };

/*
    this.__initResource=function(resource,callback=null)
    {
        //MARKED FOR DELETION
        //initializes the resource

        let query="RETRIEVE #0 :"+resource.get("uri")+"[0]\nRETRIEVE $0 /eClass@RESOURCENAME\nRETRIEVE $0 @PACKAGE\nRETRIEVE $0 /eClass";

        //Initialization after query succeeded
        let onSuccess=function(value){  

            // Resource EOQ ID is the same as the root's [0]
            resource.set("_#EOQ",value[0][2].v);             

            //Root EPackage 
            var metaResource=instance.__getResource(value[1][2]);
            var __ePackage=instance.__findIn(metaResource,value[2][2].v);
            if(!__ePackage)
            {   
                __ePackage=Ecore.EPackage.create();
                __ePackage.set("_#EOQ",value[2][2].v);          
                instance.__initPackage(__ePackage);
            }

           //Initialize Root Class
           var __eClass=self.__findIn(metaResource,value[3][2].v);
           if(!__eClass)
           {
                __eClass=Ecore.EClass.create();
                __eClass.get("eStructuralFeatures").add(Ecore.EAttribute.create({ name: '_#EOQ', eType: Ecore.EInt }));
                __eClass.set("_#EOQ",value[3][2].v);
                instance.__initClass(__eClass);
           }
  

           //Create Resource Root
           resourceRoot=__eClass.create();    
           resourceRoot.set("_#EOQ",value[0][2].v);
           resource.add(resourceRoot);    
           self.index.add(resourceRoot);
           ////$.notify("Resource initialized "+resource.get("uri"),"success");
           if(callback!=null)
           {
                setTimeout(callback, 0);
           }
          			
        };      
        
        this.domain.DoFromStr(query,onSuccess);          
    };
    */

//BA: Unused?
    // this.__fullSync=function(EClass)
    // {

    //     let query="";
    //     //EClass
    //     query+="RETRIEVE #5 /eClass\n";
    //     query+="RETRIEVE $0 @RESOURCENAME\n";
    //     query+="RETRIEVE $0 /name\n";
    //     query+="RETRIEVE $0 /abstract\n";
    //     query+="RETRIEVE $0 /ePackage\n";
    //     //References/Containments
    //     query+="RETRIEVE $0 /eAllReferences\n";
    //     query+="RETRIEVE $5 /name\n";
    //     query+="RETRIEVE $5 /eType\n";
    //     query+="RETRIEVE $5 /containment\n";
    //     query+="RETRIEVE $7 /name\n";
    //     query+="RETRIEVE $7 /ePackage\n";
    //     query+="RETRIEVE $7 @RESOURCENAME\n";
    //     //Attributes
    //     query+="RETRIEVE $0 /eAllAttributes\n";
    //     query+="RETRIEVE $12 /name\n";
    //     query+="RETRIEVE $12 /eType\n";
    //     query+="RETRIEVE $14 /name\n";
    //     query+="RETRIEVE $14 /eClass/name";

    //     let cmd = CMD.Cmp()
    //         .Get()

    //     let onSuccess=function(value){
            

    //         let __eClass=Ecore.EClass.create();
    //         __eClass.get("eStructuralFeatures").add(Ecore.EAttribute.create({ name: '_#EOQ', eType: Ecore.EInt }));
    //         __eClass.set("_#EOQ",value[0][2].v);    
     

    //         if(instance.index.contains(__eClass))
    //         {
    //             $.notify("Already contains EClass");
    //         }
    //         else
    //         {
    //             __eClass.set("name",value[2][2]);    
    //             __eClass.set("abstract",value[2][2]);   
    //         }
    //     };

    //     this.domain.DoFromStr(query,onSuccess);
    // };


// this.getAllReferableObjects=function(EObject,feature,callback=null)
// {  
//     let rootObjectId=instance.findRoot(EObject).get("_#EOQ");
//     let className=EObject.eClass.get("eStructuralFeatures").find(function(e){return e.get("name")==feature;}).get("eType").get("name");
//     let query="";
//     let cmd = CMD.Cmp();
//     if(className=="EObject")
//     {
//         query="RETRIEVE #0 ?EObject"+"\nRETRIEVE $0 /eClass";
//         cmd.Get()
//             .Get();
//     }
//     else
//     {
//          query="RETRIEVE #"+rootObjectId+" $"+className+"\nRETRIEVE $0 /eClass";
//     }
    
//     //Initialization after query succeeded
//     let onSuccess=function(value){  
//         var referableObjects=[];

//         var __eClass=null;        
//         var __eObject=null;   

//         if(Array.isArray(value[0][2]))                   
//         {
//                 //multiple objects are contained     
      
//                 for (let i in value[0][2])
//                 {                               
//                     __eObject=self.__findIn(instance.__resourceSet,value[0][2][i].v);  

//                     if(!__eObject)
//                     {
//                         __eClass=self.__findIn(instance.__resourceSet,value[1][2][i].v);  
//                         if(!__eClass)
//                         {                                                          
//                             __eClass=Ecore.EClass.create();
//                             __eClass.get("eStructuralFeatures").add(Ecore.EAttribute.create({ name: '_#EOQ', eType: Ecore.EInt }));
//                             __eClass.set("_#EOQ",value[1][2][i].v);                                                              
//                         }

//                         self.__initClass(__eClass);                              
                                    
                
//                         __eObject=__eClass.create();
//                         __eObject.set("_#EOQ",value[0][2][i].v);
//                         referableObjects.push(new Proxy(__eObject,instance.proxy.EObject()));
                            
//                     }
//                     else
//                     {
//                         if(__eObject.eClass.get("name")=="Resource")
//                         {
//                             //refer to Root node of resource that has the same EOQ
//                             __eObject=__eObject.eContents()[0];
//                         }
//                         referableObjects.push(new Proxy(__eObject,instance.proxy.EObject()));
//                     }             
                
//                 }
         
//         }
//         else
//         {
            
//             __eObject=self.__findIn(instance.__resourceSet,value[0][2][i].v);  

//             if(!__eObject)
//             {
//                 //single object is contained                        
//                 __eClass=self.__findIn(instance.__resourceSet,value[1][2].v);
//                 if(!__eClass)
//                 {
//                     __eClass=Ecore.EClass.create();
//                     __eClass.get("eStructuralFeatures").add(Ecore.EAttribute.create({ name: '_#EOQ', eType: Ecore.EInt }));
//                     __eClass.set("_#EOQ",value[1][2].v);               
//                 }    
        
//                     self.__initClass(__eClass);
                            
//                     __eObject=__eClass.create();
//                     __eObject.set("_#EOQ",value[0][2].v);
//                     referableObjects.push(new Proxy(__eObject,instance.proxy.EObject()));                   
                
//             }
//             else
//             {
//                 if(__eObject.eClass.get("name")=="Resource")
//                 {
//                     //refer to Root node of resource that has the same EOQ
//                     __eObject=__eObject.eContents()[0];
//                 }                
//                 referableObjects.push(new Proxy(__eObject,instance.proxy.EObject()));
//             }            
            
//         }               
 
//         if(callback!=null)
//         {
//             callback(referableObjects);
//         }

//     };


//     this.domain.DoFromStr(query,onSuccess);

// };


this.syncResource=function(path)
{

    if($DEBUG) if($DEBUG) console.debug('SYNCING RESOURCE: '+path)

    var pathSegments=path.split("/");

    let pthQry = QRY.Qry();
    for(let i in pathSegments)
    {
        if(i<pathSegments.length-1)
        {
            pthQry.Pth("subdirectories").Sel(QRY.Pth("name").Equ(pathSegments[i])).Idx(0);
        }
        else
        {
            pthQry.Pth("resources").Sel(QRY.Pth("name").Equ(pathSegments[i])).Idx(0);
        }
    }

    let cmd = CMD.Cmp()
        .Get(pthQry)
        .Get(QRY.His(-1).Ino("EObject"))
        .Get(QRY.His(-1).Pth("eClass"))
        .Get(QRY.His(-1).Pth("ePackage"))
        .Get(QRY.His(-2).Pth("name"))
        .Get(QRY.His(-3).Pth("eSuperTypes"))
        .Get(QRY.His(2).Pth("eAllReferences"))
        .Get(QRY.His(-1).Pth("name"))
        .Get(QRY.His(-2).Pth("containment"))
        .Get(QRY.His(-3).Pth("lowerBound"))
        .Get(QRY.His(-4).Pth("upperBound"))
        .Get(QRY.His(-5).Pth("eType"))
        .Get(QRY.His(2).Pth("eAllAttributes"))
        .Get(QRY.His(-1).Pth("name"))
        .Get(QRY.His(-2).Pth("eType"))
        .Get(QRY.His(-1).Pth("name"))
        .Get(QRY.His(-2).Pth("eClass").Pth("name"))
        .Get(QRY.His(1).Met("FEATURENAMES"))
        .Get(QRY.His(1).Met("FEATUREVALUES")) 
        .Get(QRY.His(0).Pth("contents").Idx(0));

    let query = "fullSync";
   
    return self.eoq2domain.Do(cmd).then(function(val) {
    //return ecoreSync.index.query(query).then(function(values){

        
        //basic initialization of classes, so that no further query results lookup is required when adding EReferences  
        let basicClassInitialization=[];     
        for(let i in val[2])
        {
            basicClassInitialization.push(self.initEClass(val[2][i].v,val[3][i].v,val[4][i],false,val[5][i]));
        }

        return Promise.all(basicClassInitialization).then(function()
        {           
            //adding EReferences and EAttributes to classes 
            let fullClassInitialization=[];
            for(let i in val[2])
            {
               fullClassInitialization.push(self.initEClass(val[2][i].v,val[3][i].v,val[4][i],false,val[5][i],val[6][i],val[7][i],val[8][i],val[9][i],val[10][i],val[11][i],val[12][i],val[13][i],val[14][i],val[15][i],val[16][i]));
            }

            return Promise.all(fullClassInitialization).then(function()
            {              
                let pendingInitializations=[];
               
                //object initialization
                if($DEBUG) if($DEBUG) console.debug('SYNCING RESOURCE: '+path+' : init objects')
                for(let i in val[1])
                {
                    let eObject=self.index.getById(val[1][i].v);
                    if(!eObject)
                    {
                        let eClass=self.index.getById(val[2][i].v);
                        if(eClass)
                        {
                            if(eClass.get("name")=="EdgeDefinition")
                            {
                                if($DEBUG) console.debug('SYNCING RESOURCE: '+path+' : init EdgeDef objects')
                            }
                            eObject=eClass.create();
                            let oid=val[1][i].v
                            if(oid===undefined)
                            {
                                throw 'Fatal Error: object id of '+eClass.get("name")+' object was undefined'
                            }

                            eObject.set("_#EOQ",oid);

                            try{
                                self.index.add(eObject);
                            }
                            catch(e)
                            {
                                if($DEBUG) console.debug('SYNCING RESOURCE FAILED: '+path+' : object #'+oid)  
                            }

                            if(eClass.get("name")=="EdgeDefinition")
                            {
                                if($DEBUG) if($DEBUG) console.debug('SYNCING RESOURCE: '+path+' : object #'+oid)
                            
                                if($DEBUG) if($DEBUG) console.debug(eObject);
                            }
                            
                            
                        }
                        else
                        {
                            throw 'Fatal Error: eClass uninitialized #'+val[2][i].v;
                        }
                    }
                          
                        for(let f in val[17][i])
                        {
                           
                            let eStructuralFeature=eObject.eClass.getEStructuralFeature(val[17][i][f]);
                            if(eStructuralFeature)
                            {
                                if(eStructuralFeature.eClass.get("name")=="EAttribute")
                                {
                                    eObject.set(val[17][i][f],val[18][i][f]);
                                    self.__setAttributeInitialized(eObject,val[17][i][f]);
                                }
                                
                                if(eStructuralFeature.eClass.get("name")=="EReference")
                                {
                                    if(eStructuralFeature.get("upperBound")==1)
                                    {                                        
                                        if(val[18][i][f])
                                        {
                                                
                                                let containedObject=ecoreSync.index.getById(val[18][i][f].v);
                                                if(!containedObject)
                                                {                           
                                                    if($DEBUG) if($DEBUG) console.debug('Object #'+val[18][i][f].v+' not found')                  
                                                        let obj=val[1].find(function(e){ return e.v==val[18][i][f].v});
                                                        //attempt initialization from queried information
                                                        if(obj)
                                                        {
                                                             
                                                            let idx=val[1].indexOf(obj);
                                                            
                                                            let eClass=ecoreSync.index.getById(val[2][idx].v);
                                                            if($DEBUG) if($DEBUG) console.debug('Intializing '+eClass.get("name")+' object #'+val[18][i][f].v+' from queried information') 
                                                            
                                                            if(eClass)
                                                            {
                                                                if(eClass.getEStructuralFeature('_#EOQ')===undefined)
                                                                {
                                                                    if($DEBUG) if($DEBUG) console.debug('Invalid eClass definition: object id attribute is missing in eClass #'+eClass.get('_#EOQ')+' '+eClass.get('name'))
                                                                    if($DEBUG) if($DEBUG) console.debug('hotfixed exception')
                                                                    eClass.get("eStructuralFeatures").add(Ecore.EAttribute.create({ name: '_#EOQ', eType: Ecore.EInt }));
                                                                }
                                                                containedObject=eClass.create();  
                                                                let oid=val[18][i][f].v;
                                                                if(oid===undefined)
                                                                {
                                                                    throw 'Internal error: invalid object id during eObject initialization'
                                                                }                                                                                                                     
                                                                containedObject.set("_#EOQ",oid);
                                                                if($DEBUG) if($DEBUG) console.debug(containedObject);
                                                                try{
                                                                    self.index.add(containedObject);
                                                                }
                                                                catch(e)
                                                                {
                                                                    if($DEBUG) console.debug('SYNCING RESOURCE FAILED (C1): '+path+' : object #'+oid)  
                                                                }
                                                                
                                                            }                                                       
                                                        }
                                                        else
                                                        {
                                                            if($DEBUG) if($DEBUG) console.debug('Attempting initialization of Object #'+val[18][i][f].v)  
                                                            pendingInitializations.push(self.getObjectById(val[18][i][f].v).then(function(containedObject){
                                                                eObject.set(val[17][i][f],self.proxy.unproxy(containedObject));
                                                            }));
                                                        }
                                                      
                                                    
                                                        
                                                }                          

                                                if(containedObject)
                                                {
                                                    eObject.set(val[17][i][f],containedObject);
                                                }                                                            
                                        }
                                        else
                                        {
                                           // if($DEBUG) console.debug('reference '+values[18][2][i][f]+' points to null object');
                                        }                                     
                                    }
                                    else
                                    {
                                                                           

                                        for(let c in val[18][i][f])
                                        {
                                            let containedObject=ecoreSync.index.getById(val[18][i][f][c].v);
                                            if(!containedObject)
                                            {
                                            
                                                    let obj=val[1].find(function(e){ return e.v==val[18][i][f][c].v});
                                                    if(obj)
                                                    {
                                                        let idx=val[1].indexOf(obj);
                                                        
                                                        let eClass=ecoreSync.index.getById(val[2][idx].v);
                                                        
                                                        if(eClass)
                                                        {
                                                            containedObject=eClass.create();
                                                        }
                                                        else
                                                        {
                                                            throw 'eClass undefined during feature initialization'
                                                        }
                                                    }
                                                    else
                                                    {
                                                        throw 'Cannot create eObject from abstract class without concrete class information'
                                                    }
                                                
                                                    let oid = val[18][i][f][c].v
                                                    if(oid===undefined) {
                                                        if($DEBUG) console.debug("oid is undefined");
                                                    }

                                                containedObject.set("_#EOQ",oid);
                                                try{
                                                    self.index.add(containedObject);
                                                }
                                                catch(e)
                                                {
                                                    if($DEBUG) console.debug('SYNCING RESOURCE FAILED (C2): '+path+' : object #'+oid)  
                                                }
                                            }
                                            
                                            if(containedObject)
                                            {
                                                eObject.get(val[17][i][f]).add(containedObject);                               
                                       
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if($DEBUG) console.debug(eObject.eClass);
                                throw 'Fatal Error: Structural Feature is undefined feature='+val[17][i][f]+' #'+eObject.eClass.get("_#EOQ")
                            }
               
                        }                
                  
                }

              

                //hook-in object
                let resourceObject=self.index.getById(val[0].v);
                let hookIn=self.index.getById(val[19].v);
                self.proxy.unproxy(resourceObject).get("contents").add(hookIn);

                
                return Promise.all(pendingInitializations).then(function(){
                    return Promise.resolve(hookIn);
                });
            });
        });


    });
};


this.initEClass=function(id,ePackageId,name,isAbstract,eSuperTypes,eAllReferences=[],referencesName=[],referencesIsContainment=[],referencesLowerBound=[],referencesUpperBound=[],referencesETypeId=[],eAttributes=[],attributesName=[],attributesEType=[],attributesETypeName=[],attributesETypeClassName=[])
{

    //Initializes the eClass locally
    //This function needs serious rework
    
    let eClass=self.index.getById(id);
    if(!eClass)
    {
        eClass=Ecore.EClass.create();
        eClass.set("_#EOQ",id);
        eClass.get("eStructuralFeatures").add(Ecore.EAttribute.create({ name: '_#EOQ', eType: Ecore.EInt }));
        self.index.add(eClass);
    }

    let ePackage=self.index.getById(ePackageId);    
    if(!ePackage)
    {
        ePackage=Ecore.EPackage.create();
        ePackage.set("_#EOQ",ePackageId);              
        self.__initPackage(ePackage);   
    }

    if(ePackage)
    {
        eClass.set("abstract",isAbstract);
        if(!EObjectisReferenced(ePackage.get("eClassifiers"),eClass))
        {
            ePackage.get("eClassifiers").add(eClass);
        }   
        
        
        eClass.set("name",name);
        eClass.set("abstract",isAbstract);

        //local ecoreSync meta information    
        self.__setAttributeInitialized(eClass,"name");
        self.__setAttributeInitialized(eClass,"abstract");
        eClass["_isInitialized"]=true; 

        //NOTE: eSuperTypes is unused at the moment!!! 
     
        let referenceInit=self.initEReferences(eClass,eAllReferences,referencesName,referencesIsContainment,referencesLowerBound,referencesUpperBound,referencesETypeId);
        let attributesInit=self.initEAttributes(eClass,eAttributes,attributesName,attributesEType,attributesETypeName,attributesETypeClassName);
         
        if(!eClass.eContainer)
        {
            if($DEBUG) console.debug("possible uncontained class");
            if($DEBUG) console.debug(eClass);        
        }
        //if($DEBUG) console.debug([referenceInit,attributesInit]);
        return Promise.all([referenceInit,attributesInit]).then(function(){ return Promise.resolve(); });
    }
    else
    {
        return Promise.resolve('ePackage error during eClass initialization');
    }

};

this.initEAttributes=function(eClass,eAttributes=[],attributesName=[],attributesEType=[],attributesETypeName=[],attributesETypeClassName=[])
{
    var pendingInitializations=[];
    if(eAttributes.length>0)
    {
            for(let i in eAttributes)
            {
                if(eClass.get("eStructuralFeatures").array().find(function(e){return e.get("name")==attributesName[i];})==undefined)
                {
                    
                        //user-defined attributes  
                        var attrEType;

                        //Check for Standard Ecore Datatype
                        var keys=Object.keys(Ecore);
                        if( keys.includes(attributesETypeName[i]) )
                        {
                            attrEType=Ecore[attributesETypeName[i]];
                        }
                        else
                        {
                            if(attributesETypeName=="EInteger")
                            {
                                attrEType==Ecore["EInt"];
                            }
                            else
                            {
                                    attrEType=self.index.getById(attributesEType[i].v);
                                    if(!attrEType)
                                    {
                                        if(attributesETypeClassName[i]=="EEnum")
                                        {
                                            attrEType=Ecore.EEnum.create();
                                            attrEType.set("_#EOQ",attributesEType[i].v);
                                            attrEType.set("name",attributesETypeName[i]);
                                            pendingInitializations.push(self.initENum(attrEType));                                       
                                        }

                                        if(attributesETypeClassName[i]=="EDataType")
                                        {
                                            attrEType=Ecore.EDataType.create();
                                            attrEType.set("_#EOQ",attributesEType[i].v);
                                            attrEType.set("name",attributesETypeName[i]);
                                            if($DEBUG) console.debug("unsupported EDataType #"+attributesEType[i].v+" "+attributesETypeName[i]);                                            
                                        }                                                         
                                    }
                            }
                        }
      
                        let attr=Ecore.EAttribute.create({ name: attributesName[i], eType: attrEType });
                        ecoreSync.proxy.unproxy(eClass).get("eStructuralFeatures").add(attr);                               
                }
                else
                {           
                    let attr=eClass.get("eStructuralFeatures").array().find(function(e){return e.get("name")==attributesName[i];});
                    attr.set("_#EOQ",eAttributes[i].v);
                }
            }
            return Promise.all(pendingInitializations).then(function(){ return Promise.resolve(); });
        }
        else
        {
            return Promise.resolve();
        }
};


this.initEReferences=function(eClass,eAllReferences=[],referencesName=[],referencesIsContainment=[],referencesLowerBound=[],referencesUpperBound=[],referencesETypeId=[])
{
    var pendingInitializations=[];
    if(eAllReferences.length>0)
    {
        for(let i in eAllReferences)
        {
            
            if(eClass.get("eStructuralFeatures").array().find(function(e){return e.get("name")==referencesName[i];})==undefined)
            {
                //if($DEBUG) console.debug("adding reference "+referencesName[i]+" to #"+eClass.get("_#EOQ")+" cnt="+referencesIsContainment[i]+" lwr="+referencesLowerBound[i]+" uppr="+referencesUpperBound[i]);
                let refEType=self.index.getById(referencesETypeId[i].v);
                if(refEType)
                {
                    let ref=Ecore.EReference.create({ name: referencesName[i], eType: refEType, containment: referencesIsContainment[i], lowerBound: referencesLowerBound[i], upperBound: referencesUpperBound[i] });
                    ref.set("_#EOQ",eAllReferences[i].v);
                    ecoreSync.proxy.unproxy(eClass).get("eStructuralFeatures").add(ref);   
                }
                else
                {
                    refEType=Ecore.EClass.create();
                    refEType.set("_#EOQ",referencesETypeId[i].v);
                    refEType.set("name",referencesName[i]);
                    pendingInitializations.push(self.isClassInitialized(refEType).then(function(){
                        if(eClass.get("eStructuralFeatures").array().find(function(e){return e.get("name")==referencesName[i];})==undefined)
                        {
                            let ref=Ecore.EReference.create({ name: referencesName[i], eType: refEType, containment: referencesIsContainment[i], lowerBound: referencesLowerBound[i], upperBound: referencesUpperBound[i] });
                            ref.set("_#EOQ",eAllReferences[i].v);
                            ecoreSync.proxy.unproxy(eClass).get("eStructuralFeatures").add(ref);   
                        }
                        return Promise.resolve();
                    }));                        
                }
            }
        }        

        return Promise.all(pendingInitializations).then(function(){ return Promise.resolve(); });
    }
    else
    {
        return Promise.resolve();
    }
}

this.getObjectURL=function(eObject)
{
    return (this.domain._url+'/#'+eObject.get("_#EOQ")).replace('ws://','eoq://').replace('ws/eoq.do/','');
}

this.getObjectByURL=function(url){
    var domain=null;
    if(self.isObjectURL(url))
    {
        domainHost=url.substr(url.indexOf('eoq://')+6,url.indexOf('/#')-(url.indexOf('eoq://')+6));
        
        if(this.domain._url.includes(domainHost))
        {
            let objId=url.substr(url.indexOf('/#')+2);
            return ecoreSync.index.getById(objId);
        }
        return null;
    }

}

this.isObjectURL=function(url){
    if(typeof url == 'string')
    {
        if(url.includes('eoq://') && url.includes('/#'))
        {
            return true;
        }
    }
    return false;
}

this.__notifyAll=function(EObject,EClass)
{

    // triggers the change event for all EObjects that are in relation with the specified EClass

    var contents=EObject.eContents();
    if(EObject.get("_#EOQ")!=null)
    {
        //console.log("TRIGG INFORM #"+EObject.get("_#EOQ")+"/name="+EObject.get("name")+" on #"+EClass.get("_#EOQ"));
 

        for(let i in contents)
        {  

            var isUser=false;
            var containsUsers=false;

            // Check if any contained object uses the EClass
            if(contents[i].eClass.get("_#EOQ")==EClass.get("_#EOQ"))
            {
                contents[i].eResource().trigger("change",contents[i]);
            }

            /*
            var references=contents[i].eClass.get("eAllReferences");
            for(let j in references)
            {
                if(references[j].get("eType").get("_#EOQ")==EClass.get("_#EOQ"))
                {
             
               //$.notify("REFRESH REFERENCE FIRE #"+contents[i].eClass.get("_#EOQ")+"/"+references[j].get("name") +" FOR #"+EClass.get("_#EOQ"),"info");
                contents[i].eResource().trigger("change",contents[i].get(references[j].get("name"))._owner);

                }
            }
            */


            /*
            var containments=contents[i].eClass.get("eAllContainments");
            for(let j in containments)
            {
                if(containments[j].get("eType").get("_#EOQ")==EClass.get("_#EOQ"))
                {
                   // //$.notify("NOT TRIGGERED\nCONTAINMENT #"+contents[i].eClass.get("_#EOQ")+"/"+containments[j].get("name") +" FOR #"+EClass.get("_#EOQ"),"info");
                //contents[i].eResource().trigger("change",contents[i].get(containments[j].get("name"))._owner);
                }
            }
            */

            //Find users in contained object
            self.__notifyAll(contents[i],EClass);
        }
    }
    
}

/* Encoding/Decoding for EOQ2*/
 this.encode=function(obj){  

    if(obj!=null)
    {
     switch(obj.constructor.name)
     { 
         case "EObject":
              res={qry:"OBJ",v:obj.get("_#EOQ")}
              break;
         default:
             res=obj
             break;
     }
    }
    else
    {
        return null
    }
 
     return res
 }
 
 this.decode=async function(encodedObject){

     if(encodedObject!=null)
     {
       if(encodedObject.isProxy) {
           console.error(encodedObject);
            throw 'invalid decoded object supplied'
       }
            if(encodedObject.qry=="OBJ")
            {   
                var obj=await self.getObjectById(encodedObject.v);
                return self.proxy.unproxy(obj)
            }
            else
            {
                return Promise.resolve(encodedObject)
            }
      
    }
    else
    {
        if($DEBUG) console.debug('encodedObject is null')
        return Promise.resolve(null);
    }
 }
   
 this.remoteExec=function(cmd){
     var res=self.eoq2domain.RawDo(cmd)
     if($DEBUG) this.debugger.trace(cmd,new Error().stack,res,'remote');
     return res;
 }
 
 this.exec=function(cmd)
 {     
    var res=null;
    try{
        res=self.cmdRunner.Exec(cmd)
    }catch(e){
        console.error('CMD');
        console.error(cmd);
        console.error('Query execution failed: '+e);      
     
        throw e
    }
    if($DEBUG) this.debugger.trace(cmd,new Error().stack,res,'auto');
    return res;
 }

 this.observe=async function(query,callback)
 {
    /* Observes the query for changes and calls the callback with new results*/
    if(!callback) throw ' no callback function supplied for observe operation '
    var queryObserverState=new QueryObserverState();
    var res=await self.queryObserver.Eval(query,function(results){         
        if(queryObserverState.update(results))
        {       
            callback(queryObserverState.getResults(),queryObserverState.getDeltaPlus(),queryObserverState.getDeltaMinus())
        }
    });
    queryObserverState.update(await ApplyToAllElements(res,self.decode));
    return queryObserverState.getResults();
 }

 this.sync=async function(cmd)
 {

    /* Assures synchronization between remote server and local model */ 
    var local=self.cmdRunner.Exec(cmd); 
    var remote=self.eoq2domain.RawDo(cmd);
    [localResult, remoteResult] = [ await local, await remote]

    var lResult=await ApplyToAllElements(localResult,self.encode)
    var rResult=remoteResult.v
    var resultsMatch=JSON.stringify(lResult)==JSON.stringify(rResult)
    if(!resultsMatch)
    {
        console.error({result: resultsMatch,local:lResult, remote:rResult})
        var segment=cmd.a.v.pop();
        console.error('Segment stripped')
        await self.sync(cmd)
    }
    else
    {
        console.error('Results matched')
    }
    return {syncState: resultsMatch, local: lResult, remote: rResult}
 }

this.getParent=async function(eObject)
{
    /* Gets the eObjects parent */   
    if(eObject.eContainer)
    {
        return eObject.eContainer
    }
    else
    {
        var response=await ecoreSync.remoteExec(new eoq2.Get(new eoq2.Obj(eObject.get("_#EOQ").Met('PARENT'))))
        var parent=ecoreSync.decode(response);
        return parent;
    }
}

this.getAllContents=async function(eObject,filter=null)
{
    var results=[];
    var contents=[];
    await self.isClassInitialized(eObject.eClass)
    var containments=eObject.eClass.get("eAllContainments")
    containments.forEach(function(cnt){
        contents.push(self.get(eObject,cnt.get("name")));        
    })
    
    var res=await Promise.all(contents);

    res.forEach(function(cnts){
        if(Array.isArray(cnts))
        {
            results=results.concat(cnts);
        }
        else
        {
            if(cnts)
            {
                results.push(cnts);
            }
        }
    });

    var subContents=[];
    results.forEach(function(obj){
        if(obj)
        {
            subContents.push(self.getAllContents(obj));
        }
    })
    subContents=await Promise.all(subContents);
    subContents.forEach(function(sc){
        results=results.concat(sc);
    })

    if(!filter)
    {
        return results;
    }
    else
    {
        return results.filter(filter);
    }
}   

this._trace=async function(cmd,stack,res,mode='auto'){
    var self=this;
    var traceStart=new Date();
    var results=await res  
    var traceEnd=new Date();  
    let queryTraceData = {
        queryString: cmd, // string representation will become available once jseoq is updated
        queryDuration: traceEnd.getTime() - traceStart.getTime(), // in ms
        queryResult: results, //is currently not the same for locally and remotely queries! 
        queryId: 0, // query id returned from eoq
        queryIssuer: 'ecoreSync', // component string of query issuer
        queryStackTrace: stack.split('\n'), // stack trace that lead to query
        queryMode: mode, // as shown in your table (not sure what it does)    
        dateStart: traceStart, // datetime when query was sent
        dateEnd: traceEnd, // datetime when response was received    
        dataTarget: 'unknown', // optional css selector for element that shall receive data from query
    }
    $app.plugins.require('eventBroker').publish('debug/ecoreSync/queries',queryTraceData);
}


};



  // END LEGACY FUNCTIONS



var ProxyResource={get: function(target,key,receiver){
    
   
    if(key=="get")
    {
        const originalMethod=target[key];
        const self=target;
        return function(){ return originalMethod.apply(self,arguments);}
    } 
    else
    {
        return origMethod;
    }

    }};

// BA removed global 
//var ecoreSync=new EcoreSync("http://localhost:8000/eoq.do");

// ecoreSync Object for editor Models
//var editorModels=new EcoreSync("http://localhost:8000/eoq.do");

// ecoreSync Object for editor Models
//var layoutModels=new EcoreSync("http://localhost:8000/eoq.do");



